﻿namespace FIOAPIWeb
{
    public partial class FIONavMenu
    {
        protected override void OnInitialized()
        {
            UserAppState.OnChange += StateHasChanged;
            base.OnInitialized();
        }

        public void Dispose()
        {
            UserAppState.OnChange -= StateHasChanged;
        }
    }
}
