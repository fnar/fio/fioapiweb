﻿using MudBlazor;

namespace FIOAPIWeb.Components.Shared
{
    public partial class AuthenticationComponent
    {
        private bool HasAttemptedAuth = false;

        protected override void OnInitialized()
        {
            UserAppState.OnChange += StateHasChanged;
        }

		protected override async Task OnAfterRenderAsync(bool firstRender)
		{            
			if (firstRender)
            {
				await UserAppState.InitializeAsync();
			}
		}

		public void Dispose()
        {
            UserAppState.OnChange -= StateHasChanged;
        }

        async Task LoginAsync()
        {
            var DlgOptions = new DialogOptions
            {
                CloseOnEscapeKey = true,
                CloseButton = true,
            };

            var result = await DialogService.Show<LoginDialog>("Login", DlgOptions).Result;
            if (!result.Canceled)
            {
				HasAttemptedAuth = true;
            }
        }

        async Task LogoutAsync()
        {
			HasAttemptedAuth = false;
            await UserAppState.LogoutAsync();
        }

        void ClickNotifications()
        {
            NavManager.NavigateTo("/notifications");
        }

	}
}
