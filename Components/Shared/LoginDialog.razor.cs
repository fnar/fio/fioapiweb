﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;
using MudBlazor;

namespace FIOAPIWeb.Components.Shared
{
    public partial class LoginDialog
    {
        [CascadingParameter]
        MudDialogInstance MudDialog { get; set; } = null!;

        void Login()
        {
            _ = UserAppState.LoginAsync(Username, Password, RememberMe);
            MudDialog.Close();
        }

        void Cancel() => MudDialog.Cancel();

        string Username { get; set; } = "";
        string Password { get; set; } = "";

        public bool RememberMe = true;

        public bool SubmitButtonDisabled
        {
            get
            {
                return !Common.IsUserNameValid(Username) || !Common.IsPasswordValid(Password);
            }
        }

        protected override void OnInitialized()
        {
            base.OnInitialized();
            UserAppState.OnChange += StateHasChanged;

            if (UserAppState.UserName != null)
            {
                Username = UserAppState.UserName;
            }
        }

        public void Dispose()
        {
            UserAppState.OnChange -= StateHasChanged;
        }

        public void HandleOnKeyDown(KeyboardEventArgs e)
        {
            if (e.Code == "Enter" || e.Code == "NumpadEnter")
            {
                Login();
            }
        }
    }
}
