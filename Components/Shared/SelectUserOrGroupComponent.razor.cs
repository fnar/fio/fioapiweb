﻿using Microsoft.AspNetCore.Components;

namespace FIOAPIWeb.Components.Shared
{
    public partial class SelectUserOrGroupComponent
    {
        [Parameter]
#pragma warning disable BL0007 // Component parameters should be auto properties
        public Payloads.PermissionResponse? SelectedPermission

        {
            get => _SelectedPermission;
            set
            {
                if (_SelectedPermission != value)
                {
                    _SelectedGroup = null!;
                    _SelectedPermission = value;
                    SelectedPermissionChanged.InvokeAsync(value);
                }
            }
        }
        private Payloads.PermissionResponse? _SelectedPermission = null;

        [Parameter]
        public EventCallback<Payloads.PermissionResponse?> SelectedPermissionChanged { get; set; }

        [Parameter]
#pragma warning disable BL0007 // Component parameters should be auto properties
        public Payloads.Group? SelectedGroup
#pragma warning restore BL0007 // Component parameters should be auto properties
        {
            get => _SelectedGroup;
            set
            {
                if (_SelectedGroup != value)
                {
                    _SelectedPermission = null!;
                    _SelectedGroup = value;
                    SelectedGroupChanged.InvokeAsync(value);
                }
            }
        }
        private Payloads.Group? _SelectedGroup = null;

        [Parameter]
        public EventCallback<Payloads.Group?> SelectedGroupChanged { get; set; }

        protected override void OnInitialized()
        {
            base.OnInitialized();
            UserAppState.OnChange += StateHasChanged;
        }

        public void Dispose()
        {
            UserAppState.OnChange -= StateHasChanged;
        }

        public void OnUserClick(string UserName)
        {
            SelectedPermission = UserAppState.Permissions.First(p => p.GrantorUserName == UserName);
        }

        public void OnGroupClick(int GroupId)
        {
            SelectedGroup = UserAppState.Groups.First(g => g.GroupId == GroupId);
        }
    }
}
