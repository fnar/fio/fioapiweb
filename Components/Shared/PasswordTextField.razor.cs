﻿using Microsoft.AspNetCore.Components;
using MudBlazor;

namespace FIOAPIWeb.Components.Shared
{
    public partial class PasswordTextField
    {
        private const string VisibleIcon = Icons.Material.Filled.Visibility;
        private const string NotVisibleIcon = Icons.Material.Filled.VisibilityOff;

        [Parameter]
        public RenderFragment ChildContent { get; set; } = null!;

        [Parameter]
#pragma warning disable BL0007 // Component parameters should be auto properties
        public string Password
#pragma warning restore BL0007 // Component parameters should be auto properties
        {
            get => _Password;
            set
            {
                if (_Password != value)
                {
                    _Password = value;
                    HasError = !Common.IsPasswordValid(value);
                    ErrorText = HasError ? Common.PasswordLengthError : "";
                    PasswordChanged.InvokeAsync(value);
                    StateHasChanged();
                }
            }
        }
        private string _Password = "";

        [Parameter]
        public EventCallback<string> PasswordChanged { get; set; }

        [Parameter]
        public string Label { get; set; } = "Password";

        private bool HasError { get; set; } = true;
        private string ErrorText { get; set; } = "Password is required";

        private bool IsTextVisible { get; set; } = false;
        private InputType TextInputType { get; set; } = InputType.Password;
        private string InputIcon { get; set; } = NotVisibleIcon;

        void OnToggleShowPassword()
        {
            IsTextVisible = !IsTextVisible;
            TextInputType = IsTextVisible ? InputType.Text : InputType.Password;
            InputIcon = IsTextVisible ? VisibleIcon : NotVisibleIcon;
        }
    }
}
