﻿using Microsoft.AspNetCore.Components;

namespace FIOAPIWeb.Components.Shared
{
    public partial class LengthValidatedTextField
    {
        [Parameter]
        public RenderFragment ChildContent { get; set; } = null!;

        [Parameter]
#pragma warning disable BL0007 // Component parameters should be auto properties
        public string Text
#pragma warning restore BL0007 // Component parameters should be auto properties
        {
            get => _Text;
            set
            {
                if (_Text != value)
                {
                    _Text = value;
                    TextChanged.InvokeAsync(value);
                    StateHasChanged();
                }
            }
        }
        private string _Text = "";

        [Parameter]
        public EventCallback<string> TextChanged { get; set; }

        [Parameter]
        public string Label { get; set; } = "Password";

        [Parameter]
        public int MinLength { get; set; } = -1;

        [Parameter]
        public int MaxLength { get; set; } = -1;

        [Parameter]
        public bool AllowEmpty { get; set; } = false;

        private bool HasError 
        { 
            get
            {
                if (Text.Length == 0 && AllowEmpty)
                {
                    return false;
                }
                else if (MinLength != -1 && MaxLength != -1)
                {
                    return Text.Length < MinLength || Text.Length > MaxLength;
                }
                else if (MinLength != -1)
                {
                    return Text.Length < MinLength;
                }
                else if (MaxLength != -1)
                {
                    return Text.Length > MaxLength;
                }
                else
                {
                    // Do no error checking
                    return false;
                }
            }
        }

        private string ErrorText
        {
            get
            {
                if (!HasError)
                {
                    return "";
                }

                if (MinLength != -1 && MaxLength != -1)
                {
                    return $"Length must be [{MinLength},{MaxLength}]";
                }
                else if (MinLength != -1)
                {
                    return $"Length must be >= {MinLength}";
                }
                else if (MaxLength != -1)
                {
                    return $"Length must be <= {MaxLength}";
                }
                else
                {
                    // Do no error checking
                    return "";
                }
            }
        }
    }
}
