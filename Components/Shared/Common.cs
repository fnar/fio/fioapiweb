﻿using Microsoft.AspNetCore.Components;
using MudBlazor;

namespace FIOAPIWeb.Components.Shared
{
    public static class Common
    {
        public static DialogOptions DefaultOptions = new DialogOptions
        {
            CloseOnEscapeKey = true,
            CloseButton = true,
        };

        public const int UserNameMinLength = 3;
        public const int UserNameMaxLength = 32;
        public static string UserNameLengthError = GetLengthError(UserNameMinLength, UserNameMaxLength);
        public static bool IsUserNameValid(string UserName) => IsCorrectLength(UserName, UserNameMinLength, UserNameMaxLength);

        public const int PasswordMinLength = 3;
        public const int PasswordMaxLength = 256;
        public static string PasswordLengthError = GetLengthError(PasswordMinLength, PasswordMaxLength);
        public static bool IsPasswordValid(string Password) => IsCorrectLength(Password, PasswordMinLength, PasswordMaxLength);

        public static bool IsCorrectLength(string Input, int MinLength = -1, int MaxLength = -1, bool AllowEmpty = false)
        {
            if (Input.Length == 0 && AllowEmpty)
            {
                return true;
            }
            else if (MinLength != -1 && MaxLength != -1)
            {
                return Input.Length >= MinLength && Input.Length <= MaxLength;
            }
            else if (MinLength != -1)
            {
                return Input.Length >= MinLength;
            }
            else if (MaxLength != -1)
            {
                return Input.Length <= MaxLength;
            }
            else
            {
                return true;
            }
        }

        public static string GetLengthError(int MinLength = -1, int MaxLength = -1)
        {
            if (MinLength != -1 && MaxLength != -1)
            {
                return $"Must have a length of [{MinLength},{MaxLength}]";
            }
            else if (MinLength != -1)
            {
                return $"Must have a length >= {MinLength}";
            }
            else if (MaxLength != -1)
            {
                return $"Must have a length <= {MaxLength}";
            }
            else
            {
                return "";
            }
        }

        public static MarkupString Spacer(int Length = 100)
        {
            return new MarkupString(string.Concat(Enumerable.Repeat("&nbsp;", Length)));
        }
    }
}
