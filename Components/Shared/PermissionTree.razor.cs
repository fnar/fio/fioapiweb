﻿using Microsoft.AspNetCore.Components;

namespace FIOAPIWeb.Components.Shared
{
    public class PermissionTreeItemData
    {
        public PermissionTreeItemData? Parent { get; set; } = null;

        public string Text { get; set; } = "";

        public bool IsExpanded { get; set; } = false;

        public bool IsChecked { get; set; } = false;

        public bool HasChild => TreeItems != null && TreeItems.Count > 0;

        public HashSet<PermissionTreeItemData> TreeItems { get; set; } = new();

        public PermissionTreeItemData(string text)
        {
            Text = text;
            IsChecked = true;
        }

        public void AddChild(string itemName, bool IsChecked)
        {
            var item = new PermissionTreeItemData(itemName);
            item.Parent = this;
            item.IsChecked = IsChecked;
            TreeItems.Add(item);
        }

        public bool? GetCheckedState()
        {
            if (HasChild)
            {
                int ChildrenCheckedCount = TreeItems.Where(ti => ti.IsChecked).Count();
                if (ChildrenCheckedCount > 0 && ChildrenCheckedCount < TreeItems.Count)
                {
                    return null;
                }
                else
                {
                    return ChildrenCheckedCount == TreeItems.Count;
                }
            }
            else
            {
                return IsChecked;
            }
        }
    }

    public partial class PermissionTree
    {
#pragma warning disable BL0007
        [Parameter]
        public Payloads.Permissions Permissions
        {
            get => _Permissions;
            set
            {
                if (_Permissions != value)
                {
                    _Permissions = value;
                    PermissionsChanged.InvokeAsync(value);
                }
            }
        }
        private Payloads.Permissions _Permissions = new();
#pragma warning restore BL0007

        [Parameter]
        public EventCallback<Payloads.Permissions> PermissionsChanged { get; set; }

        [Parameter]
        public bool ReadOnly { get; set; } = false;

        public HashSet<PermissionTreeItemData> TreeItems { get; set; } = new();

        private const string Perm_ShipInfo = "Ship Info";
        private const string Perm_ShipRepairs = "Ship Repairs";
        private const string Perm_ShipFlights = "Ship Flights";
        private const string Perm_ShipInventory = "Ship Inventory";
        private const string Perm_ShipFuelInventory = "Ship Fuel Inventory";

        private const string Perm_SiteLocation = "Locations";
        private const string Perm_SiteWorkforce = "Workforces";
        private const string Perm_SiteExperts = "Experts";
        private const string Perm_SiteBuildings = "Buildings";
        private const string Perm_SiteRepairs = "Repairs";
        private const string Perm_SiteReclaimables = "Reclaimables";
        private const string Perm_SiteProductionLines = "Production Lines";

        private const string Perm_StorageLocation = "Storage Locations";
        private const string Perm_StorageInformation = "Storage Info";
        private const string Perm_StorageItems = "Storage Items";

        private const string Perm_TradeContracts = "Contracts";
        private const string Perm_TradeCXOS = "CXOS";

        private const string Perm_CompanyInfo = "Company Info";
        private const string Perm_CompanyLiquidCurrency = "Liquid Currency";
        private const string Perm_CompanyHeadquarters = "Headquarters";

        private const string Perm_MiscShipmentTracking = "Shipment Tracking";

        protected override void OnInitialized()
        {
            var ship = new PermissionTreeItemData("Ship");
            ship.AddChild(Perm_ShipInfo, Permissions.ShipPermissions.Information);
            ship.AddChild(Perm_ShipRepairs, Permissions.ShipPermissions.Repair);
            ship.AddChild(Perm_ShipFlights, Permissions.ShipPermissions.Flight);
            ship.AddChild(Perm_ShipInventory, Permissions.ShipPermissions.Inventory);
            ship.AddChild(Perm_ShipFuelInventory, Permissions.ShipPermissions.FuelInventory);
            TreeItems.Add(ship);

            var sites = new PermissionTreeItemData("Sites");
            sites.AddChild(Perm_SiteLocation, Permissions.SitesPermissions.Location);
            sites.AddChild(Perm_SiteWorkforce, Permissions.SitesPermissions.Workforces);
            sites.AddChild(Perm_SiteExperts, Permissions.SitesPermissions.Experts);
            sites.AddChild(Perm_SiteBuildings, Permissions.SitesPermissions.Buildings);
            sites.AddChild(Perm_SiteRepairs, Permissions.SitesPermissions.Repair);
            sites.AddChild(Perm_SiteReclaimables, Permissions.SitesPermissions.Reclaimable);
            sites.AddChild(Perm_SiteProductionLines, Permissions.SitesPermissions.ProductionLines);
            TreeItems.Add(sites);

            var storage = new PermissionTreeItemData("Storage");
            storage.AddChild(Perm_StorageLocation, Permissions.StoragePermissions.Location);
            storage.AddChild(Perm_StorageInformation, Permissions.StoragePermissions.Information);
            storage.AddChild(Perm_StorageItems, Permissions.StoragePermissions.Items);
            TreeItems.Add(storage);

            var trade = new PermissionTreeItemData("Trade");
            trade.AddChild(Perm_TradeContracts, Permissions.TradePermissions.Contract);
            trade.AddChild(Perm_TradeCXOS, Permissions.TradePermissions.CXOS);
            TreeItems.Add(trade);

            var company = new PermissionTreeItemData("Company");
            company.AddChild(Perm_CompanyInfo, Permissions.CompanyPermissions.Info);
            company.AddChild(Perm_CompanyLiquidCurrency, Permissions.CompanyPermissions.LiquidCurrency);
            company.AddChild(Perm_CompanyHeadquarters, Permissions.CompanyPermissions.Headquarters);
            TreeItems.Add(company);

            var misc = new PermissionTreeItemData("Misc");
            misc.AddChild(Perm_MiscShipmentTracking, Permissions.MiscPermissions.ShipmentTracking);
            TreeItems.Add(misc);
        }

        private void UpdatePermissionObject()
        {
            Payloads.Permissions NewPerm = new();

            var childTreeItems = TreeItems.SelectMany(ti => ti.TreeItems);
            foreach (var treeItem in childTreeItems)
            {
                switch (treeItem.Text)
                {
                    case Perm_ShipInfo:
                        NewPerm.ShipPermissions.Information = treeItem.IsChecked;
                        break;
                    case Perm_ShipRepairs:
                        NewPerm.ShipPermissions.Repair = treeItem.IsChecked;
                        break;
                    case Perm_ShipFlights:
                        NewPerm.ShipPermissions.Flight = treeItem.IsChecked;
                        break;
                    case Perm_ShipInventory:
                        NewPerm.ShipPermissions.Inventory = treeItem.IsChecked;
                        break;
                    case Perm_ShipFuelInventory:
                        NewPerm.ShipPermissions.FuelInventory = treeItem.IsChecked;
                        break;
                    case Perm_SiteLocation:
                        NewPerm.SitesPermissions.Location = treeItem.IsChecked;
                        break;
                    case Perm_SiteWorkforce:
                        NewPerm.SitesPermissions.Workforces = treeItem.IsChecked;
                        break;
                    case Perm_SiteExperts:
                        NewPerm.SitesPermissions.Experts = treeItem.IsChecked;
                        break;
                    case Perm_SiteBuildings:
                        NewPerm.SitesPermissions.Buildings = treeItem.IsChecked;
                        break;
                    case Perm_SiteRepairs:
                        NewPerm.SitesPermissions.Repair = treeItem.IsChecked;
                        break;
                    case Perm_SiteReclaimables:
                        NewPerm.SitesPermissions.Reclaimable = treeItem.IsChecked;
                        break;
                    case Perm_SiteProductionLines:
                        NewPerm.SitesPermissions.ProductionLines = treeItem.IsChecked;
                        break;
                    case Perm_StorageLocation:
                        NewPerm.StoragePermissions.Location = treeItem.IsChecked;
                        break;
                    case Perm_StorageInformation:
                        NewPerm.StoragePermissions.Information = treeItem.IsChecked;
                        break;
                    case Perm_StorageItems:
                        NewPerm.StoragePermissions.Items = treeItem.IsChecked;
                        break;
                    case Perm_TradeContracts:
                        NewPerm.TradePermissions.Contract = treeItem.IsChecked;
                        break;
                    case Perm_TradeCXOS:
                        NewPerm.TradePermissions.CXOS = treeItem.IsChecked;
                        break;
                    case Perm_CompanyInfo:
                        NewPerm.CompanyPermissions.Info = treeItem.IsChecked;
                        break;
                    case Perm_CompanyLiquidCurrency:
                        NewPerm.CompanyPermissions.LiquidCurrency = treeItem.IsChecked;
                        break;
                    case Perm_CompanyHeadquarters:
                        NewPerm.CompanyPermissions.Headquarters = treeItem.IsChecked;
                        break;
                    case Perm_MiscShipmentTracking:
                        NewPerm.MiscPermissions.ShipmentTracking = treeItem.IsChecked;
                        break;
                }
            }

            Permissions = NewPerm;
        }

        protected void CheckedChanged(PermissionTreeItemData item)
        {
            item.IsChecked = !item.IsChecked;
            // checked status on any child items should mirrror this parent item
            if (item.HasChild)
            {
                foreach (var child in item.TreeItems)
                {
                    child.IsChecked = item.IsChecked;
                }
            }
            // if there's a parent and all children are checked/unchecked, parent should match
            if (item.Parent != null)
            {
                item.Parent.IsChecked = !item.Parent.TreeItems.Any(i => !i.IsChecked);
            }

            UpdatePermissionObject();
        }
    }
}
