﻿using Microsoft.AspNetCore.Components;
using MudBlazor;

namespace FIOAPIWeb.Components.Shared
{
    public partial class SimpleConfirmDialog
    {
        [CascadingParameter]
        protected MudDialogInstance MudDialog { get; set; } = null!;

        [Parameter]
        public string ContentText { get; set; } = "Are you sure you want to continue?";

        [Parameter]
        public string AcceptButtonText { get; set; } = "Confirm";

        [Parameter]
        public string CancelButtonText { get; set; } = "Cancel";

        [Parameter]
        public Func<Task>? AcceptFunc { get; set; }

        [Parameter]
        public Func<Task>? CancelFunc { get; set; }

        public bool IsProcessing { get; set; } = false;
        public bool IsCanceling { get; set; } = false;
        public bool IsAccepting { get; set; } = false;

        public virtual async Task OnCancelButton()
        {
            IsProcessing = true;
            IsCanceling = true;
            StateHasChanged();

            if (CancelFunc != null)
            {
                await CancelFunc();
            }

            IsCanceling = false;
            IsProcessing = false;

            MudDialog.Cancel();
            StateHasChanged();
        }

        public virtual async Task OnAcceptButton()
        {
            IsProcessing = true;
            IsAccepting = true;
            StateHasChanged();

            if (AcceptFunc != null)
            {
                await AcceptFunc();
            }

            IsAccepting = false;
            IsProcessing = false;

            MudDialog.Close(DialogResult.Ok(true));
            StateHasChanged();
        }
    }
}
