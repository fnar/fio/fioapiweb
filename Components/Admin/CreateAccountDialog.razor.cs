﻿using System.Net;
using System.Text;
using System.Text.Json;
using Microsoft.AspNetCore.Components;
using MudBlazor;

namespace FIOAPIWeb.Components.Admin
{
    public partial class CreateAccountDialog
    {
        [CascadingParameter]
        MudDialogInstance MudDialog { get; set; } = null!;

        public string? RecentClipboardAccountInfo
        {
            get; private set;
        } = null;


        string Username
        {
            get => _Username;
            set
            {
                if (_Username != value)
                {
                    _Username = value;
                    IsValidUsername = _Username.Length >= 3 && _Username.Length <= 32;
                }
            }
        }
        string _Username = "";

        string Password
        {
            get => _Password;
            set
            {
                if (_Password != value)
                {
                    _Password = value;
                    IsValidPassword = _Password.Length >= 3 && _Password.Length <= 256;
                }
            }
        }
        string _Password = GenerateRandomPassword(16);

        Severity Severity
        {
            get
            {
                if (IsExistingUser || !IsValidUsername || !IsValidPassword)
                {
                    return Severity.Error;

                }
                else
                {
                    return Severity.Info;
                }
            }
        }

        string StatusText
        {
            get
            {
                if (IsValidUsername && IsValidPassword)
                {
                    if (IsExistingUser)
                    {
                        return "Users Exists: This Will Update The User's Password";
                    }
                    else
                    {
                        if (Admin)
                        {
                            return "This will create a new user that is an administrator";
                        }
                        else
                        {
                            return "This will create a new user that is not an administrator";
                        }
                    }
                }
                else
                {
                    return "Username must be length [3,32] and password length [3,256]";
                }
            }
        }

        static MarkupString SpacerString = new MarkupString(string.Concat(Enumerable.Repeat("&nbsp;", 85)));

        bool Admin = false;

        bool IsValidUsername = false;
        bool IsValidPassword = true;
        bool CheckingIfExistingUser = false;
        bool IsExistingUser = false;

        bool CreatingAccount = false;

        protected override void OnInitialized()
        {
            base.OnInitialized();
            UserAppState.OnChange += StateHasChanged;
        }

        public void Dispose()
        {
            UserAppState.OnChange -= StateHasChanged;
        }

        // Not cryptographically secure, but who cares.
        private static Random rnd = new Random();
        private static string GenerateRandomPassword(int length)
        {
            const string validCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%^&*";
            StringBuilder res = new StringBuilder();
            while (0 < length--)
            {
                res.Append(validCharacters[rnd.Next(validCharacters.Length)]);
            }

            return res.ToString();
        }

        async Task Create()
        {
            CreatingAccount = true;
            {
                var createAccountPayload = new Payloads.CreateAccount()
                {
                    UserName = Username,
                    Password = Password,
                    Admin = Admin
                };

                var createUserRequest = new Web.Request(HttpMethod.Post, "/admin/createaccount", UserAppState.BearerToken, JsonSerializer.Serialize(createAccountPayload));
                await createUserRequest.GetResultNoResponse();

                string ClipboardAccountInfo = $"Account information:\r\n```\r\nUserName: {createAccountPayload.UserName}\r\nPassword: {createAccountPayload.Password}\r\n```";
                if (createUserRequest.StatusCode == HttpStatusCode.OK)
                {
                    RecentClipboardAccountInfo = $"Account information:\r\n```\r\nUserName: {createAccountPayload.UserName}\r\nPassword: {createAccountPayload.Password}\r\n```";
                    try
                    {
                        await Clipboard.WriteTextAsync(ClipboardAccountInfo);
                        Snackbar.Add("Account created.  Details in clipboard.", Severity.Success);
                    }
                    catch
                    {
                        Snackbar.Add("Browser failed to allow clipboard copying.", Severity.Error);
                    }

                    MudDialog.Close(DialogResult.Ok(ClipboardAccountInfo));
                }
                else
                {
                    Snackbar.Add("Failed to Create Account", Severity.Error);
                    MudDialog.Cancel();
                }
            }
            CreatingAccount = false;
        }

        void Cancel() => MudDialog.Cancel();

        async Task HandleIntervalElapsed(string usernameText)
        {
            CheckingIfExistingUser = true;
            {
                IsExistingUser = false;

                var userExistsRequest = new Web.Request(HttpMethod.Get, $"/admin/isuser/{usernameText}", UserAppState.BearerToken);
                await userExistsRequest.GetResultNoResponse();
                if (userExistsRequest.StatusCode == HttpStatusCode.OK)
                {
                    IsExistingUser = true;
                }
            }
            CheckingIfExistingUser = false;
        }
    }
}
