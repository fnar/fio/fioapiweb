﻿using Microsoft.AspNetCore.Components;
using System.Net;

namespace FIOAPIWeb.Components.Notifications
{
    public partial class AcceptRejectButtons
    {
        [Parameter]
        public int GroupId { get; set; }

        [Parameter]
        public EventCallback OnCompletion { get; set; }

        private bool IsRejecting { get; set; } = false;
        private bool IsAccepting { get; set; } = false;
        private bool IsProcessing => IsRejecting || IsAccepting;

        protected override void OnInitialized()
        {
            base.OnInitialized();
            UserAppState.OnChange += StateHasChanged;
        }

        public void Dispose()
        {
            UserAppState.OnChange -= StateHasChanged;
        }

        async Task OnReject()
        {
            IsRejecting = true;
            StateHasChanged();

            var request = new Web.Request(HttpMethod.Put, $"/group/invite/reject/{GroupId}", UserAppState.BearerToken);
            await request.GetResultNoResponse();
            if (request.StatusCode == HttpStatusCode.OK)
            {
                Snackbar.Add($"Rejected {GroupId} invite", MudBlazor.Severity.Success);
                await OnCompletion.InvokeAsync();
            }
            else
            {
                Snackbar.Add("Encountered unknown error", MudBlazor.Severity.Error);
                IsRejecting = false;
            }
        }

        async Task OnAccept()
        {
            IsAccepting = true;
            StateHasChanged();

            var request = new Web.Request(HttpMethod.Put, $"/group/invite/accept/{GroupId}", UserAppState.BearerToken);
            await request.GetResultNoResponse();

            if (request.StatusCode == HttpStatusCode.OK)
            {
                Snackbar.Add($"Accepted {GroupId} invite", MudBlazor.Severity.Success);
                await OnCompletion.InvokeAsync();
            }
            else
            {
                Snackbar.Add("Encountered unknown error", MudBlazor.Severity.Error);
                IsAccepting = false;
            }
        }
    }
}
