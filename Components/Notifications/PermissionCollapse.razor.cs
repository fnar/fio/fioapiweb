﻿using Microsoft.AspNetCore.Components;

namespace FIOAPIWeb.Components.Notifications
{
    public partial class PermissionCollapse
    {
        [Parameter]
        public Payloads.Permissions Perms { get; set; } = new();

        public bool IsExpanded { get; set; } = false;

        public void OnExpandCollapse()
        {
            IsExpanded = !IsExpanded;
        }
    }
}
