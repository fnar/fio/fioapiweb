﻿using System.Net;
using System.Text.Json;

using Microsoft.AspNetCore.Components;
using MudBlazor;

namespace FIOAPIWeb.Components.Account
{
    public partial class InviteToGroupDialog
    {
        [CascadingParameter]
        MudDialogInstance MudDialog { get; set; } = null!;

        [Parameter]
        public bool IsAdminDialog { get; set; } = false;

        [Parameter]
        public Payloads.Group Group { get; set; } = new();

        [Parameter]
        public List<string> AllFIOUsers { get; set; } = new();

        private List<string> EligibleUsers { get; set; } = new();

        private IEnumerable<string> SelectedUsers { get; set; } = new List<string>();

        protected override void OnInitialized()
        {
            base.OnInitialized();
            UserAppState.OnChange += StateHasChanged;

            // Admins are GroupOwner and existing Admins
            var GroupAdminUserNames = Group.Admins.Select(a => a.UserName).ToList();
            GroupAdminUserNames.Add(Group.GroupOwner);

            if (IsAdminDialog)
            {
                var PendingAdminInviteUserNames = Group.PendingInvites
                    .Where(pi => pi.Admin)
                    .Select(pi => pi.UserName)
                    .ToList();

                EligibleUsers = AllFIOUsers
                    .Except(GroupAdminUserNames, StringComparer.OrdinalIgnoreCase)
                    .Except(PendingAdminInviteUserNames, StringComparer.OrdinalIgnoreCase)
                    .ToList();
            }
            else
            {
                var PendingInviteUsernames = Group.PendingInvites
                    .Select(pi => pi.UserName)
                    .ToList();

                var GroupNormalUsers = Group.Users.Select(u => u.UserName).ToList();
                var ExistingUsers = GroupAdminUserNames.Union(GroupNormalUsers);
                EligibleUsers = AllFIOUsers
                    .Except(ExistingUsers, StringComparer.OrdinalIgnoreCase)
                    .Except(PendingInviteUsernames, StringComparer.OrdinalIgnoreCase)
                    .ToList();
            }
        }

        public void Dispose()
        {
            UserAppState.OnChange -= StateHasChanged;
        }

        private bool InviteButtonDisabled
        {
            get
            {
                return !SelectedUsers.Any() || IsInviting;
            }
        }

        private bool IsInviting { get; set; } = false;

        void OnCancel() => MudDialog.Cancel();

        async Task OnInviteAdmins()
        {
            IsInviting = true;
            StateHasChanged();

            var payload = new Payloads.Invite();
            payload.GroupId = Group.GroupId;
            payload.Invites = SelectedUsers.Select(su => new Payloads.UserInvite
            {
                Admin = IsAdminDialog,
                UserName = su
            }).ToList();

            var inviteRequest = new Web.Request(HttpMethod.Post, "/group/invite", UserAppState.BearerToken, JsonSerializer.Serialize(payload));
            await inviteRequest.GetResultNoResponse();
            if (inviteRequest.StatusCode == HttpStatusCode.OK)
            {
                string type = IsAdminDialog ? "admin" : "user";
                string suffix = SelectedUsers.Count() > 1 ? "s" : "";
                Snackbar.Add($"Invited {SelectedUsers.Count()} {type}{suffix}", Severity.Success);
                MudDialog.Close();
            }
            else
            {
                Snackbar.Add("Unknown error occurred", Severity.Error);
                MudDialog.Cancel();
            }

            IsInviting = false;
            StateHasChanged();
        }
    }
}
