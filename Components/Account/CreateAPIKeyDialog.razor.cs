﻿using FIOAPIWeb.Payloads;
using Microsoft.AspNetCore.Components;
using MudBlazor;
using System.Net;
using System.Text.Json;

using FIOAPIWeb.Components.Shared;

namespace FIOAPIWeb.Components.Account
{
    public partial class CreateAPIKeyDialog
    {
        [CascadingParameter]
        MudDialogInstance MudDialog { get; set; } = null!;

        public string ApplicationName { get; set; } = "";

        public string Password { get; set; } = "";

        public bool AllowWrites { get; set; } = false;

        protected override void OnInitialized()
        {
            base.OnInitialized();
            UserAppState.OnChange += StateHasChanged;
        }

        public void Dispose()
        {
            UserAppState.OnChange -= StateHasChanged;
        }

        void OnCancel() => MudDialog.Cancel();

        public bool CreateButtonDisabled
        {
            get
            {
                return !Common.IsCorrectLength(ApplicationName, 3, 128) ||
                       !Common.IsPasswordValid(Password) ||
                       IsCreatingAPIKey;
            }
        }

        public bool IsCreatingAPIKey = false;
        async Task OnCreateAPIKey()
        {
            IsCreatingAPIKey = true;
            StateHasChanged();

            var payload = new Payloads.CreateAPIKey()
            {
                UserName = UserAppState.UserName!,
                Password = Password,
                ApplicationName = ApplicationName,
                AllowWrites = AllowWrites
            };

            var createAPIKeyRequest = new Web.Request(HttpMethod.Post, "/auth/createapikey", UserAppState.BearerToken, JsonSerializer.Serialize(payload));
            var response = await createAPIKeyRequest.GetResponseAsync<CreateAPIKeyResponse>();
            switch(createAPIKeyRequest.StatusCode)
            {
                case HttpStatusCode.OK:
                    Snackbar.Add("APIKey Created", Severity.Success);
                    break;
                case HttpStatusCode.Unauthorized:
                    Snackbar.Add("Incorrect password", Severity.Error);
                    break;
                case HttpStatusCode.NotAcceptable:
                    Snackbar.Add("At maximum APIKey count. Cannot create more.", Severity.Error);
                    break;
                default:
                    Snackbar.Add("Encountered unknown error while creating an API Key", Severity.Error);
                    break;
            }

            IsCreatingAPIKey = false;
            StateHasChanged();

            if (createAPIKeyRequest.StatusCode == HttpStatusCode.OK && response != null)
            {
                MudDialog.Close(DialogResult.Ok(new APIKey
                {
                    Key = response.APIKey,
                    UserName = UserAppState.UserName!,
                    Application = ApplicationName,
                    AllowWrites = AllowWrites,
                    CreateTime = DateTime.UtcNow,
                }));
            }
            else
            {
                MudDialog.Cancel();
            }
        }
    }
}
