﻿using Microsoft.AspNetCore.Components;
using MudBlazor;
using System.Text.Json;

using FIOAPIWeb.Components.Shared;

namespace FIOAPIWeb.Components.Account
{
    public partial class SetDiscordNameDialog
    {
        static MarkupString SpacerString = new MarkupString(string.Concat(Enumerable.Repeat("&nbsp;", 100)));

        [CascadingParameter]
        MudDialogInstance MudDialog { get; set; } = null!;

        public string DiscordName { get; set; } = "";

        protected override void OnInitialized()
        {
            base.OnInitialized();
            UserAppState.OnChange += StateHasChanged;
            DiscordName = UserAppState.DiscordName;
        }

        public void Dispose()
        {
            UserAppState.OnChange -= StateHasChanged;
        }

        void Cancel() => MudDialog.Cancel();

        public bool DiscordNameHasError
        {
            get
            {
                return !Common.IsCorrectLength(DiscordName, 2, 32, true);
            }
        }

        public string DiscordNameErrorText
        {
            get
            {
                return Common.GetLengthError(2, 32);
            }
        }

        public bool SetDiscordNameDisabled
        {
            get
            {
                return DiscordNameHasError || IsSettingDiscordName;
            }
        }

        private bool IsSettingDiscordName { get; set; } = false;

        async Task SetDiscordName()
        {
            IsSettingDiscordName = true;
            {
                var setDiscordPayload = new Payloads.SetDiscord
                {
                    DiscordName = DiscordName,
                };

                var setDiscordNameRequest = new Web.Request(HttpMethod.Put, "/auth/discord", UserAppState.BearerToken, JsonSerializer.Serialize(setDiscordPayload));
                await setDiscordNameRequest.GetResultNoResponse();
                if (setDiscordNameRequest.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    await UserAppState.RefreshDiscordName();
                    Snackbar.Add("Discord Name set", Severity.Success);
                }
                else
                {
                    Snackbar.Add("Failed to change Discord Name", Severity.Error);
                }
            }
            IsSettingDiscordName = false;

            MudDialog.Close();
        }
    }
}
