﻿using FIOAPIWeb.Payloads;
using Microsoft.AspNetCore.Components;
using MudBlazor;
using System.Net;
using System.Text.Json;

namespace FIOAPIWeb.Components.Account
{
    public partial class CreatePermissionDialog
    {
        [CascadingParameter]
        protected MudDialogInstance MudDialog { get; set; } = null!;

        [Parameter]
        public List<string> AllFIOUsers { get; set; } = new();

        [Parameter]
        public List<string> CurrentPermissionUsers { get; set; } = new();

        public List<string> EligibleUsers { get; set; } = new();

        public string? SelectedUser { get; set; } = null;

        public Permissions Perms { get; set; } = new();

        protected override void OnInitialized()
        {
            base.OnInitialized();
            UserAppState.OnChange += StateHasChanged;

            EligibleUsers = AllFIOUsers.ToList();   // ToList for copy

            // Add "all users" wildcard
            EligibleUsers.Add("*");

            // Remove ourselves
            EligibleUsers.RemoveAll(u => u.Equals(UserAppState.UserName, StringComparison.OrdinalIgnoreCase));

            // Remove existing invites
            var ExistingPermUsers = CurrentPermissionUsers.ToList(); // Copy
            EligibleUsers.RemoveAll(u => ExistingPermUsers.Contains(u, StringComparer.OrdinalIgnoreCase));
        }

        public void Dispose()
        {
            UserAppState.OnChange -= StateHasChanged;
        }

        private bool CreateButtonDisabled
        {
            get
            {
                return SelectedUser == null || IsCreatingPermission;
            }
        }

        private bool IsCreatingPermission { get; set; } = false;

        void OnCancel() => MudDialog.Cancel();

        async Task OnCreate()
        {
            IsCreatingPermission = true;
            StateHasChanged();

            var payload = new Grant()
            {
                UserName = SelectedUser!,
                Permissions = Perms
            };

            var request = new Web.Request(HttpMethod.Put, "/permission/grant", UserAppState.BearerToken, JsonSerializer.Serialize(payload));
            await request.GetResultNoResponse();
            if (request.StatusCode == HttpStatusCode.OK)
            {
                Snackbar.Add("Created Permission", Severity.Success);

                PermissionResponse pm = new()
                {
                    GrantorUserName = UserAppState.UserName!,
                    GranteeUserName = SelectedUser!,
                    Permissions = Perms
                };
                MudDialog.Close(DialogResult.Ok(pm));
            }
            else
            {
                Snackbar.Add("Encountered unknown error", Severity.Error);
                MudDialog.Cancel();
            }

            IsCreatingPermission = false;
            StateHasChanged();
        }
    }
}
