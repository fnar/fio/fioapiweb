﻿using System.Net;
using System.Text.Json;

using Microsoft.AspNetCore.Components;
using MudBlazor;

namespace FIOAPIWeb.Components.Account
{
    public partial class EditPermissionsExpander
    {
        [Parameter]
        public string UserName { get; set; } = "";

#pragma warning disable BL0007
        [Parameter]
        public Payloads.Permissions Perms
        {
            get => _Perms;
            set
            {
                if (_Perms != value)
                {
                    _Perms = value;
                    PermsChanged.InvokeAsync(value);
                }
            }
        }
        private Payloads.Permissions _Perms = new();
#pragma warning restore BL0007

        [Parameter]
        public EventCallback<Payloads.Permissions> PermsChanged { get; set; }

        public Payloads.Permissions WorkingPerms { get; set; } = new();

        public bool IsExpanded { get; set; } = false;

        public bool IsSaving { get; set; } = false;

        protected override void OnInitialized()
        {
            base.OnInitialized();
            UserAppState.OnChange += StateHasChanged;

            WorkingPerms = new(Perms);
        }

        public void Dispose()
        {
            UserAppState.OnChange -= StateHasChanged;
        }

        public void OnExpandCollapse()
        {
            IsExpanded = !IsExpanded;

            if (!IsExpanded)
            {
                // Discard
                WorkingPerms = new(Perms);
                StateHasChanged();
            }
        }

        async Task OnSave()
        {
            IsSaving = true;
            StateHasChanged();

            var payload = new Payloads.Grant()
            {
                UserName = UserName,
                Permissions = WorkingPerms
            };

            var request = new Web.Request(HttpMethod.Put, "/permission/grant", UserAppState.BearerToken, JsonSerializer.Serialize(payload));
            await request.GetResultNoResponse();
            if (request.StatusCode == HttpStatusCode.OK)
            {
                Perms = WorkingPerms;
                Snackbar.Add("Permission Updated", Severity.Success);
                IsExpanded = false;
            }
            else
            {
                IsExpanded = false;
                Snackbar.Add("Encountered unknown error", Severity.Error);
            }

            IsSaving = false;
            StateHasChanged();
        }
    }
}
