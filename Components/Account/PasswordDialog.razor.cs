﻿using FIOAPIWeb.Components.Shared;
using Microsoft.AspNetCore.Components;

namespace FIOAPIWeb.Components.Account
{
    public partial class PasswordDialog : SimpleConfirmDialog
    {
        [Parameter]
        public Func<string, Task<bool>>? AcceptPasswordFunc { get; set; }

        public string Password { get; set; } = "";

        private bool AcceptButtonDisabled
        {
            get
            {
                return !Common.IsPasswordValid(Password) || IsProcessing;
            }
        }

        public override async Task OnAcceptButton()
        {
            IsAccepting = true;
            StateHasChanged();

            bool Success = false;
            if (AcceptPasswordFunc != null)
            {
                Success = await AcceptPasswordFunc(Password);
            }

            IsAccepting = false;

            if (Success)
            {
                MudDialog.Close();
            }
            else
            {
                MudDialog.Cancel();
            }
            StateHasChanged();
        }
    }
}
