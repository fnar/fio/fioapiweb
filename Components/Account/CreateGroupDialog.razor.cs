﻿using Microsoft.AspNetCore.Components;
using MudBlazor;
using System.Net;
using System.Text.Json;

using FIOAPIWeb.Components.Shared;

namespace FIOAPIWeb.Components.Account
{
    public partial class CreateGroupDialog
    {
        [CascadingParameter]
        MudDialogInstance MudDialog { get; set; } = null!;

        #region RequestId
        public int RequestedId { get; set; } = 0;

        public bool RequestedIdHasError
        {
            get
            {
                return RequestedId < 0 || RequestedId > 999999;
            }
        }

        public string RequestedIdErrorText
        {
            get
            {
                return RequestedIdHasError ? "Must be 0 or in range [0, 999999]" : "";
            }
        }
        #endregion

        public string GroupName { get; set; } = "";

        public Payloads.Permissions Perms { get; set; } = new();

        protected override void OnInitialized()
        {
            base.OnInitialized();
            UserAppState.OnChange += StateHasChanged;
        }

        public void Dispose()
        {
            UserAppState.OnChange -= StateHasChanged;
        }

        public bool CreateButtonDisabled
        {
            get
            {
                return IsCreatingGroup ||
                       RequestedIdHasError ||
                       !Common.IsCorrectLength(GroupName, 3, 16);
            }
        } 

        public bool IsCreatingGroup = false;

        private void OnCancel() => MudDialog.Cancel();
        private async void OnCreateGroup()
        {
            IsCreatingGroup = true;
            StateHasChanged();

            var payload = new Payloads.Create()
            {
                RequestedId = RequestedId,
                GroupName = GroupName,
                Invites = new(),
                Permissions = Perms
            };

            var createRequest = new Web.Request(HttpMethod.Post, "/group/create", UserAppState.BearerToken, JsonSerializer.Serialize(payload));
            var createResponse = await createRequest.GetResponseAsync<Payloads.CreateResponse>();
            if (createResponse != null && createRequest.StatusCode == HttpStatusCode.OK)
            {
                Snackbar.Add("Group Created", Severity.Success);
                MudDialog.Close(DialogResult.Ok(createResponse));
            }
            else if (createRequest.StatusCode == HttpStatusCode.NotAcceptable)
            {
                Snackbar.Add("Reached maximum threshold of groups or requested id is already present", Severity.Error);
                MudDialog.Cancel();
            }
            else
            {
                Snackbar.Add("Unknown failure occurred", Severity.Error);
                MudDialog.Cancel();
            }

            IsCreatingGroup = false;
            StateHasChanged();
        }
    }
}
