﻿using System.Text.Json;

using Microsoft.AspNetCore.Components;
using MudBlazor;

using FIOAPIWeb.Components.Shared;

namespace FIOAPIWeb.Components.Account
{
    public partial class ChangePasswordDialog
    {
        [CascadingParameter]
        MudDialogInstance MudDialog { get; set; } = null!;

        public string OldPassword { get; set; } = "";

        public string NewPassword1 { get; set; } = "";

        public string NewPassword2 { get; set; } = "";

        public Severity AlertSeverity 
        { 
            get
            {
                return (NewPassword1 == NewPassword2) ? Severity.Info : Severity.Error;
            }
        }

        public string AlertStatusText
        {
            get
            {
                return (NewPassword1 == NewPassword2) ? "Passwords Match" : "Passwords Do Not Match";
            }
        }

        private bool ChangePasswordDisabled
        {
            get
            {
                return !Common.IsPasswordValid(OldPassword) || 
                       !Common.IsPasswordValid(NewPassword1) || 
                       !Common.IsPasswordValid(NewPassword2) || 
                       NewPassword1 != NewPassword2 ||
                       IsChangingPassword;
            }
        }

        private bool IsChangingPassword { get; set; } = false;

        protected override void OnInitialized()
        {
            base.OnInitialized();
            UserAppState.OnChange += StateHasChanged;
        }

        public void Dispose()
        {
            UserAppState.OnChange -= StateHasChanged;
        }

        void Cancel() => MudDialog.Cancel();

        async Task ChangePassword()
        {
            IsChangingPassword = true;
            StateHasChanged();
            {
                var changePasswordPayload = new Payloads.ChangePassword
                {
                    OldPassword = OldPassword,
                    NewPassword = NewPassword1
                };

                var changePasswordRequest = new Web.Request(HttpMethod.Post, "/auth/changepassword", UserAppState.BearerToken, JsonSerializer.Serialize(changePasswordPayload));
                await changePasswordRequest.GetResultNoResponse();
                if (changePasswordRequest.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    Snackbar.Add("Password changed", Severity.Success);
                }
                else
                {
                    Snackbar.Add("Failed to change password", Severity.Error);
                }
            }
            IsChangingPassword = false;
            StateHasChanged();

            MudDialog.Close();
        }
    }
}
