﻿using Microsoft.AspNetCore.Components;

namespace FIOAPIWeb.Components.PlanetSearch
{
	public partial class MatchPlanetSettingsComponent
	{
		[Parameter]
#pragma warning disable BL0007 // Component parameters should be auto properties
		public bool Rocky
#pragma warning restore BL0007 // Component parameters should be auto properties
		{
			get => _Rocky;
			set
			{
				bool oldValue = _Rocky;	// This is necessary to avoid infinite redraw due to EnforceOneSelection
				if (_Rocky != value)
				{
					_Rocky = value;
                    EnforceOneSelection();
					if (oldValue != _Rocky)
					{
                        RockyChanged.InvokeAsync(value);
                    }
				}
			}
		}
		private bool _Rocky = false;

		[Parameter]
		public EventCallback<bool> RockyChanged { get; set; }

		[Parameter]
#pragma warning disable BL0007 // Component parameters should be auto properties
		public bool Gaseous
#pragma warning restore BL0007 // Component parameters should be auto properties
		{
			get => _Gaseous;
			set
			{
				if (_Gaseous != value)
				{
					_Gaseous = value;
                    EnforceOneSelection();
                    GaseousChanged.InvokeAsync(value);
				}
			}
		}
		private bool _Gaseous = false;

		[Parameter]
		public EventCallback<bool> GaseousChanged { get; set; }

		[Parameter]
#pragma warning disable BL0007 // Component parameters should be auto properties
		public bool LowGravity
#pragma warning restore BL0007 // Component parameters should be auto properties
		{
			get => _LowGravity;
			set
			{
				if (_LowGravity != value)
				{
					_LowGravity = value;
                    EnforceOneSelection();
                    LowGravityChanged.InvokeAsync(value);
				}
			}
		}
		private bool _LowGravity = false;

		[Parameter]
		public EventCallback<bool> LowGravityChanged { get; set; }

		[Parameter]
#pragma warning disable BL0007 // Component parameters should be auto properties
		public bool HighGravity
#pragma warning restore BL0007 // Component parameters should be auto properties
		{
			get => _HighGravity;
			set
			{
				if (_HighGravity != value)
				{
					_HighGravity = value;
                    EnforceOneSelection();
                    HighGravityChanged.InvokeAsync(value);
				}
			}
		}
		private bool _HighGravity = false;

		[Parameter]
		public EventCallback<bool> HighGravityChanged { get; set; }

		[Parameter]
#pragma warning disable BL0007 // Component parameters should be auto properties
		public bool LowPressure
#pragma warning restore BL0007 // Component parameters should be auto properties
		{
			get => _LowPressure;
			set
			{
				if (_LowPressure != value)
				{
					_LowPressure = value;
                    EnforceOneSelection();
                    LowPressureChanged.InvokeAsync(value);
				}
			}
		}
		private bool _LowPressure = false;

		[Parameter]
		public EventCallback<bool> LowPressureChanged { get; set; }

		[Parameter]
#pragma warning disable BL0007 // Component parameters should be auto properties
		public bool HighPressure
#pragma warning restore BL0007 // Component parameters should be auto properties
		{
			get => _HighPressure;
			set
			{
				if (_HighPressure != value)
				{
					_HighPressure = value;
                    EnforceOneSelection();
                    HighPressureChanged.InvokeAsync(value);
				}
			}
		}
		private bool _HighPressure = false;

		[Parameter]
		public EventCallback<bool> HighPressureChanged { get; set; }

		[Parameter]
#pragma warning disable BL0007 // Component parameters should be auto properties
		public bool LowTemperature
#pragma warning restore BL0007 // Component parameters should be auto properties
		{
			get => _LowTemperature;
			set
			{
				if (_LowTemperature != value)
				{
					_LowTemperature = value;
                    EnforceOneSelection();
                    LowTemperatureChanged.InvokeAsync(value);
				}
			}
		}
		private bool _LowTemperature = false;

		[Parameter]
		public EventCallback<bool> LowTemperatureChanged { get; set; }

		[Parameter]
#pragma warning disable BL0007 // Component parameters should be auto properties
		public bool HighTemperature
#pragma warning restore BL0007 // Component parameters should be auto properties
		{
			get => _HighTemperature;
			set
			{
				if (_HighTemperature != value)
				{
					_HighTemperature = value;
					EnforceOneSelection();
					HighTemperatureChanged.InvokeAsync(value);
				}
			}
		}
		private bool _HighTemperature = false;

		[Parameter]
		public EventCallback<bool> HighTemperatureChanged { get; set; }

        protected override void OnInitialized()
        {
            base.OnInitialized();

			EnforceOneSelection();
        }

        private void EnforceOneSelection()
		{
			if (!Rocky && !Gaseous && !LowGravity && !HighGravity && !LowPressure && !HighPressure && !LowTemperature && !HighTemperature)
			{
				Rocky = true;
				RockyChanged.InvokeAsync(Rocky);
			}
		}
	}
}
