﻿using FIOAPIWeb.PageHelpers.PlanetSearch;
using Microsoft.AspNetCore.Components;

namespace FIOAPIWeb.Components.PlanetSearch
{
    public partial class PlanetPreviewComponent
    {
        [Parameter]
        public List<PlanetSearchModelResource>? Resources { get; set; }

        [Parameter]
        public double ProductionEfficiency { get; set; }

        private string GetHexForColor(System.Drawing.Color c)
        {
            return "#" + c.R.ToString("X2") + c.G.ToString("X2") + c.B.ToString("X2");
        }

        private string GetConcentrationColor(double Concentration)
        {
            double alpha = Concentration;
            int component = (int)(255.0 * alpha);
            int background = (int)(255.0 * (1.0 - alpha));
            System.Drawing.Color c = System.Drawing.Color.FromArgb(background, component + background, background);
            return GetHexForColor(c);
        }

        private string GetDailyExtractionColor(double Concentration)
        {
            double alpha = Concentration;
            int component = (int)(255.0 * alpha);
            int background = (int)(255.0 * (1.0 - alpha));
            System.Drawing.Color c = System.Drawing.Color.FromArgb(component + background, component + background, background);
            return GetHexForColor(c);
        }

        private string Get300AColor(double ThreeHundredA)
        {
            double alpha = Math.Clamp(ThreeHundredA / 2000.0, 0.0, 1.0);
            int component = (int)(255.0 * alpha);
            int background = (int)(255.0 * (1.0 - alpha));
            System.Drawing.Color c = System.Drawing.Color.FromArgb(background, component + background, component + background);
            return GetHexForColor(c);
        }
    }
}
