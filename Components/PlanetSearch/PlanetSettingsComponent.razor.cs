﻿using Microsoft.AspNetCore.Components;

namespace FIOAPIWeb.Components.PlanetSearch
{
	public partial class PlanetSettingsComponent
	{
		[Parameter]
#pragma warning disable BL0007 // Component parameters should be auto properties
		public bool Fertile
#pragma warning restore BL0007 // Component parameters should be auto properties
		{
			get => _Fertile;
			set
			{
				if (_Fertile != value)
				{
					_Fertile = value;
					FertileChanged.InvokeAsync(value);
				}
			}
		}
		private bool _Fertile = false;

		[Parameter]
		public EventCallback<bool> FertileChanged { get; set; }

		[Parameter]
#pragma warning disable BL0007 // Component parameters should be auto properties
		public bool LocalMarket
#pragma warning restore BL0007 // Component parameters should be auto properties
		{
			get => _LocalMarket;
			set
			{
				if (_LocalMarket != value)
				{
					_LocalMarket = value;
					LocalMarketChanged.InvokeAsync(value);
				}
			}
		}
		private bool _LocalMarket = false;

		[Parameter]
		public EventCallback<bool> LocalMarketChanged { get; set; }

		[Parameter]
#pragma warning disable BL0007 // Component parameters should be auto properties
		public bool COGC
#pragma warning restore BL0007 // Component parameters should be auto properties
		{
			get => _COGC;
			set
			{
				if (_COGC != value)
				{
					_COGC = value;
					COGCChanged.InvokeAsync(value);
				}
			}
		}
		private bool _COGC = false;

		[Parameter]
		public EventCallback<bool> COGCChanged { get; set; }

		[Parameter]
#pragma warning disable BL0007 // Component parameters should be auto properties
		public bool Warehouse
#pragma warning restore BL0007 // Component parameters should be auto properties
		{
			get => _Warehouse;
			set
			{
				if (_Warehouse != value)
				{
					_Warehouse = value;
					WarehouseChanged.InvokeAsync(value);
				}
			}
		}
		private bool _Warehouse = false;

		[Parameter]
		public EventCallback<bool> WarehouseChanged { get; set; }

		[Parameter]
#pragma warning disable BL0007 // Component parameters should be auto properties
		public bool AdminCenter
#pragma warning restore BL0007 // Component parameters should be auto properties
		{
			get => _AdminCenter;
			set
			{
				if (_AdminCenter != value)
				{
					_AdminCenter = value;
					AdminCenterChanged.InvokeAsync(value);
				}
			}
		}
		private bool _AdminCenter = false;

		[Parameter]
		public EventCallback<bool> AdminCenterChanged { get; set; }

		[Parameter]
#pragma warning disable BL0007 // Component parameters should be auto properties
		public bool Shipyard
#pragma warning restore BL0007 // Component parameters should be auto properties
		{
			get => _Shipyard;
			set
			{
				if (_Shipyard != value)
				{
					_Shipyard = value;
					ShipyardChanged.InvokeAsync(value);
				}
			}
		}
		private bool _Shipyard = false;

		[Parameter]
		public EventCallback<bool> ShipyardChanged { get; set; }
	}
}
