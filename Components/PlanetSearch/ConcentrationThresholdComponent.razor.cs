﻿using Microsoft.AspNetCore.Components;

namespace FIOAPIWeb.Components.PlanetSearch
{
	public partial class ConcentrationThresholdComponent
	{
		[Parameter]
#pragma warning disable BL0007 // Component parameters should be auto properties
		public double ConcentrationThreshold
#pragma warning restore BL0007 // Component parameters should be auto properties
		{
			get => _ConcentrationThreshold;
			set
			{
				if (_ConcentrationThreshold != value)
				{
					_ConcentrationThreshold = value;
					ConcentrationThresholdChanged.InvokeAsync(value);
				}
			}
		}
		private double _ConcentrationThreshold = 0.0;

		[Parameter]
		public EventCallback<double> ConcentrationThresholdChanged { get; set; }
	}
}
