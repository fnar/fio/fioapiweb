﻿using Microsoft.AspNetCore.Components;

namespace FIOAPIWeb.Components.PlanetSearch
{
	public partial class ProductionEfficiencyComponent
	{
		[Parameter]
#pragma warning disable BL0007 // Component parameters should be auto properties
		public double ProductionEfficiency
#pragma warning restore BL0007 // Component parameters should be auto properties
		{
			get => _ProductionEfficiency;
			set
			{
				if (_ProductionEfficiency != value)
				{
					_ProductionEfficiency = value;
					ProductionEfficiencyChanged.InvokeAsync(value);
				}
			}
		}
		private double _ProductionEfficiency = 1000.0;

		[Parameter]
		public EventCallback<double> ProductionEfficiencyChanged { get; set; }
	}
}
