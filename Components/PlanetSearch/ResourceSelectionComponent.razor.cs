﻿using Microsoft.AspNetCore.Components;

namespace FIOAPIWeb.Components.PlanetSearch
{
	public partial class ResourceSelectionComponent
	{
		[Parameter]
#pragma warning disable BL0007 // Component parameters should be auto properties
		public IEnumerable<string> SelectedMaterialTickers
#pragma warning restore BL0007 // Component parameters should be auto properties
		{
			get => _SelectedMaterialTickers;
			set
			{
				if (_SelectedMaterialTickers != value)
				{
					_SelectedMaterialTickers = value;
					SelectedMaterialTickersChanged.InvokeAsync(value);
				}
			}
		}
		private IEnumerable<string> _SelectedMaterialTickers = new HashSet<string>();

		[Parameter]
		public EventCallback<IEnumerable<string>> SelectedMaterialTickersChanged { get; set; }

		[Parameter]
		public List<string> PlanetMaterialTickers 
		{ 
			get; 
			set; 
		} = new();
	}
}
