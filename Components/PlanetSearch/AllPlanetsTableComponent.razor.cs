﻿using FIOAPIWeb.PageHelpers.PlanetSearch;
using Microsoft.AspNetCore.Components;
using MudBlazor;

namespace FIOAPIWeb.Components.PlanetSearch
{
    public partial class AllPlanetsTableComponent
    {
        [Parameter]
        public List<PlanetSearchModel> AllPlanets { get; set; } = new();

        [Parameter]
#pragma warning disable BL0007 // Component parameters should be auto properties
        public PlanetSearchModel SelectedPlanet
#pragma warning restore BL0007 // Component parameters should be auto properties
        {
            get => _SelectedPlanet;
            set
            {
                if (_SelectedPlanet != value)
                {
                    _SelectedPlanet = value;
                    SelectedPlanetChanged.InvokeAsync(value);
                }
            }
        }
        private PlanetSearchModel _SelectedPlanet = null!;

        [Parameter]
        public EventCallback<PlanetSearchModel> SelectedPlanetChanged { get; set; }

        [Parameter]
#pragma warning disable BL0007 // Component parameters should be auto properties
        public string SearchFilter
#pragma warning restore BL0007 // Component parameters should be auto properties

        {
            get => _SearchFilter;
            set
            {
                if (_SearchFilter != value)
                {
                    _SearchFilter = value;
                    SearchFilterChanged.InvokeAsync(value);
                }
            }
        }
        private string _SearchFilter = "";

        [Parameter]
        public EventCallback<string> SearchFilterChanged { get; set; }

        [Parameter]
#pragma warning disable BL0007 // Component parameters should be auto properties
        public PlanetSearchState PlanetSearchState
#pragma warning restore BL0007 // Component parameters should be auto properties
        {
            get => _PlanetSearchState; 
            set
            {
                if (_PlanetSearchState != value)
                {
                    _PlanetSearchState = value;
                    RefreshFromSearchState();
                }
            }
        }
        private PlanetSearchState _PlanetSearchState = null!;

        [Parameter]
        public double ProductionEfficiency { get; set; } = 100.0;

        [Parameter]
        public List<string> ExchangeLocationNames { get; set; } = new();

        private List<PlanetSearchModel> FilteredPlanets { get; set; } = new();

        protected override void OnAfterRender(bool firstRender)
        {
            base.OnAfterRender(firstRender);

            if (!firstRender)
            {
                RefreshFromSearchState();
            }
        }

        private void RefreshFromSearchState()
        {
            var Filter = AllPlanets.AsEnumerable();

            if (PlanetSearchState.SearchFilter.Length > 0)
            {
                Filter = Filter.Where(p => p.DisplayName.Contains(PlanetSearchState.SearchFilter, StringComparison.OrdinalIgnoreCase));
            }

            if (PlanetSearchState.SelectedMaterials.Count > 0)
            {
                // Make sure we have all the selected resources on the planet
                Filter = Filter
                    .Where(p => !PlanetSearchState.SelectedMaterials
                                    .Except(p.Resources.Select(r => r.Material))    
                                .Any())
                    // Make sure all the selected materials are > than the concentration threshold
                    .Where(p => p.Resources
                                    .All(r => !PlanetSearchState.SelectedMaterials
                                                .Contains(r.Material) || (r.Concentration * 100.0) > PlanetSearchState.ConcentrationThreshold));  
            }

            // Must have settings
            if (PlanetSearchState.Fertile)
            {
                Filter = Filter.Where(p => p.IsFertile);
            }
            if (PlanetSearchState.LocalMarket)
            {
                Filter = Filter.Where(p => p.HasLocalMarket);
            }
            if (PlanetSearchState.COGC)
            {
                Filter = Filter.Where(p => p.HasChamberOfCommerce);
            }
            if (PlanetSearchState.Warehouse)
            {
                Filter = Filter.Where(p => p.HasWarehouse);
            }
            if (PlanetSearchState.AdminCenter)
            {
                Filter = Filter.Where(p => p.HasAdministrationCenter);
            }
            if (PlanetSearchState.Shipyard)
            {
                Filter = Filter.Where(p => p.HasShipyard);
            }

            // Match planet settings
            if (!PlanetSearchState.Rocky)
            {
                Filter = Filter.Where(p => p.SurfaceType != "Rocky");
            }
            if (!PlanetSearchState.Gaseous)
            {
                Filter = Filter.Where(p => p.SurfaceType != "Gaseous");
            }
            if (!PlanetSearchState.LowGravity)
            {
                Filter = Filter.Where(p => p.Gravity != "Low");
            }
            if (!PlanetSearchState.HighGravity) 
            {
                Filter = Filter.Where(p => p.Gravity != "High");
            }
            if (!PlanetSearchState.LowTemperature)
            {
                Filter = Filter.Where(p => p.Temperature != "Low");
            }
            if (!PlanetSearchState.HighTemperature)
            {
                Filter = Filter.Where(p => p.Temperature != "High");
            }
            if (!PlanetSearchState.LowPressure)
            {
                Filter = Filter.Where(p => p.Pressure != "Low");
            }
            if (!PlanetSearchState.HighPressure)
            {
                Filter = Filter.Where(p => p.Pressure != "High");
            }

            FilteredPlanets = Filter.ToList();
            StateHasChanged();
        }

        private async Task OnOpenChanged(bool Opened)
        {
            if (!Opened)
            {
                await PlanetSearchState.Save();
            }
        }

        private MudTable<PlanetSearchModel> PlanetDataTable { get; set; } = null!;
    }
}
