﻿using Microsoft.AspNetCore.Components;
using System.Text;
using System.Text.Json.Serialization;
using System.Web;

namespace FIOAPIWeb.PageHelpers.PlanetSearch
{
    public class PlanetSearchState
    {
        /// <summary>
        /// Bump version number if PlanetSearchState updates.
        /// If we attempt to load an old version, we ignore it.
        /// </summary>
        private static readonly int CurrentVersion = 1;

        private const string PlanetSearchStateLocalStorageKey = "PlanetSearchState";

        public int Version { get; set; } = CurrentVersion;

        #region Active Settings
        public List<string> SelectedMaterials { get; set; } = new List<string>();
        public double ConcentrationThreshold { get; set; } = 0.0;
        public double ProductionEfficiency { get; set; } = 100.0;

        // Must have settings
        public bool Fertile { get; set; } = false;
        public bool LocalMarket { get; set; } = false;
        public bool COGC { get; set; } = false;
        public bool Warehouse { get; set; } = false;
        public bool AdminCenter { get; set; } = false;
        public bool Shipyard { get; set; } = false;

        public string JumpDistanceSourcePlanet { get; set; } = "";
        public int? JumpDistanceLimit { get; set; } = null;

        // Match planet settings
        public bool Rocky { get; set; } = false;
        public bool Gaseous { get; set; } = false;
        public bool LowGravity { get; set; } = false;
        public bool HighGravity { get; set; } = false;
        public bool LowPressure { get; set; } = false;
        public bool HighPressure { get; set; } = false;
        public bool LowTemperature { get; set; } = false;
        public bool HighTemperature { get; set; } = false;

        public string SearchFilter { get; set; } = "";
        #endregion

        #region Show Settings
        // Visibility
        public bool ShowFertility { get; set; } = true;
        public bool ShowSurfaceType { get; set; } = true;
        public bool ShowGravity { get; set; } = true;
        public bool ShowPressure { get; set; } = true;
        public bool ShowTemperature { get; set; } = true;
        public bool ShowLM { get; set; } = true;
        public bool ShowWAR { get; set; } = true;
        public bool ShowADM { get; set; } = true;
        public bool ShowSHY { get; set; } = true;
        public bool ShowCOGC { get; set; } = true;

        public List<bool> ShowExchangeJumps { get; set; } = new List<bool>();
        public bool ShowCustomJumpDistance { get; set; } = true;

        public bool ShowCurrency { get; set; } = false;
        public bool ShowTotalPioneers { get; set; } = false;
        public bool ShowUnemployedPioneers { get; set; } = false;
        public bool ShowTotalSettlers { get; set; } = false;
        public bool ShowUnemployedSettlers { get; set; } = false;
        public bool ShowTotalTechnicians { get; set; } = false;
        public bool ShowUnemployedTechnicians { get; set; } = false;
        public bool ShowTotalEngineers { get; set; } = false;
        public bool ShowUnemployedEngineers { get; set; } = false;
        public bool ShowTotalScientists { get; set; } = false;
        public bool ShowUnemployedScientists { get; set; } = false;
        public bool ShowTotalPlots { get; set; } = false;
        #endregion

        [JsonIgnore]
        private Blazored.LocalStorage.ILocalStorageService? LocalStorage;

        [JsonConstructor]
        private PlanetSearchState()
        {
            // Necessary for Json Serialization
        }

        public PlanetSearchState(Blazored.LocalStorage.ILocalStorageService LocalStorage)
        {
            this.LocalStorage = LocalStorage;
        }

        public async Task Load()
        {
            var LoadedPSS = await LocalStorage!.GetItemAsync<PlanetSearchState>(PlanetSearchStateLocalStorageKey);
            if (LoadedPSS?.Version == CurrentVersion)
            {
                SelectedMaterials = LoadedPSS.SelectedMaterials;
                ConcentrationThreshold = LoadedPSS.ConcentrationThreshold;
                ProductionEfficiency = LoadedPSS.ProductionEfficiency;

                Fertile = LoadedPSS.Fertile;
                LocalMarket = LoadedPSS.LocalMarket;
                COGC = LoadedPSS.COGC;
                Warehouse = LoadedPSS.Warehouse;
                AdminCenter = LoadedPSS.AdminCenter;
                Shipyard = LoadedPSS.Shipyard;

                JumpDistanceSourcePlanet = LoadedPSS.JumpDistanceSourcePlanet;
                JumpDistanceLimit = LoadedPSS.JumpDistanceLimit;

                Rocky = LoadedPSS.Rocky;
                Gaseous = LoadedPSS.Gaseous;
                LowGravity = LoadedPSS.LowGravity;
                HighGravity = LoadedPSS.HighGravity;
                LowPressure = LoadedPSS.LowPressure;
                HighPressure = LoadedPSS.HighPressure;
                LowTemperature = LoadedPSS.LowTemperature;
                HighTemperature = LoadedPSS.HighTemperature;

                SearchFilter = LoadedPSS.SearchFilter;

                ShowExchangeJumps = LoadedPSS.ShowExchangeJumps;
                ShowCustomJumpDistance = LoadedPSS.ShowCustomJumpDistance;

                ShowCurrency = LoadedPSS.ShowCurrency;
                ShowTotalPioneers = LoadedPSS.ShowTotalPioneers;
                ShowUnemployedPioneers = LoadedPSS.ShowUnemployedPioneers;
                ShowTotalSettlers = LoadedPSS.ShowTotalSettlers;
                ShowUnemployedSettlers = LoadedPSS.ShowUnemployedSettlers;
                ShowTotalTechnicians = LoadedPSS.ShowTotalTechnicians;
                ShowUnemployedTechnicians = LoadedPSS.ShowUnemployedTechnicians;
                ShowTotalEngineers = LoadedPSS.ShowTotalEngineers;
                ShowUnemployedEngineers = LoadedPSS.ShowUnemployedEngineers;
                ShowTotalScientists = LoadedPSS.ShowTotalScientists;
                ShowUnemployedScientists = LoadedPSS.ShowUnemployedScientists;
                ShowTotalPlots = LoadedPSS.ShowTotalPlots;
            }
        }

        public async Task Save()
        {
            await LocalStorage!.SetItemAsync(PlanetSearchStateLocalStorageKey, this);
        }

        public void Reset()
        {
            SelectedMaterials.Clear();
            ConcentrationThreshold = 0.0;
            ProductionEfficiency = 100.0;
            Fertile = false;
            LocalMarket = false;
            COGC = false;
            Warehouse = false;
            AdminCenter = false;
            Shipyard = false;
            JumpDistanceSourcePlanet = "";
            JumpDistanceLimit = null;
            Rocky = false;
            Gaseous = false;
            LowGravity = false;
            HighGravity = false;
            LowPressure = false;
            HighPressure = false;
            LowTemperature = false;
            HighTemperature = false;
            SearchFilter = "";

            // @TODO: Reset Show Settings?
        }

        public void SetFromUri(string uriQuery)
        {
            Reset();

            foreach (var majorToken in uriQuery.Split(new char[] { '&' }, StringSplitOptions.RemoveEmptyEntries))
            {
                var subTokens = majorToken.Split(new char[] { '=' }, StringSplitOptions.RemoveEmptyEntries);
                if (subTokens.Length == 2)
                {
                    var key = subTokens[0].ToUpper().TrimStart('?');
                    var value = subTokens[1].ToUpper();
                    switch (key)
                    {
                        case "RESOURCES":
                            SelectedMaterials = value.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries).Take(4).ToList();
                            break;
                        case "CONCTHRESH":
                            if (double.TryParse(value, out var OutConcentrationThreshold))
                            {
                                ConcentrationThreshold = OutConcentrationThreshold;
                            }
                            break;
                        case "PRODEFF":
                            if (double.TryParse(value, out var OutProductionEfficiency))
                            {
                                ProductionEfficiency = OutProductionEfficiency;
                            }
                            break;
                        case "FERTILE":
                            Fertile = (value == "TRUE");
                            break;
                        case "LM":
                            LocalMarket = (value == "TRUE");
                            break;
                        case "COGC":
                            COGC = (value == "TRUE");
                            break;
                        case "WAR":
                            Warehouse = (value == "TRUE");
                            break;
                        case "ADM":
                            AdminCenter = (value == "TRUE");
                            break;
                        case "SHY":
                            Shipyard = (value == "TRUE");
                            break;
                        case "ROCKY":
                            Rocky = (value == "TRUE");
                            break;
                        case "GAS":
                            Gaseous = (value == "TRUE");
                            break;
                        case "LOWGRAV":
                            LowGravity = (value == "TRUE");
                            break;
                        case "HIGHGRAV":
                            HighGravity = (value == "TRUE");
                            break;
                        case "LOWPRES":
                            LowPressure = (value == "TRUE");
                            break;
                        case "HIGHPRES":
                            HighPressure = (value == "TRUE");
                            break;
                        case "LOWTEMP":
                            LowTemperature = (value == "TRUE");
                            break;
                        case "HIGHTEMP":
                            HighTemperature = (value == "TRUE");
                            break;
                        case "SEARCH":
                            SearchFilter = HttpUtility.UrlDecode(value);
                            break;
                        case "JUMPSOURCE":
                            JumpDistanceSourcePlanet = HttpUtility.UrlDecode(value);
                            break;
                        case "JUMPLIMIT":
                            if (int.TryParse(value, out int OutJumpDistanceLimit))
                            {
                                JumpDistanceLimit = OutJumpDistanceLimit;
                            }
                            break;
                    }
                }
            }
        }

        public string GetUri()
        {
            StringBuilder sb = new StringBuilder();
            if (SelectedMaterials.Count > 0)
            {
                sb.Append("Resources=");
                sb.Append(String.Join(';', SelectedMaterials));
                sb.Append("&");
            }
            if (ConcentrationThreshold != 0.0)
            {
                sb.Append($"ConcThresh={ConcentrationThreshold:N2}&");
            }
            if (ProductionEfficiency != 100.0)
            {
                sb.Append($"ProdEff={ProductionEfficiency:N2}&");
            }
            if (Fertile)
            {
                sb.Append("Fertile=true&");
            }
            if (LocalMarket)
            {
                sb.Append("LM=true&");
            }
            if (COGC)
            {
                sb.Append("COGC=true&");
            }
            if (Warehouse)
            {
                sb.Append("WAR=true&");
            }
            if (AdminCenter)
            {
                sb.Append("ADM=true&");
            }
            if (Shipyard)
            {
                sb.Append("SHY=true&");
            }
            if (Rocky)
            {
                sb.Append("Rocky=true&");
            }
            if (Gaseous)
            {
                sb.Append("Gas=true&");
            }
            if (LowGravity)
            {
                sb.Append("LowGrav=true&");
            }
            if (HighGravity)
            {
                sb.Append("HighGrav=true&");
            }
            if (LowPressure)
            {
                sb.Append("LowPres=true&");
            }
            if (HighPressure)
            {
                sb.Append("HighPres=true&");
            }
            if (LowTemperature)
            {
                sb.Append("LowTemp=true&");
            }
            if (HighTemperature)
            {
                sb.Append("HighTemp=true&");
            }
            if (SearchFilter.Length > 0)
            {
                sb.Append($"Search={HttpUtility.UrlEncode(SearchFilter)}&");
            }
            if (JumpDistanceSourcePlanet.Length > 0)
            {
                sb.Append($"JumpSource={HttpUtility.UrlEncode(JumpDistanceSourcePlanet)}&");
            }
            if (JumpDistanceLimit != null && JumpDistanceLimit >= 0)
            {
                sb.Append($"JumpLimit={JumpDistanceLimit}&");
            }

            return sb.ToString().TrimEnd('&');
        }
    }
}
