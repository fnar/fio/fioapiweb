﻿using System.ComponentModel;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace FIOAPIWeb.PageHelpers.PlanetSearch
{
    public class PlanetSearchModelResource
    {
        public bool IsValid { get; set; } = false;

        public string Material { get; set; } = null!;

        [DisplayName("Type")]
        public string ResourceType { get; set; } = null!;

        public double Concentration { get; set; }

        [DisplayName("Daily Extraction")]
        public double DailyExtraction { get; set; }

        // F(Rcon, Rtyp) = (Rcon * 100) * (Rtyp == Gas ? 0.6 : 0.7) * 300 / (Rtyp == Min ? 25 : Rtyp == Gas ? 15 : 10)
        [DisplayName("300A")]
        public double ThreeHundredArea
        {
            get
            {
                const double ConcentrationModifier = 100.0; // Increase to percentage value
                const double TotalArea = 300.0;          // Up to 300 area
                double ExtractionModifier = 0.0;
                double Area = 0.0;
                switch (ResourceType)
                {
                    case "Gaseous":
                        ExtractionModifier = 0.6;
                        Area = 15.0;
                        break;
                    case "Mineral":
                        ExtractionModifier = 0.7;
                        Area = 25.0;
                        break;
                    case "Liquid":
                        ExtractionModifier = 0.7;
                        Area = 10.0;
                        break;
                }

                return (Concentration * ConcentrationModifier * ExtractionModifier * TotalArea) / Area;
            }
        }
    }

    public class PlanetSearchModel : ICloneable
    {
        public string NaturalId { get; set; } = null!;

        public string Name { get; set; } = null!;

        [DisplayName("Planet")]
        public string DisplayName { get; set; } = null!;

        public bool IsFertile { get; set; } = false;

        public double Fertility { get; set; } = -1.0;

        [JsonIgnore]
        public string FertilityDisplay
        {
            get
            {
                if (IsFertile)
                {
                    var FertilityPercentage = Fertility * 100.0;
                    var Prefix = Fertility >= 0.0 ? "+" : "-";
                    return $"{Prefix}{FertilityPercentage:N2}%";
                }

                return "";
            }
        }

        [DisplayName("Type")]
        public string SurfaceType { get; set; } = null!;

        public string Gravity { get; set; } = null!;
        public string Pressure { get; set; } = null!;
        public string Temperature { get; set; } = null!;

        public string FactionCode { get; set; } = null!;

        [DisplayName("LM")]
        public bool HasLocalMarket { get; set; }

        public bool HasChamberOfCommerce { get; set; }

        [DisplayName("WAR")]
        public bool HasWarehouse { get; set; }

        [DisplayName("ADM")]
        public bool HasAdministrationCenter { get; set; }

        [DisplayName("SHY")]
        public bool HasShipyard { get; set; }

        [DisplayName("Materials")]
        public string MaterialSummary { get; set; } = null!;

        public List<PlanetSearchModelResource> Resources { get; set; } = new();

        [DisplayName("COGC")]
        public string ActiveCOGCDisplay { get; set; } = null!;

        public string? ActiveCOGC { get; set; }

        public string CurrencyCode { get; set; } = null!;

        public bool HasJobData { get; set; }

        public int? TotalPioneers { get; set; }
        public int? OpenPioneers { get; set; }

        public int? TotalSettlers { get; set; }
        public int? OpenSettlers { get; set; }

        public int? TotalTechnicians { get; set; }
        public int? OpenTechnicians { get; set; }

        public int? TotalEngineers { get; set; }
        public int? OpenEngineers { get; set; }

        public int? TotalScientists { get; set; }
        public int? OpenScientists { get; set; }

        public int? TotalPlots { get; set; }

        public List<int> DistanceResults { get; set; } = new List<int>();

        public static async Task<List<PlanetSearchModel>> GetAllPlanets(List<Payloads.Material> AllPlanetMaterials, List<string> ExchangeLocationNames)
        {
            var psp = new Payloads.PlanetSearch
            {
                IncludeRocky = true,
                IncludeGaseous = true,
                IncludeLowGravity = true,
                IncludeHighGravity = true,
                IncludeLowPressure = true,
                IncludeHighPressure = true,
                IncludeLowTemperature = true,
                IncludeHighTemperature = true,
                DistanceChecks = ExchangeLocationNames,
            };

            return await Search(AllPlanetMaterials, psp);
        }

        public static async Task<List<PlanetSearchModel>> Search(List<Payloads.Material> AllJsonPlanetMaterials, Payloads.PlanetSearch psp)
        {
            var PlanetSearchModels = new List<PlanetSearchModel>();

            var planetSearchRequest = new Web.Request(HttpMethod.Post, "/planet/search", Payload: JsonSerializer.Serialize(psp));
            var searchedPlanetData = await planetSearchRequest.GetResponseAsync<List<Payloads.PlanetSearchResult>>();
            if (searchedPlanetData != null)
            {
                foreach (var planetData in searchedPlanetData)
                {
                    PlanetSearchModel model = new PlanetSearchModel();

                    model.NaturalId = planetData.NaturalId;
                    model.Name = planetData.Name!;
                    model.DisplayName = (model.Name != model.NaturalId) ? $"{model.Name} ({model.NaturalId})" : model.NaturalId;

                    model.IsFertile = planetData.Fertility > -1.0;
                    if (model.IsFertile)
                    {
                        model.Fertility = (float)(planetData.Fertility! / 3.3);
                    }

                    model.SurfaceType = planetData.Surface ? "Rocky" : "Gaseous";
                    model.Gravity = GetDescriptorStr(planetData.Gravity, 0.25, 2.5);
                    model.Pressure = GetDescriptorStr(planetData.Pressure, 0.25, 2.0);
                    model.Temperature = GetDescriptorStr(planetData.Temperature, -25.0, 75.0);
                    model.HasLocalMarket = planetData.HasLocalMarket;
                    model.HasChamberOfCommerce = planetData.HasChamberOfCommerce;
                    model.HasWarehouse = planetData.HasWarehouse;
                    model.HasAdministrationCenter = planetData.HasAdministrationCenter;
                    model.HasShipyard = planetData.HasShipyard;

                    model.MaterialSummary = "";
                    foreach (var resource in planetData.Resources)
                    {
                        PlanetSearchModelResource pr = new();

                        var material = AllJsonPlanetMaterials.FirstOrDefault(r => r.MaterialId == resource.MaterialId);
                        if (material != null)
                        {    
                            pr.IsValid = true;
                            pr.Material = material.Ticker;
                            pr.ResourceType = resource.Type.Substring(0, 1) + resource.Type.Substring(1).ToLower(); // GASEOUS -> Gaseous
                            pr.Concentration = (float)resource.Concentration;
                            pr.DailyExtraction = (resource.Type == "GASEOUS") ? resource.Concentration * 60.0 : resource.Concentration * 70.0;
                            model.Resources.Add(pr);

                            model.MaterialSummary += $"{pr.Material} ({pr.Concentration:P2}), ";
                        }
                    }

                    if (model.MaterialSummary.Length > 2)
                    {
                        model.MaterialSummary = model.MaterialSummary.Substring(0, model.MaterialSummary.Length - 2); // Trim off ", " from the end
                    }

                    model.FactionCode = (planetData.CountryCode != null) ? planetData.CountryCode : "";
                    model.CurrencyCode = (planetData.CurrencyCode != null) ? planetData.CurrencyCode : "";

                    model.HasJobData = planetData.HasJobData;

                    model.TotalPioneers = planetData.TotalPioneers;
                    model.OpenPioneers = planetData.OpenPioneers;
                    model.TotalSettlers = planetData.TotalSettlers;
                    model.OpenSettlers = planetData.OpenSettlers;
                    model.TotalTechnicians = planetData.TotalTechnicians;
                    model.OpenTechnicians = planetData.OpenTechnicians;
                    model.TotalEngineers = planetData.TotalEngineers;
                    model.OpenEngineers = planetData.OpenEngineers;
                    model.TotalScientists = planetData.TotalScientists;
                    model.OpenScientists = planetData.OpenScientists;
                    model.TotalPlots = planetData.Plots;
                    model.DistanceResults = planetData.DistanceResults;

                    DateTime now = DateTime.UtcNow;
                    foreach (var cogcProgram in planetData.COGCPrograms)
                    {
                        if (now >= cogcProgram.StartTime && now <= cogcProgram.EndTime)
                        {
                            if (planetData.CurrentCOGCProgramEndTime < now)
                            {
                                model.ActiveCOGC = "Inactive";
                            }
                            else
                            {
                                model.ActiveCOGC = planetData.CurrentCOGCProgram != null ? planetData.CurrentCOGCProgram : "Inactive";
                            }
                            break;
                        }
                    }

                    if (!model.HasChamberOfCommerce)
                    {
                        model.ActiveCOGC = "Not Present";
                    }
                    else if (model.HasChamberOfCommerce && model.ActiveCOGC == null)
                    {
                        model.ActiveCOGC = "No Data";
                    }

                    model.ActiveCOGCDisplay = GetFriendlyCOGCDisplay(model.ActiveCOGC!);

                    PlanetSearchModels.Add(model);
                }
            }

            return PlanetSearchModels;
        }

        private static string GetDescriptorStr(double value, double lower, double upper)
        {
            if (value < lower)
            {
                return "Low";
            }
            else if (value > upper)
            {
                return "High";
            }

            return "Normal";
        }

        private static string GetFriendlyCOGCDisplay(string cogc)
        {
            if (!String.IsNullOrWhiteSpace(cogc))
            {
                switch (cogc)
                {
                    case "WORKFORCE_PIONEERS":
                        return "Pioneers";
                    case "WORKFORCE_SETTLERS":
                        return "Settlers";
                    case "WORKFORCE_TECHNICIANS":
                        return "Technicians";
                    case "WORKFORCE_ENGINEERS":
                        return "Engineers";
                    case "WORKFORCE_SCIENTISTS":
                        return "Scientists";
                    case "ADVERTISING_AGRICULTURE":
                        return "Agriculture";
                    case "ADVERTISING_CHEMISTRY":
                        return "Chemistry";
                    case "ADVERTISING_CONSTRUCTION":
                        return "Construction";
                    case "ADVERTISING_ELECTRONICS":
                        return "Electronics";
                    case "ADVERTISING_FOOD_INDUSTRIES":
                        return "Food Industries";
                    case "ADVERTISING_FUEL_REFINING":
                        return "Fuel Refining";
                    case "ADVERTISING_MANUFACTURING":
                        return "Manufacturing";
                    case "ADVERTISING_METALLURGY":
                        return "Metallurgy";
                    case "ADVERTISING_RESOURCE_EXTRACTION":
                        return "Resource Extraction";
                    default:
                        return cogc;
                }
            }

            return "None";
        }

		public object Clone()
		{
            return new PlanetSearchModel()
            {
				NaturalId = this.NaturalId,
				Name = this.Name,
				DisplayName = this.DisplayName,
				IsFertile = this.IsFertile,
				Fertility = this.Fertility,
				SurfaceType = this.SurfaceType,
				Gravity = this.Gravity,
				Pressure = this.Pressure,
				Temperature = this.Temperature,
				FactionCode = this.FactionCode,
				HasLocalMarket = this.HasLocalMarket,
				HasChamberOfCommerce = this.HasChamberOfCommerce,
				HasWarehouse = this.HasWarehouse,
				HasAdministrationCenter = this.HasAdministrationCenter,
				HasShipyard = this.HasShipyard,
				MaterialSummary = this.MaterialSummary,
				Resources = this.Resources,
				ActiveCOGCDisplay = this.ActiveCOGCDisplay,
				ActiveCOGC = this.ActiveCOGC,
				CurrencyCode = this.CurrencyCode,
				HasJobData = this.HasJobData,
				TotalPioneers = this.TotalPioneers,
				OpenPioneers = this.OpenPioneers,
				TotalSettlers = this.TotalSettlers,
				OpenSettlers = this.OpenSettlers,
				TotalTechnicians = this.TotalTechnicians,
				OpenTechnicians = this.OpenTechnicians,
				TotalEngineers = this.TotalEngineers,
				OpenEngineers = this.OpenEngineers,
				TotalScientists = this.TotalScientists,
				OpenScientists = this.OpenScientists,
				TotalPlots = this.TotalPlots,
				DistanceResults = this.DistanceResults,
			};
		}
	}
}
