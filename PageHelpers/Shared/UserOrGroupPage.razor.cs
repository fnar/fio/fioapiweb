﻿using FIOAPIWeb.Extensions;
using FIOAPIWeb.Services;
using Microsoft.AspNetCore.Components;

namespace FIOAPIWeb.PageHelpers.Shared
{
    public enum PullType
    {
        Data = 0,
        Burn = 1
    }

    [Flags]
    public enum PullParameters
    {
        None = 0,
        Company = 1 << 0,
        Contract = 1 << 1,
        CXOS = 1 << 2,
        ProductionLine = 1 << 3,
        Ship = 1 << 4,
        Site = 1 << 5,
        Storage = 1 << 6,
        Workforce = 1 << 7,
    }

    public partial class UserOrGroupPage
    {
        [Parameter]
        public string Title { get; set; } = "";

        [Parameter]
        public RenderFragment ChildContent { get; set; } = null!;

        [Parameter]
        public PullType PullType { get; set; } = PullType.Data;

        [Parameter]
        public PullParameters PullParameters { get; set; } = PullParameters.None;

        private List<string>? AllUsers = new();

        public int CurrentProgress { get; set; } = 0;

        protected override void OnInitialized()
        {
            base.OnInitialized();
            UserAppState.OnChange += StateHasChanged;
        }

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            await base.OnAfterRenderAsync(firstRender);

            if (UserAppState.AuthState == AuthState.Authenticating && NeedsRefresh)
            {
                await Refresh();
            }
        }


        public void Dispose()
        {
            UserAppState.OnChange -= StateHasChanged;
        }
        private bool NeedsRefresh = true;
        private async Task Refresh()
        {
            if (NeedsRefresh)
            {
                NeedsRefresh = false;

                // @TODO: Singleton Cache?
                var allUsersRequest = new Web.Request(HttpMethod.Get, "/auth/users", UserAppState.BearerToken);
                AllUsers = await allUsersRequest.GetResponseAsync<List<string>>();
                CurrentProgress = 1;

                var GroupOrUsernames = NavManager.GetGroupOrUsernames(UserAppState.UserName);
                bool HasUsernamesSpecified = GroupOrUsernames.Usernames.Count > 0;
                bool HasGroupSpecified = GroupOrUsernames.Group != null;

                if (HasGroupSpecified)
                {

                }
                else
                {
                    System.Diagnostics.Debug.Assert(HasUsernamesSpecified);
                }
            }
        }

        private bool TryGetQueryUrl(List<string> usernames, string group, out string QueryUrl)
        {
            // @TODO
            QueryUrl = "";
            return false;
            /*
            int GroupId = 0;
            if (!string.IsNullOrWhiteSpace(group))
            {
                System.Diagnostics.Debug.Assert(usernames.Count == 0);
                var groupParts = group.Split('-', StringSplitOptions.RemoveEmptyEntries);
                foreach (var groupPart in groupParts)
                {
                    if (int.TryParse(groupPart, out GroupId))
                    {
                        break;
                    }
                }

                if (GroupId == 0)
                {
                    return false;
                }
            }
            else
            {
                System.Diagnostics.Debug.Assert(string.IsNullOrWhiteSpace(group));
                
            }

            if (PullType == PullType.Data)
            {

            }
            else
            {
                System.Diagnostics.Debug.Assert(PullType == PullType.Burn);
            }
            */
        }
    }
}
