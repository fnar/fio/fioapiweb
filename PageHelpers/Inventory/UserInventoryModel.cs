﻿using FIOAPIWeb.Payloads;
using FIOAPIWeb.Web;

namespace FIOAPIWeb.PageHelpers.Inventory
{
	public class UserInventoryModel
	{
		public string Username { get; set; } = null!;
		public List<LocationModel> Locations { get; set; } = new();

		public static async Task<List<UserInventoryModel>> RetrieveUsernameModels(string AuthToken, int GroupId = -1, List<string>? Usernames = null)
		{
			if (GroupId == -1 && Usernames == null)
			{
				throw new ArgumentException($"Both {nameof(GroupId)} and {nameof(Usernames)} are invalid.");
			}
			else if (GroupId != -1 && Usernames != null && Usernames.Count > 0)
			{
				throw new ArgumentException($"Both {nameof(GroupId)} and {nameof(Usernames)} are specified but only one can be valid at a time.");
			}

			RetrievalResponse<AllData>? retrievalResponse = null;
			var requestPerms = new DataRequestPerms
			{
				SiteData = true,
				StorageData = true
			};

			bool IsGroup = GroupId != -1;
			if (IsGroup)
			{
				retrievalResponse = await DataRequest.Get(GroupId, requestPerms, AuthToken);
			}
			else
			{
				retrievalResponse = await DataRequest.Get(Usernames!, requestPerms, AuthToken);
			}



			return null!;
		}
	}

	public class LocationModel
	{
		public string Location { get; set; } = null!;
		public List<ItemModel> Items { get; set; } = new();
	}

	public class ItemModel
	{
		public string Ticker { get; set; } = null!;
		public int Amount { get; set; }
	}
}
