﻿namespace FIOAPIWeb.Pages
{
	public partial class Setup
	{
		public MudBlazor.Color LoginStatusColor
		{
			get
			{
				return GetColor(0);
			}
			set
			{

			}
		}

		public MudBlazor.Size LoginStatusSize
		{
			get
			{
				return GetSize(0);
			}
			set
			{

			}
		}

		public MudBlazor.Color RefreshApexColor
		{
			get
			{
				return GetColor(1);
			}
			set
			{

			}
		}

		public MudBlazor.Size RefreshApexSize
		{
			get
			{
				return GetSize(1);
			}
			set
			{

			}
		}

		public MudBlazor.Color OpenAllBasesColor
		{
			get
			{
				return GetColor(2);
			}
			set
			{

			}
		}

		public MudBlazor.Size OpenAllBasesSize
		{
			get
			{
				return GetSize(2);
			}
			set
			{

			}
		}

		public MudBlazor.Color DoneColor
		{
			get
			{
				return GetColor(3);
			}
			set
			{

			}
		}

		public MudBlazor.Size DoneSize
		{
			get
			{
				return GetSize(3);
			}
			set
			{

			}
		}

		public MudBlazor.Color GetColor(int StepIdx)
		{
			if (StepIdx < ActiveStepIndex)
			{
				return MudBlazor.Color.Dark;
			}
			else if (StepIdx == ActiveStepIndex)
			{
				return MudBlazor.Color.Primary;
			}
			else
			{
				return MudBlazor.Color.Success;
			}
		}

		public MudBlazor.Size GetSize(int StepIdx)
		{
			if (StepIdx == ActiveStepIndex)
			{
				return MudBlazor.Size.Large;
			}

			return MudBlazor.Size.Small;
		}

		public int ActiveStepIndex { get; set; } = 0;

		private System.Timers.Timer? timer = null;

		protected override void OnInitialized()
		{
			base.OnInitialized();

			timer = new System.Timers.Timer(2000.0);
			timer.Elapsed += async delegate { await NotifyTimerElapsed(); };
			timer.AutoReset = false;
			timer.Enabled = true;
		}

		private async Task NotifyTimerElapsed()
		{
			await InvokeAsync(() => {
				ActiveStepIndex++;
				StateHasChanged();
			});
			timer!.Start();
		}
	}
}
