﻿using FIOAPIWeb.Components.Account;
using FIOAPIWeb.Components.Shared;
using FIOAPIWeb.Payloads;
using FIOAPIWeb.Services;
using MudBlazor;
using System.Net;
using System.Text.Json;

namespace FIOAPIWeb.Pages
{
    public partial class Account
	{
		int CurrentProgress
		{
			get => _CurrentProgress;
			set
			{
				if (_CurrentProgress != value)
				{
					_CurrentProgress = value;
					StateHasChanged();
				}
			}
		}
		private int _CurrentProgress = 0;

		private const int TotalProgress = 2;

        private bool NeedsRefresh = true;
        private List<string>? AllUsers = new();
		private List<Payloads.APIKey>? APIKeys = new();
		private List<Payloads.PermissionResponse>? PermissionsGranted = new();

		protected override void OnInitialized()
		{
			base.OnInitialized();
			UserAppState.OnChange += StateHasChanged;
		}

		public void Dispose()
		{
			UserAppState.OnChange -= StateHasChanged;
		}
		
		private async Task Refresh()
		{
            NeedsRefresh = false;

			AllUsers = GlobalCaches.AllUsernames.Items.ToList();

            var allAPIKeysRequest = new Web.Request(HttpMethod.Get, "/auth/listapikeys", UserAppState.BearerToken);
            APIKeys = await allAPIKeysRequest.GetResponseAsync<List<Payloads.APIKey>>();
            CurrentProgress = 1;

            var grantedPermissionsRequest = new Web.Request(HttpMethod.Get, "/permission", UserAppState.BearerToken);
            PermissionsGranted = await grantedPermissionsRequest.GetResponseAsync<List<Payloads.PermissionResponse>>();
            
			if (allAPIKeysRequest == null || grantedPermissionsRequest == null) 
			{
				Snackbar.Add("Failed to retrieve data from server. Refresh.", Severity.Error);
				throw new InvalidOperationException();
			}

			CurrentProgress = 2;
		}

		protected override async Task OnAfterRenderAsync(bool firstRender)
		{
			await base.OnAfterRenderAsync(firstRender);

            if (UserAppState.AuthState == AuthState.Authenticated && NeedsRefresh)
			{
				await Refresh();
			}
		}

		async Task ResetDataAsync()
		{
			Func<Task> AcceptFunc = async () =>
			{
				var resetDataRequest = new Web.Request(HttpMethod.Delete, "/auth/resetgamedata", UserAppState.BearerToken);
				await resetDataRequest.GetResultNoResponse();
				if (resetDataRequest.StatusCode == HttpStatusCode.OK)
				{
					Snackbar.Add("Reset all game data", Severity.Success);
				}
				else
				{
					Snackbar.Add("Unknown Error: Unable to reset game data", Severity.Error);
				}
			};

			var dlgParameters = new DialogParameters<SimpleConfirmDialog>
			{
				{ d => d.ContentText, "Are you sure you want to reset all game data?" },
				{ d => d.AcceptFunc, AcceptFunc }
			};

			await DialogService.Show<SimpleConfirmDialog>("Reset All Game Data?", dlgParameters, Common.DefaultOptions).Result;
		}

		async Task ChangePasswordAsync()
		{
			await DialogService.Show<ChangePasswordDialog>("Change Password", Common.DefaultOptions).Result;
        }

		async Task SetDiscordNameAsync()
		{
			await DialogService.Show<SetDiscordNameDialog>("Set Discord Name", Common.DefaultOptions).Result;
        }

		async Task CreateAPIKey()
		{
			var dlgResult = await DialogService.Show<CreateAPIKeyDialog>("Create APIKey", Common.DefaultOptions).Result;
			if (!dlgResult.Canceled)
			{
				Payloads.APIKey apiKey = (Payloads.APIKey)dlgResult.Data;
				APIKeys!.Add(apiKey);
				StateHasChanged();
            }
        }

		async Task DeleteAPIKey(Guid key)
		{
			Func<string, Task<bool>> AcceptPasswordFunc = async (string password) =>
			{
				var payload = new Payloads.RevokeAPIKey
				{
					UserName = UserAppState.UserName!,
					Password = password,
					APIKeyToRevoke = key
				};

                var revokeKeyRequest = new Web.Request(HttpMethod.Post, "/auth/revokeapikey", UserAppState.BearerToken, JsonSerializer.Serialize(payload));
                await revokeKeyRequest.GetResultNoResponse();
				switch(revokeKeyRequest.StatusCode)
				{
					case HttpStatusCode.OK:
                        Snackbar.Add("Deleted APIKey", Severity.Success);
						return true;
					case HttpStatusCode.Unauthorized:
						Snackbar.Add("Password incorrect", Severity.Error);
						return false;
					default:
                        Snackbar.Add("Unknown error occurred", Severity.Error);
						return false;
				}
            };

            var dlgParameters = new DialogParameters<PasswordDialog>
            {
                { d => d.ContentText, "Delete API Key?" },
				{ d => d.AcceptButtonText, "Delete" },
                { d => d.AcceptPasswordFunc, AcceptPasswordFunc }
            };

            var dlgResult = await DialogService.Show<PasswordDialog>("Delete API Key", dlgParameters, Common.DefaultOptions).Result;
			if (!dlgResult.Canceled)
			{
				APIKeys!.RemoveAll(a => a.Key == key);
				StateHasChanged();
			}
		}

		async Task CopyAPIKeyToClipboard(Payloads.APIKey apiKey)
		{
			await Clipboard.WriteTextAsync(apiKey.Key.ToString());
			Snackbar.Add("Copied API Key to clipboard", Severity.Success);
        }

		bool AmIAdmin(Payloads.Group group)
		{
			return group.GroupOwner == UserAppState.UserName!.ToLower() || group.Admins.Any(ga => ga.UserName == UserAppState.UserName.ToLower());
		}

		bool AmIOwner(Payloads.Group group)
		{
			return group.GroupOwner == UserAppState.UserName!.ToLower();
		}

        async Task CreateGroup()
		{
			var result = await DialogService.Show<CreateGroupDialog>("Create Group", Common.DefaultOptions).Result;
			if (!result.Canceled)
			{
				await UserAppState.RefreshGroupMemberships();
			}
        }

		async Task InviteAdmins(int GroupId)
		{
			var DlgOptions = new DialogOptions
			{
				CloseOnEscapeKey = true,
				CloseButton = true,
				DisableBackdropClick = true
			};

            var dlgParameters = new DialogParameters<InviteToGroupDialog>
            {
                { d => d.Group, UserAppState.Groups.First(g => g.GroupId == GroupId) },
                { d => d.AllFIOUsers, AllUsers! },
				{ d => d.IsAdminDialog, true }
            };

            await DialogService.Show<InviteToGroupDialog>("Invite Admins", dlgParameters, DlgOptions).Result;
		}

		async Task InviteMembers(int GroupId)
		{
            var DlgOptions = new DialogOptions
            {
                CloseOnEscapeKey = true,
                CloseButton = true,
                DisableBackdropClick = true
            };

            var dlgParameters = new DialogParameters<InviteToGroupDialog>
            {
                { d => d.Group, UserAppState.Groups.First(g => g.GroupId == GroupId) },
                { d => d.AllFIOUsers, AllUsers! },
                { d => d.IsAdminDialog, false }
            };

            await DialogService.Show<InviteToGroupDialog>("Invite Users", dlgParameters, Common.DefaultOptions).Result;
        }

		async Task DeleteGroup(int GroupId)
		{
			Func<Task> AcceptTask = async () =>
			{
                var deleteGroupRequest = new Web.Request(HttpMethod.Delete, $"/group/delete/{GroupId}", UserAppState.BearerToken);
                await deleteGroupRequest.GetResultNoResponse();
                if (deleteGroupRequest.StatusCode == HttpStatusCode.OK)
                {
                    Snackbar.Add("Deleted group", Severity.Success);
                    await UserAppState.RefreshGroupMemberships();
                }
                else
                {
                    Snackbar.Add("Unknown error occurred", Severity.Error);
                }
            };

            var dlgParameters = new DialogParameters<SimpleConfirmDialog>
            {
                { d => d.ContentText, $"Are you sure you want to delete Group {GroupId}? It can't be recovered." },
                { d => d.AcceptButtonText, "Delete" },
                { d => d.AcceptFunc, AcceptTask }
            };

			await DialogService.Show<SimpleConfirmDialog>("Delete Group?", dlgParameters, Common.DefaultOptions).Result;
        }

		async Task CreatePermission()
		{
			var dlgParameters = new DialogParameters<CreatePermissionDialog>
			{
				{ d => d.AllFIOUsers, AllUsers! },
				{ d => d.CurrentPermissionUsers, PermissionsGranted!.Select(p => p.GranteeUserName).ToList() }
			};

			var result = await DialogService.Show<CreatePermissionDialog>("Created Permission", dlgParameters, Common.DefaultOptions).Result;
			if (!result.Canceled)
			{
				PermissionsGranted!.Add((PermissionResponse)result.Data);
			}
		}

		async Task DeletePermission(string UserName)
		{
            Func<Task> AcceptFunc = async () =>
            {
                var resetDataRequest = new Web.Request(HttpMethod.Delete, $"/permission/revoke/{UserName}", UserAppState.BearerToken);
                await resetDataRequest.GetResultNoResponse();
                if (resetDataRequest.StatusCode == HttpStatusCode.OK)
                {
                    Snackbar.Add("Deleted Permission", Severity.Success);
                }
                else
                {
                    Snackbar.Add("Unknown error encountered", Severity.Error);
                }
            };

            var dlgParameters = new DialogParameters<SimpleConfirmDialog>
            {
                { d => d.ContentText, "Are you sure you want to delete this permission?" },
                { d => d.AcceptFunc, AcceptFunc }
            };

            var res = await DialogService.Show<SimpleConfirmDialog>("Delete Permission?", dlgParameters, Common.DefaultOptions).Result;
			if (!res.Canceled)
			{
				PermissionsGranted!.RemoveAll(p => p.GranteeUserName == UserName);
			}
        }
    }
}
