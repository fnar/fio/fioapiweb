using FIOAPIWeb.Extensions;
using FIOAPIWeb.Payloads;
using FIOAPIWeb.Web;
using MudBlazor;

namespace FIOAPIWeb.Pages
{
    public class InventoryItem
    {
        public string Username { get; set; } = "";
        public string Location { get; set; } = "";
        public string Ticker { get; set; } = "";
        public int Amount { get; set; } = 0;
    }

    public partial class Inventory
    {
        private int CurrentProgress
        {
            get => _CurrentProgress;
            set
            {
                if (_CurrentProgress != value) 
                {
                    _CurrentProgress = value;
                    StateHasChanged();
                }
            }
        }
        private int _CurrentProgress = 0;

        private const int TotalProgress = 1;

        private TableGroupDefinition<InventoryItem> groupDef = new()
        {
            GroupName = "Location",
            Indentation = true,
            Expandable = true,
            Selector = (e) => e.Location
        };

        private List<InventoryItem> InventoryItems { get; set; } = new();

        protected override void OnInitialized()
        {
            base.OnInitialized();
            UserAppState.OnChange += StateHasChanged;
        }

        public void Dispose()
        {
            UserAppState.OnChange -= StateHasChanged;
        }

        private bool ShouldShowUsernames { get; set; } = false;

        private bool NeedsRefresh = true;
        private async Task Refresh()
        {
            if (NeedsRefresh)
            {
                NeedsRefresh = false;
                if (UserAppState.UserName != null && UserAppState.BearerToken != null)
                {
                    var GroupOrUsernames = NavManager.GetGroupOrUsernames(UserAppState.UserName);
                    NavManager.SetGroupOrUsernames(GroupOrUsernames);

                    ShouldShowUsernames = GroupOrUsernames.Usernames.Count > 1 || GroupOrUsernames.GroupId != null;

					var dataRequestResult = await DataRequest.Get(GroupOrUsernames, new DataRequestPerms { SiteData = true, StorageData = true }, UserAppState.BearerToken);
                    if (dataRequestResult != null)
                    {
                        foreach(var kvp in dataRequestResult.UserNameToData)
                        {
                            var username = kvp.Key;
                            var data = kvp.Value.Data;

                            if (data.Storages != null)
                            {
                                foreach (var storage in data.Storages)
                                {
                                    string location = storage.LocationId != null ? storage.LocationId : "";
                                    if (storage.LocationName != null)
                                    {
                                        location = storage.LocationName;
                                    }
                                    if (storage.LocationNaturalId != null)
                                    {
                                        location = storage.LocationNaturalId;
                                    }
                                    
                                    foreach (var item in storage.StorageItems)
                                    {
                                        if (item.MaterialTicker != null && item.Amount != null)
                                        {
                                            InventoryItems.Add(new InventoryItem
                                            {
                                                Username = username,
                                                Location = location,
                                                Ticker = item.MaterialTicker,
                                                Amount = (int)item.Amount
                                            });
                                        }                                        
                                    }
                                }
                            }                            
                        }
                    }
                }
                CurrentProgress = 1;
            }
        }

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            await base.OnAfterRenderAsync(firstRender);

            if (UserAppState.AuthState == Services.AuthState.Authenticated && NeedsRefresh)
            {
                await Refresh();
            }
        }
    }
}
