﻿namespace FIOAPIWeb.Pages
{
    public partial class Notifications
    {
        protected override void OnInitialized()
        {
            base.OnInitialized();
            UserAppState.OnChange += StateHasChanged;
        }

        public void Dispose()
        {
            UserAppState.OnChange -= StateHasChanged;
        }

        async Task AcceptInviteAsync()
        {
            await Task.CompletedTask;
        }

        async Task RejectInviteAsync()
        {
            await Task.CompletedTask;
        }
    }
}
