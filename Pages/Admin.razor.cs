using System.Net;
using System.Runtime.InteropServices;
using System.Text.Json;
using FIOAPIWeb.Components.Admin;
using FIOAPIWeb.Components.Shared;
using FIOAPIWeb.Payloads;
using FIOAPIWeb.Services;
using MudBlazor;

namespace FIOAPIWeb.Pages
{
    public partial class Admin
    {
        private string? DiscordClipboardContents = null;

        private static DialogOptions DlgOptions = new DialogOptions
        {
            CloseOnEscapeKey = true,
            CloseButton = true,
        };

        private List<string> AllUsernames = new();
        private List<ComexExchange> ComexExchanges = new();
        private List<Material> Materials = new();

		protected override void OnInitialized()
		{
			base.OnInitialized();
			UserAppState.OnChange += StateHasChanged;

            AllUsernames = GlobalCaches.AllUsernames.Items.ToList();
            ComexExchanges = GlobalCaches.ComexExchanges.Items.ToList();
            Materials = GlobalCaches.Materials.Items.ToList();
		}

		public void Dispose()
		{
			UserAppState.OnChange -= StateHasChanged;
		}

        async Task CreateAccountAsync()
        {
            var result = await DialogService.Show<CreateAccountDialog>("Create Account", DlgOptions).Result;
            DiscordClipboardContents = result.Data as string;
        }

        async Task CopyAccountInfoToClipboard()
        {
            await Clipboard.WriteTextAsync(DiscordClipboardContents);
            Snackbar.Add("Copied Account Information To Clipboard", Severity.Success);
        }

        async Task ResetData(string Endpoint, string DataName)
        {
            Func<Task> AcceptTask = async () =>
            {
                var clearDataRequest = new Web.Request(HttpMethod.Delete, Endpoint, UserAppState.BearerToken);
                await clearDataRequest.GetResultNoResponse();
                if (clearDataRequest.StatusCode == HttpStatusCode.OK)
                {
                    Snackbar.Add($"Cleared {DataName} Data", Severity.Success);
                }
                else
                {
                    Snackbar.Add("Operation Failed", Severity.Error);
                }
            };

            var dlgParameters = new DialogParameters<SimpleConfirmDialog>
            {
                { d => d.ContentText, $"Are you sure you want to delete all {DataName}?" },
                { d => d.AcceptButtonText, "Delete" },
                { d => d.AcceptFunc, AcceptTask }
            };

            await DialogService.Show<SimpleConfirmDialog>("Delete CX Data?", dlgParameters, Common.DefaultOptions).Result;
        }

        async Task CopyUsernamesToClipboard()
        {
            await Clipboard.WriteTextAsync(JsonSerializer.Serialize(AllUsernames));
            Snackbar.Add("Copied Usernames to clipboard", Severity.Success);
        }

        async Task CopyComexExchangesToClipboard()
        {
            await Clipboard.WriteTextAsync(JsonSerializer.Serialize(ComexExchanges));
            Snackbar.Add("Copied Comex Exchanges to clipboard", Severity.Success);
        }

        async Task CopyMaterialsToClipboard()
        {
            await Clipboard.WriteTextAsync(JsonSerializer.Serialize(Materials));
            Snackbar.Add("Copied Materials to clipboard", Severity.Success);
        }
    }
}
