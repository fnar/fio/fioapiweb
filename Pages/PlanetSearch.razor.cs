﻿using System.Net;
using FIOAPIWeb.PageHelpers.PlanetSearch;
using FIOAPIWeb.Services;

namespace FIOAPIWeb.Pages
{
    public partial class PlanetSearch
    {
		#region Non-Changing Fields
		public List<Payloads.Material> PlanetMaterials { get; set; } = new();
        public List<string> PlanetMaterialTickers { get; set; } = new();

		public List<Payloads.ComexExchange> ComexExchanges { get; set; } = new();

		public List<PlanetSearchModel> AllPlanets { get; set; } = new();
		#endregion

		public PlanetSearchState PlanetSearchState { get; set; } = null!;

		#region Loading Progress
		public int CurrentProgress
		{
			get => _CurrentProgress;
			set
			{
				if (_CurrentProgress != value)
				{
					_CurrentProgress = value;
					StateHasChanged();
				}
			}
		}
		private int _CurrentProgress = 0;
		#endregion

		#region IsSearching
		public bool IsSearching
		{
			get => _IsSearching;
			set
			{
				if (_IsSearching != value)
				{
					_IsSearching = value;
					StateHasChanged();
				}
			}
		}
		private bool _IsSearching = false;
        #endregion

		#region Selected Materials
		public IEnumerable<string> SelectedMaterialTickers
        {
            get => _SelectedMaterialTickers;
            set
            {
                if (_SelectedMaterialTickers != value)
                {
                    _SelectedMaterialTickers = value;

                    PlanetSearchState.SelectedMaterials = _SelectedMaterialTickers.ToList();
					HandleChange();
                }
            }
        }
        private IEnumerable<string> _SelectedMaterialTickers = new HashSet<string>();
        #endregion

        #region Concentration Threshold
        public double ConcentrationThreshold
        {
            get => _ConcentrationThreshold;
            set
            {
                if (_ConcentrationThreshold != value)
                {
                    _ConcentrationThreshold = value;

                    PlanetSearchState.ConcentrationThreshold = _ConcentrationThreshold;
                    HandleChange();
                }
            }
        }
        private double _ConcentrationThreshold = 0.0;
        #endregion

        #region Production Efficiency
        public double ProductionEfficiency
        {
            get => _ProductionEfficiency;
            set
            {
                if (_ProductionEfficiency != value) 
                { 
                    _ProductionEfficiency = value;

                    PlanetSearchState.ProductionEfficiency = _ProductionEfficiency;
                    HandleChange();
                }
            }
        }
        private double _ProductionEfficiency = 100.0;
		#endregion

		#region Fertile
		public bool Fertile
        {
            get => _Fertile;
            set
            {
                if (_Fertile != value)
                {
                    _Fertile = value;

                    PlanetSearchState.Fertile = _Fertile;
                    HandleChange();
                }
            }
        }
        private bool _Fertile = false;
		#endregion

		#region LocalMarket
		public bool LocalMarket
		{
			get => _LocalMarket;
			set
			{
				if (_LocalMarket != value)
				{
					_LocalMarket = value;

					PlanetSearchState.LocalMarket = _LocalMarket;
                    HandleChange();
                }
			}
		}
		private bool _LocalMarket = false;
		#endregion

		#region COGC
		public bool COGC
		{
			get => _COGC;
			set
			{
				if (_COGC != value)
				{
					_COGC = value;

					PlanetSearchState.COGC = _COGC;
					HandleChange();
				}
			}
		}
		private bool _COGC = false;
		#endregion

		#region Warehouse
		public bool Warehouse
		{
			get => _Warehouse;
			set
			{
				if (_Warehouse != value)
				{
					_Warehouse = value;

					PlanetSearchState.Warehouse = _Warehouse;
					HandleChange();
				}
			}
		}
		private bool _Warehouse = false;
		#endregion

		#region AdminCenter
		public bool AdminCenter
		{
			get => _AdminCenter;
			set
			{
				if (_AdminCenter != value)
				{
					_AdminCenter = value;

					PlanetSearchState.AdminCenter = _AdminCenter;
                    HandleChange();
                }
			}
		}
		private bool _AdminCenter = false;
		#endregion

		#region Shipyard
		public bool Shipyard
		{
			get => _Shipyard;
			set
			{
				if (_Shipyard != value)
				{
					_Shipyard = value;

					PlanetSearchState.Shipyard = _Shipyard;
                    HandleChange();
                }
			}
		}
		private bool _Shipyard = false;
		#endregion

		#region Rocky
		public bool Rocky
		{
			get => _Rocky;
			set
			{
				if (_Rocky != value)
				{
					_Rocky = value;

					PlanetSearchState.Rocky = _Rocky;
                    HandleChange();
                }
			}
		}
		private bool _Rocky = false;
		#endregion

		#region Gaseous
		public bool Gaseous
		{
			get => _Gaseous;
			set
			{
				if (_Gaseous != value)
				{
					_Gaseous = value;

					PlanetSearchState.Gaseous = _Gaseous;
                    HandleChange();
                }
			}
		}
		private bool _Gaseous = false;
		#endregion

		#region LowGravity
		public bool LowGravity
		{
			get => _LowGravity;
			set
			{
				if (_LowGravity != value)
				{
					_LowGravity = value;

					PlanetSearchState.LowGravity = _LowGravity;
                    HandleChange();
                }
			}
		}
		private bool _LowGravity = false;
		#endregion

		#region HighGravity
		public bool HighGravity
		{
			get => _HighGravity;
			set
			{
				if (_HighGravity != value)
				{
					_HighGravity = value;

					PlanetSearchState.HighGravity = _HighGravity;
                    HandleChange();
                }
			}
		}
		private bool _HighGravity = false;
		#endregion

		#region LowPressure
		public bool LowPressure
		{
			get => _LowPressure;
			set
			{
				if (_LowPressure != value)
				{
					_LowPressure = value;

					PlanetSearchState.LowPressure = _LowPressure;
                    HandleChange();
                }
			}
		}
		private bool _LowPressure = false;
		#endregion

		#region HighPressure
		public bool HighPressure
		{
			get => _HighPressure;
			set
			{
				if (_HighPressure != value)
				{
					_HighPressure = value;

					PlanetSearchState.HighPressure = _HighPressure;
                    HandleChange();
                }
			}
		}
		private bool _HighPressure = false;
		#endregion

		#region LowTemperature
		public bool LowTemperature
		{
			get => _LowTemperature;
			set
			{
				if (_LowTemperature != value)
				{
					_LowTemperature = value;

					PlanetSearchState.LowTemperature = _LowTemperature;
                    HandleChange();
                }
			}
		}
		private bool _LowTemperature = false;
		#endregion

		#region HighTemperature
		public bool HighTemperature
		{
			get => _HighTemperature;
			set
			{
				if (_HighTemperature != value)
				{
					_HighTemperature = value;

					PlanetSearchState.HighTemperature = _HighTemperature;
                    HandleChange();
                }
			}
		}
		private bool _HighTemperature = false;
		#endregion

		#region SearchFilter
		public string SearchFilter
		{
			get => _SearchFilter;
			set
			{
				if (_SearchFilter != value)
				{
					_SearchFilter = value;

					PlanetSearchState.SearchFilter = _SearchFilter;
                    HandleChange();
                }
			}
		}
		private string _SearchFilter = "";
		#endregion

		#region JumpDistanceSourcePlanet
		public string JumpDistanceSourcePlanet
		{
			get => _JumpDistanceSourcePlanet;
			set
			{
				if (_JumpDistanceSourcePlanet != value)
				{
					_JumpDistanceSourcePlanet = value;

					PlanetSearchState.JumpDistanceSourcePlanet = _JumpDistanceSourcePlanet;
                    HandleChange();
                }
			}
		}
		private string _JumpDistanceSourcePlanet = "";
		#endregion

		#region JumpDistanceLimit
		public int JumpDistanceLimit
		{
			get => _JumpDistanceLimit;
			set
			{
				if (_JumpDistanceLimit != value)
				{
					_JumpDistanceLimit = value;

					PlanetSearchState.JumpDistanceLimit = _JumpDistanceLimit;
                    HandleChange();
                }
			}
		}
		private int _JumpDistanceLimit = -1;
		#endregion

		public List<string> ExchangeLocationNames { get; set; } = new();

		// Can be modified from AllPlanets
		public List<PlanetSearchModel> Planets { get; set;} = null!;

		public PlanetSearchModel? SelectedPlanet { get; set; } = null;

        protected override void OnInitialized()
        {
            base.OnInitialized();
            PlanetSearchState = new PlanetSearchState(LocalStorage);
        }

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            await base.OnAfterRenderAsync(firstRender);

            if (firstRender)
            {
				PlanetMaterials = GlobalCaches.Materials.Items.ToList();
				PlanetMaterialTickers = PlanetMaterials
						.Select(m => m.Ticker)
						.ToList();

				ComexExchanges = GlobalCaches.ComexExchanges.Items.ToList();
				ExchangeLocationNames = ComexExchanges
					.Select(ce => ce.NaturalId)
					.ToList();

				AllPlanets = GlobalCaches.AllPlanetSearchModels.Items.ToList();
                CurrentProgress = 1;

                await PlanetSearchState.Load();
                var uriQuery = NavManager.ToAbsoluteUri(NavManager.Uri).Query;
				PlanetSearchState.SetFromUri(uriQuery);
                CurrentProgress = 2;
            }
        }

        public async Task CopyToClipboard(string message, string text)
        {
            try
            {
                await Clipboard.WriteTextAsync(text);
                Snackbar.Add(message, MudBlazor.Severity.Success);
            }
            catch
            {
                Snackbar.Add("Failed to copy text to clipboard", MudBlazor.Severity.Error);
            }
        }

		private void HandleChange()
		{
			NavManager.NavigateTo($"/planetsearch?{PlanetSearchState.GetUri()}");
			StateHasChanged();
		}
    }
}
