﻿namespace FIOAPIWeb
{
	public partial class MainLayout
	{
		private bool _drawerOpen = false;

		void DrawerToggle()
		{
			_drawerOpen = !_drawerOpen;
		}

		protected override void OnInitialized()
		{
			UserAppState.OnChange += StateHasChanged;
			base.OnInitialized();
		}

		public void Dispose()
		{
			UserAppState.OnChange -= StateHasChanged;
		}
	}
}
