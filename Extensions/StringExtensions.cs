﻿namespace FIOAPIWeb.Extensions
{
    public static class StringExtensions
    {
        /// <summary>
        /// Splits on sep
        /// </summary>
        /// <param name="str">String to split</param>
        /// <param name="sep">Separator</param>
        /// <returns>A list of strings</returns>
        public static List<string> SplitOn(this string str, string sep)
        {
            return str
                .Split(new string[] { sep }, StringSplitOptions.RemoveEmptyEntries)
                .ToList();
        }

        /// <summary>
        /// Splits on any number of separators
        /// </summary>
        /// <param name="str">The string to split</param>
        /// <param name="groupSep">The main group separator</param>
        /// <param name="internalSeperators">Separators per group</param>
        /// <returns>A List of List of string, where the inner list is limited to internalSeparators count + 1</returns>
        public static List<List<string>> SplitOnMultiple(this string str, string groupSep, params string[] internalSeperators)
        {
            List<List<string>> Result = new();

            var GroupRes = str.Split(new string[] { groupSep }, StringSplitOptions.RemoveEmptyEntries);
            foreach (var Group in GroupRes)
            {
                string GroupStr = Group;
                List<string> GroupList = new();
                foreach (var internalSeperator in internalSeperators)
                {
                    var res = GroupStr.Split(new string[] { internalSeperator }, StringSplitOptions.RemoveEmptyEntries);
                    if (res.Length != 2)
                    {
                        throw new InvalidOperationException("Encountered invalid split result");
                    }

                    GroupList.Add(res[0]);
                    GroupStr = res[1];
                }

                GroupList.Add(GroupStr);
                Result.Add(GroupList);
            }

            return Result;
        }
    }
}
