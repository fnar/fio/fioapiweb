﻿namespace FIOAPIWeb.Extensions
{
    /// <summary>
    /// DateTimeExtensions
    /// </summary>
    public static class DateTimeExtensions
    {
        private static readonly DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        /// <summary>
        /// Convert a DateTime to epoch ms (long)
        /// </summary>
        /// <param name="dateTime">DateTime object</param>
        /// <returns>milliseconds since epoch</returns>
        public static long ToEpochMs(this DateTime dateTime)
        {
            TimeSpan t = dateTime.ToUniversalTime() - epoch;
            return (long)t.TotalMilliseconds;
        }

        /// <summary>
        /// Convert unix time (ms since epoch) to DateTime
        /// </summary>
        /// <param name="unixTime">ms since epoch (long)</param>
        /// <returns>DateTime object</returns>
        public static DateTime FromUnixTime(this long unixTime)
        {
            return epoch.AddMilliseconds(unixTime);
        }
    }
}
