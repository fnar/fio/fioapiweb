﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Primitives;
using System.Text;

namespace FIOAPIWeb.Extensions
{
	public class NavigationResult
	{
        public string? Group { get; set; }
        public int? GroupId { get; set; }
        public List<string> Usernames { get; set; } = new();
	}

	public static class NavigationManagerExtensions
    {
        public static NavigationResult GetGroupOrUsernames(this NavigationManager navManager, string? ThisUser)
        {
            NavigationResult res = new();
            if (navManager.TryGetQueryValues("group", out List<string> groups))
            {
                if (groups.Count > 0)
                {
                    res.Group = groups.First();
                    var splitRes = res.Group.Split(new char[] { '-', '_', ':' });
                    foreach (var splitItem in splitRes)
                    {
                        if (int.TryParse(splitItem.Trim(), out int groupId))
                        {
                            res.GroupId = groupId;
                            break;
                        }
                    }
                }
            }

            if (navManager.TryGetQueryValues("username", out List<string> usernames))
            {
                res.Usernames.AddRange(usernames);
            }

            if (navManager.TryGetQueryValues("user", out List<string> users))
            {
                res.Usernames.AddRange(users);
            }

            res.Usernames = res.Usernames
                .Distinct()
                .ToList();

            if (res.Group == null && res.Usernames.Count == 0 && ThisUser != null)
            {
                res.Usernames.Add(ThisUser);
            }

            return res;
        }

        public static void SetGroupOrUsernames(this NavigationManager navManager, NavigationResult navResult)
        {
            if (navResult.Group != null)
            {
                navManager.SetQueryValue("group", navResult.Group);
            }

            if (navResult.Usernames.Count > 0)
            {
                navManager.SetQueryValues("username", navResult.Usernames);
            }
        }

        public static bool TryGetQueryString<T>(this NavigationManager navManager, string key, out T? value)
        {
            var uri = navManager.ToAbsoluteUri(navManager.Uri);

            if (QueryHelpers.ParseQuery(uri.Query).TryGetValue(key, out var valueFromQueryString))
            {
                if (typeof(T) == typeof(int) && int.TryParse(valueFromQueryString, out var valueAsInt))
                {
                    value = (T)(object)valueAsInt;
                    return true;
                }

                if (typeof(T) == typeof(string))
                {
                    value = (T)(object)valueFromQueryString.ToString();
                    return true;
                }

                if (typeof(T) == typeof(decimal) && decimal.TryParse(valueFromQueryString, out var valueAsDecimal))
                {
                    value = (T)(object)valueAsDecimal;
                    return true;
                }
            }

            value = default;
            return false;
        }

        public static bool TryGetQueryValues(this NavigationManager navManager, string key, out List<string> values)
        {
            values = new List<string>();

            var uri = navManager.ToAbsoluteUri(navManager.Uri);
            if (QueryHelpers.ParseQuery(uri.Query).TryGetValue(key, out var valueFromQueryString))
            {
                var resultSet = new HashSet<string>();
                var workingList = valueFromQueryString.ToList()!;
                foreach (var workingListItem in workingList)
                {
                    var splitItems = workingListItem!.Split(new char[] { '+', ' ' }, StringSplitOptions.RemoveEmptyEntries).ToList();
                    foreach (var splitItem in splitItems)
                    {
                        resultSet.Add(splitItem);
                    }
                }

                values = resultSet.ToList();
                return true;
            }

            return false;
        }

        public static void SetQueryValue(this NavigationManager navManager, string key, string? value, bool prepend = false) => SetQueryValues(navManager, key, value != null ? [value] : null, prepend);

        public static void SetQueryValues(this NavigationManager navManager, string key, List<string>? values, bool prepend = false)
        {
            bool bHaveValues = values?.Count > 0;

            var uri = navManager.ToAbsoluteUri(navManager.Uri);
            var parsedQuery = QueryHelpers.ParseQuery(uri.Query);
            if (parsedQuery.TryGetValue(key, out var _))
            {
                if (bHaveValues)
                {
                    // Modify the values
                    parsedQuery.Remove(key);
                    parsedQuery.Add(key, new StringValues(values!.ToArray()));
                }
                else
                {
                    // Remove the key from the URL
                    parsedQuery.Remove(key);
                }
            }
            else
            {
                if (bHaveValues)
                {
                    parsedQuery.Add(key, new StringValues(values!.ToArray()));
                }
            }

            var relativePathNoParams = navManager.GetRelativePathWithoutParams();

            if (parsedQuery.Count > 0)
            {
                var queryParams = new StringBuilder();
                if (prepend && parsedQuery.TryGetValue(key, out var value))
                {
                    queryParams.Append(key);
                    queryParams.Append('=');
                    queryParams.Append(value.ToString().Replace(",", "+").Replace(" ", "+"));
                    queryParams.Append('&');
                    parsedQuery.Remove(key);
                }
                
                foreach (var kvp in parsedQuery)
                {
                    queryParams.Append(kvp.Key);
                    queryParams.Append('=');
                    queryParams.Append(kvp.Value.ToString().Replace(",", "+").Replace(" ", "+"));
                    queryParams.Append('&');
                }
                queryParams.Remove(queryParams.Length - 1, 1);

                navManager.NavigateTo($"{relativePathNoParams}?{queryParams}");
            }
            else
            {
                navManager.NavigateTo($"{relativePathNoParams}");
            }
        }

        public static string GetRelativePathWithParams(this NavigationManager navManager)
        {
            return navManager.Uri.Replace(navManager.BaseUri, "");
        }

        public static string GetRelativePathWithoutParams(this NavigationManager navManager)
        {
            return navManager.Uri.Split("?", StringSplitOptions.RemoveEmptyEntries)[0].Replace(navManager.BaseUri, "");
        }
    }
}
