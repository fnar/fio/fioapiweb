﻿using FIOAPIWeb.Services;
using System.Collections.ObjectModel;

namespace FIOAPIWeb.Utils
{
    public class TimedCache<T> where T: class, ICloneable
    {
        public TimedCache(Func<Task<List<T>>> RefreshFunc, TimeSpan refreshThreshold)
        {
            _refreshFunc = RefreshFunc;
            _refreshThreshold = refreshThreshold;

            var task = RefreshFromFunc();
            task.Wait();

            if (Items.Count == 0)
            {
                throw new InvalidOperationException($"Failed to retrieve data. Likely FIOAPI is not responsive/returning invalid responses.");
            }
        }

        internal ReaderWriterLockSlim _lock = new();
        internal List<T> _cache = new();

        internal Func<Task<List<T>>> _refreshFunc;
        internal TimeSpan _refreshThreshold;

        internal DateTime _lastUpdate = DateTime.MinValue;

        public ReadOnlyCollection<T> Items
        {
            get
            {
                _lock.EnterReadLock();
                try
                {
                    ConditionalUpdate();

                    return _cache
                        .ToList()
                        .Clone()
                        .AsReadOnly();
                }
                finally
                {
                    _lock.ExitReadLock();
                }
            }
        }

        public void ConditionalUpdate()
        {
            if (_lastUpdate + _refreshThreshold > DateTime.Now)
            {
                _ = Task.Run(() => RefreshFromFunc());
                _lastUpdate = DateTime.Now;
            }
        }

        private async Task RefreshFromFunc()
        {
            var result = await _refreshFunc();
            if (result != null)
            {
                _lock.EnterWriteLock();
                try
                {
                    _cache = result;
                }
                finally
                {
                    _lock.ExitWriteLock();
                }
            }
        }
    }

    public static class GlobalCacheExtensions
    {
        /// <summary>
        /// Clones a List of T
        /// </summary>
        /// <typeparam name="T">An ICloneable</typeparam>
        /// <param name="theList">List to clone</param>
        /// <returns>A cloned list</returns>
        public static List<T> Clone<T>(this List<T> theList) where T : class, ICloneable
        {
            List<T> newList = new List<T>();
            theList.ForEach(i =>
            {
                newList.Add((T)i.Clone());
            });
            return newList;
        }
    }
}
