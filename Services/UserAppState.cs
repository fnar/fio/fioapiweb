﻿using System.Net;
using System.Text.Json;

using Blazored.LocalStorage;

namespace FIOAPIWeb.Services
{
    public enum AuthState
    {
        Loading,
        ServerNonResponsive,
        NoCredentials,
        CredentialsFailed,
        Authenticating,
        Authenticated
    }

    public class UserAppState
    {
        public event Action? OnChange;

        public AuthState AuthState
        {
            get
            {
                return _AuthState;
            }
            private set
            {
                if (_AuthState != value)
                {
                    _AuthState = value;
                    NotifyStateChanged();
                }
            }
        }
        private AuthState _AuthState { get; set; } = AuthState.Loading;

        public string AuthError
        {
            get
            {
                switch (AuthState)
                {
                    case AuthState.Loading:
                        return "Loading...";
                    case AuthState.ServerNonResponsive:
                        return "Server Non Responsive";
                    case AuthState.NoCredentials:
                        return "No stored credentials present";
                    case AuthState.CredentialsFailed:
                        return "Invalid credentials";
                    case AuthState.Authenticating:
                        return "Authenticating...";
                    case AuthState.Authenticated:
                        return "Authenticated";
                    default:
                        return "";
                }
            }
        }

        public bool IsAuthenticated
        {
            get
            {
                return AuthState == AuthState.Authenticated;
            }
        }

        public string? UserName { get; private set; }
        public bool IsAdmin { get; private set; }

        public int NumNotifications
        {
            get
            {
                return PendingGroupInvites.Count;
            }
        }

        public List<Payloads.PermissionResponse> Permissions { get; private set; } = new();
        public List<Payloads.Group> Groups { get; private set; } = new();
        public List<Payloads.InviteResponse> PendingGroupInvites { get; private set; } = new();
        public string DiscordName { get; private set; } = "";

        public string? BearerToken { get; private set; }

        private readonly IServiceProvider serviceProvider;

        private readonly ILocalStorageService localStorageService;

        const string LocalStorageUserNameKey = "UserName";
        const string LocalStorageBearerTokenKey = "BearerToken";

        public UserAppState(IServiceProvider sp)
        {
            serviceProvider = sp;
#if false
            using var scope = serviceProvider.CreateScope();
            localStorageService = scope.ServiceProvider.GetRequiredService<ILocalStorageService>();
#else
            // For a reason that is completely not clear to me, creating a scope to access the ILocalStorageService means
            // the JSRuntime isn't initialized. I'm pretty sure doing this is bad :tm:, but I don't know a way around this...
            localStorageService = serviceProvider.GetRequiredService<ILocalStorageService>();
#endif
        }

        private bool _Initialized = false;
        public async Task InitializeAsync()
        {
            if (!_Initialized)
            {
                bool HasUserName = await localStorageService.ContainKeyAsync(LocalStorageUserNameKey);
                if (!HasUserName)
                {
                    AuthState = AuthState.NoCredentials;
                    return;
                }
                UserName = await localStorageService.GetItemAsStringAsync(LocalStorageUserNameKey);

                bool HasBearerToken = await localStorageService.ContainKeyAsync(LocalStorageBearerTokenKey);
                if (!HasBearerToken)
                {
                    AuthState = AuthState.NoCredentials;
                    return;
                }

                BearerToken = await localStorageService.GetItemAsStringAsync(LocalStorageBearerTokenKey);

                await AuthenticateFromBearerTokenAsync(UserName!, BearerToken!);

                _Initialized = true;
            }
        }

        public async Task LoginAsync(string username, string password, bool remember)
        {
            await AuthenticateFromCredentialsAsync(username, password);
            if (remember && AuthState == AuthState.Authenticated)
            {
                await localStorageService.SetItemAsStringAsync(LocalStorageUserNameKey, UserName!);
                await localStorageService.SetItemAsStringAsync(LocalStorageBearerTokenKey, BearerToken!);
            }
        }

        public async Task LogoutAsync()
        {
            await localStorageService.RemoveItemAsync(LocalStorageBearerTokenKey);
            AuthState = AuthState.NoCredentials;
        }

        public async Task Relogin()
        {
            if (AuthState == AuthState.Authenticated)
            {
                AuthState = AuthState.Authenticating;
                await AuthenticateFromBearerTokenAsync(UserName!, BearerToken!);
            }

            NotifyStateChanged();
        }

        public async Task Refresh(bool bNotifyStateChanged = true)
        {
            await RefreshPermissionAllowances(bNotifyStateChanged: false);
            await RefreshGroupMemberships(bNotifyStateChanged: false);
            await RefreshPendingGroupInvites(bNotifyStateChanged: false);
            await RefreshDiscordName(bNotifyStateChanged: false);

            if (bNotifyStateChanged)
            {
                NotifyStateChanged();
            }
        }

        private async Task AuthenticateFromCredentialsAsync(string username, string password)
        {
            AuthState = AuthState.Authenticating;
            UserName = username;
            BearerToken = null;
            IsAdmin = false;

            var payload = JsonSerializer.Serialize(new Payloads.Login()
            {
                UserName = username,
                Password = password
            });

            var request = new Web.Request(HttpMethod.Post, "/auth/login", null, payload, "application/json");
            var response = await request.GetResponseAsync<Payloads.LoginResponse>();
            switch (request.StatusCode)
            {
                case HttpStatusCode.OK:
                    // Success
                    UserName = username;
                    BearerToken = response!.Token;
                    await AuthenticateFromBearerTokenAsync(username, response!.Token);
                    break;

                case HttpStatusCode.Unauthorized:
                    // Bad credentials
                    AuthState = AuthState.CredentialsFailed;
                    break;

                default:
                    // Non-responsive
                    AuthState = AuthState.ServerNonResponsive;
                    break;
            }
        }

        private async Task AuthenticateFromBearerTokenAsync(string username, string bearer)
        {
            AuthState = AuthState.Authenticating;
            UserName = username;
            BearerToken = null;
            IsAdmin = false;

            var request = new Web.Request(HttpMethod.Get, "/auth", bearer);
            await request.GetResultNoResponse();
            var AuthStatusCode = request.StatusCode;

            bool isAdmin = false;
            if (request.StatusCode == HttpStatusCode.OK)
            {
                request = new Web.Request(HttpMethod.Get, "/admin", bearer);
                await request.GetResultNoResponse();
                isAdmin = request.StatusCode == HttpStatusCode.OK;
            }

            switch (AuthStatusCode)
            {
                case HttpStatusCode.OK:
                    // Success
                    BearerToken = bearer;
                    IsAdmin = isAdmin;
                    await Refresh(bNotifyStateChanged: false);
                    AuthState = AuthState.Authenticated;
                    break;

                case HttpStatusCode.Unauthorized:
                    // Bad credentials
                    AuthState = AuthState.CredentialsFailed;
                    break;

                default:
                    // Non-responsive
                    AuthState = AuthState.ServerNonResponsive;
                    break;
            }
        }

        public async Task RefreshPermissionAllowances(bool bNotifyStateChanged = true)
        {
            var request = new Web.Request(HttpMethod.Get, "/permission/granted", BearerToken);
            var perms = await request.GetResponseAsync<List<Payloads.PermissionResponse>>();
            if (request.StatusCode == HttpStatusCode.OK && perms != null)
            {
                Permissions = perms;
            }

            if (bNotifyStateChanged)
            {
                NotifyStateChanged();
            }
        }

        public async Task RefreshGroupMemberships(bool bNotifyStateChanged = true)
        {
            var request = new Web.Request(HttpMethod.Get, "/group/list", BearerToken);
            var groupMemberships = await request.GetResponseAsync<List<Payloads.Group>>();
            if (request.StatusCode == HttpStatusCode.OK && groupMemberships != null)
            {
                Groups = groupMemberships;
            }

            if (bNotifyStateChanged)
            {
                NotifyStateChanged();
            }
        }

        public async Task RefreshPendingGroupInvites(bool bNotifyStateChanged = true)
        {
            var request = new Web.Request(HttpMethod.Get, "/group/list/invite", BearerToken);
            var groupPendingInvites = await request.GetResponseAsync<List<Payloads.InviteResponse>>();
            if (request.StatusCode == HttpStatusCode.OK && groupPendingInvites != null)
            {
                PendingGroupInvites = groupPendingInvites;
            }

            if (bNotifyStateChanged)
            {
                NotifyStateChanged();
            }
        }

        public async Task RefreshDiscordName(bool bNotifyStateChanged = true)
        {
            var request = new Web.Request(HttpMethod.Get, "/auth/discord", BearerToken);
            var discordName = await request.GetResultAsStringAsync();
            if (request.StatusCode == HttpStatusCode.OK && discordName != null)
            {
                DiscordName = discordName;
            }

            if (bNotifyStateChanged)
            {
                NotifyStateChanged();
            }
        }

        private void NotifyStateChanged() => OnChange?.Invoke();
    }
}
