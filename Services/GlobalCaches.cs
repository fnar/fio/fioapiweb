﻿using System.Data;
using System.Net;

using FIOAPIWeb.PageHelpers.PlanetSearch;
using FIOAPIWeb.Payloads;
using FIOAPIWeb.Utils;

namespace FIOAPIWeb.Services
{
    public static class GlobalCaches
    {
        public static void Init()
        {
            _ = AllUsernames.Items;
            _ = ComexExchanges.Items;
            _ = Materials.Items;
            _ = AllPlanetSearchModels.Items;
        }

        public static TimedCache<string> AllUsernames { get; set; } = new(
            async () =>
            {
				var allUsersRequest = new Web.Request(HttpMethod.Get, "/auth/users", Constants.APIKey);
				var AllUsers = await allUsersRequest.GetResponseAsync<List<string>>();
                return AllUsers != null ? AllUsers : new List<string>();
			},
            TimeSpan.FromSeconds(30.0)
            );

        public static TimedCache<ComexExchange> ComexExchanges { get; set; } = new(
            async () =>
            {
				var request = new Web.Request(HttpMethod.Get, "/global/comexexchanges", Constants.APIKey);
				var ComexExchangesResponse = await request.GetResponseAsync<List<ComexExchange>>();
				if (request.StatusCode == System.Net.HttpStatusCode.OK && ComexExchangesResponse != null)
                {
					var ComexExchanges = ComexExchangesResponse
						.OrderBy(c => c.NaturalId)
						.ToList();
                    return ComexExchanges;
                }

                return new List<ComexExchange>();
			},
            TimeSpan.FromDays(1)
            );

        public static TimedCache<Material> Materials { get; set; } = new(
            async () =>
            {
				var request = new Web.Request(HttpMethod.Get, "/material", Constants.APIKey);
				var allMaterials = await request.GetResponseAsync<List<Material>>();
				if (request.StatusCode == HttpStatusCode.OK && allMaterials != null)
				{
					allMaterials = allMaterials
						.OrderBy(t => t.Ticker)
						.ToList();
                    return allMaterials;
				}

                return new List<Material>();
			},
            TimeSpan.FromDays(1)
            );

        public static TimedCache<PlanetSearchModel> AllPlanetSearchModels { get; private set; } = new(
            async () =>
            {
                var ResourceMaterials = Materials.Items
                    .Where(m => m.IsResource)
                    .ToList();
                var ComexExchangeNaturalIds = ComexExchanges.Items
                    .Select(cx => cx.NaturalId)
                    .ToList();

                var allModels = await PlanetSearchModel.GetAllPlanets(ResourceMaterials, ComexExchangeNaturalIds);
				if (allModels != null)
                {
                    return allModels;
                }

                return new List<PlanetSearchModel>();
            },
            TimeSpan.FromMinutes(15)
            );
    }
}
