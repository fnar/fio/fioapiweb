﻿namespace FIOAPIWeb.Payloads
{
    /// <summary>
    /// Invite response
    /// </summary>
    public class InviteResponse
    {
        /// <summary>
        /// GroupId
        /// </summary>
        public int GroupId { get; set; }

        /// <summary>
        /// The group name
        /// </summary>
        public string GroupName { get; set; } = null!;

        /// <summary>
        /// If this is an invite for an admin role
        /// </summary>
        public bool Admin { get; set; } = false;

        /// <summary>
        /// The permissions for the group
        /// </summary>
        public Permissions Permisssions { get; set; } = new();
    }
}
