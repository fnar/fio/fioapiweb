﻿namespace FIOAPIWeb.Payloads
{
	/// <summary>
	/// StorageItem
	/// </summary>
	public class StorageItem
	{
		/// <summary>
		/// ItemId
		/// </summary>
		public string ItemId { get; set; } = null!;

		/// <summary>
		/// MaterialId
		/// </summary>
		public string? MaterialId { get; set; }

		/// <summary>
		/// MaterialTicker
		/// </summary>
		public string? MaterialTicker { get; set; }

		/// <summary>
		/// Amount
		/// </summary>
		public int? Amount { get; set; }

		/// <summary>
		/// Weight
		/// </summary>
		public double Weight { get; set; }

		/// <summary>
		/// Volume
		/// </summary>
		public double Volume { get; set; }

		/// <summary>
		/// Type
		/// </summary>
		public string Type { get; set; } = null!;

		/// <summary>
		/// ValueCurrencyCode
		/// </summary>
		public string? ValueCurrencyCode { get; set; }

		/// <summary>
		/// Value
		/// </summary>
		public double? Value { get; set; }
	}
}
