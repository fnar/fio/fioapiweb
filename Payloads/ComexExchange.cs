﻿namespace FIOAPIWeb.Payloads
{
    public class ComexExchange : ICloneable
    {
        /// <summary>
        /// Exchange Id
        /// </summary>
        public string ComexExchangeId { get; set; } = null!;

        /// <summary>
        /// Code
        /// </summary>
        public string Code { get; set; } = null!;

        /// <summary>
        /// Name of the exchange
        /// </summary>
        public string Name { get; set; } = null!;

        /// <summary>
        /// Decimals to show
        /// </summary>
        public int Decimals { get; set; }

        /// <summary>
        /// NumericCode
        /// </summary>
        public int NumericCode { get; set; }

        /// <summary>
        /// Exchange NaturalId
        /// </summary>
        public string NaturalId { get; set; } = null!;

        /// <summary>
        /// SystemId
        /// </summary>
        public string SystemId { get; set; } = null!;

        /// <summary>
        /// SystemName
        /// </summary>
        public string SystemName { get; set; } = null!;

        /// <summary>
        /// SystemNaturalId
        /// </summary>
        public string SystemNaturalId { get; set; } = null!;

        /// <summary>
        /// The name of the user that submitted this
        /// </summary>
        public string UserNameSubmitted { get; set; } = null!;

        /// <summary>
        /// Timestamp
        /// </summary>
        public DateTime Timestamp { get; set; }

		public object Clone()
		{
            return new ComexExchange()
            {
				ComexExchangeId = this.ComexExchangeId,
                Code = this.Code,
                Name = this.Name,
                Decimals = this.Decimals,
                NumericCode = this.NumericCode,
                NaturalId = this.NaturalId,
                SystemId = this.SystemId,
                SystemName = this.SystemName,
                SystemNaturalId = this.SystemNaturalId,
                UserNameSubmitted = this.UserNameSubmitted,
                Timestamp = this.Timestamp,
			};
		}
	}
}
