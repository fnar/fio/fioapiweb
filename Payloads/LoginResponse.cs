﻿using System.ComponentModel.DataAnnotations;

namespace FIOAPIWeb.Payloads
{
    public class LoginResponse
    {
        public string Token { get; set; } = "";

        public bool IsAdministrator { get; set; } = false;
    }
}
