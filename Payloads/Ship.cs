﻿using CsvHelper.Configuration.Attributes;
using Microsoft.VisualBasic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace FIOAPIWeb.Payloads
{
	/// <summary>
	/// Ship
	/// </summary>
	public class Ship
	{
		/// <summary>
		/// ShipId
		/// </summary>
		public string ShipId { get; set; } = null!;

		/// <summary>
		/// StoreId (Cargo)
		/// </summary>
		public string StoreId { get; set; } = null!;

		/// <summary>
		/// STLFuelStoreId
		/// </summary>
		public string STLFuelStoreId { get; set; } = null!;

		/// <summary>
		/// FTLFuelStoreId
		/// </summary>
		public string FTLFuelStoreId { get; set; } = null!;

		/// <summary>
		/// Registration
		/// </summary>
		public string Registration { get; set; } = null!;

		/// <summary>
		/// Name
		/// </summary>
		public string? Name { get; set; }

		/// <summary>
		/// CommisioningTime
		/// </summary>
		public DateTime CommisioningTime { get; set; }

		/// <summary>
		/// BlueprintNaturalId
		/// </summary>
		public string BlueprintNaturalId { get; set; } = null!;

		/// <summary>
		/// CurrentLocationId
		/// </summary>
		public string? CurrentLocationId { get; set; }

		/// <summary>
		/// CurrentLocationName
		/// </summary>
		public string? CurrentLocationName { get; set; }

		/// <summary>
		/// CurrentLocationNaturalId
		/// </summary>
		public string? CurrentLocationNaturalId { get; set; }

		/// <summary>
		/// FlightId
		/// </summary>
		public string? FlightId { get; set; }

		/// <summary>
		/// Acceleration
		/// </summary>
		public double Acceleration { get; set; }

		/// <summary>
		/// Thrust
		/// </summary>
		public double Thrust { get; set; }

		/// <summary>
		/// Mass
		/// </summary>
		public double Mass { get; set; }

		/// <summary>
		/// OperatingEmptyMass
		/// </summary>
		public double OperatingEmptyMass { get; set; }

		/// <summary>
		/// Volume
		/// </summary>
		public double Volume { get; set; }

		/// <summary>
		/// ReactorPower
		/// </summary>
		public double? ReactorPower { get; set; }

		/// <summary>
		/// EmitterPower
		/// </summary>
		public double EmitterPower { get; set; }

		/// <summary>
		/// STLFuelFlowRate
		/// </summary>
		public double STLFuelFlowRate { get; set; }

		/// <summary>
		/// OperatingTimeSTL
		/// </summary>
		public long OperatingTimeSTLMilliseconds { get; set; }

		/// <summary>
		/// OperatingTimeFTLMilliseconds
		/// </summary>
		public long OperatingTimeFTLMilliseconds { get; set; }

		/// <summary>
		/// Condition
		/// </summary>
		public double Condition { get; set; }

		/// <summary>
		/// LastRepair
		/// </summary>
		public DateTime? LastRepair { get; set; }

		/// <summary>
		/// RepairMaterials
		/// </summary>
		public virtual List<ShipRepairMaterial> RepairMaterials { get; set; } = new();

		/// <summary>
		/// The timestamp of the data
		/// </summary>
		public DateTime Timestamp { get; set; }
	}
}
