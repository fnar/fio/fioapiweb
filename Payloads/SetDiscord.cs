﻿namespace FIOAPIWeb.Payloads
{
    /// <summary>
    /// SetDiscord
    /// </summary>
    public class SetDiscord
    {
        /// <summary>
        /// DiscordName
        /// </summary>
        public string DiscordName { get; set; } = "";
    }
}
