﻿namespace FIOAPIWeb.Payloads
{
    /// <summary>
    /// GroupPendingInvite model
    /// </summary>
    public class GroupPendingInvite
    {
        /// <summary>
        /// UserName
        /// </summary>
        public string UserName { get; set; } = null!;

        /// <summary>
        /// If the invite is for an admin role
        /// </summary>
        public bool Admin { get; set; } = false;
    }
}
