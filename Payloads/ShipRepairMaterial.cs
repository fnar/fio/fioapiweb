﻿namespace FIOAPIWeb.Payloads
{
	/// <summary>
	/// ShipRepairMaterial
	/// </summary>
	public class ShipRepairMaterial
	{
		/// <summary>
		/// ShipRepairMaterialId: {ShipId}-{MaterialId}
		/// </summary>
		public string ShipRepairMaterialId { get; set; } = null!;

		/// <summary>
		/// MaterialId
		/// </summary>
		public string MaterialId { get; set; } = null!;

		/// <summary>
		/// MaterialTicker
		/// </summary>
		public string MaterialTicker { get; set; } = null!;

		/// <summary>
		/// Amount
		/// </summary>
		public int Amount { get; set; }
	}
}
