﻿namespace FIOAPIWeb.Payloads
{
	/// <summary>
	/// WorkforceNeed
	/// </summary>
	public class WorkforceNeed
	{
		/// <summary>
		/// Level
		/// </summary>
		public string Level { get; set; } = null!;

		/// <summary>
		/// Category
		/// </summary>
		public string Category { get; set; } = null!;

		/// <summary>
		/// Essential
		/// </summary>
		public bool Essential { get; set; }

		/// <summary>
		/// MaterialId
		/// </summary>
		public string MaterialId { get; set; } = null!;

		/// <summary>
		/// MaterialTicker
		/// </summary>
		public string MaterialTicker { get; set; } = null!;

		/// <summary>
		/// MaterialName
		/// </summary>
		public string MaterialName { get; set; } = null!;

		/// <summary>
		/// Satisfaction
		/// </summary>
		public double Satisfaction { get; set; }

		/// <summary>
		/// UnitsPerInterval
		/// </summary>
		public double UnitsPerInterval { get; set; }

		/// <summary>
		/// UnitsPer100
		/// </summary>
		public double UnitsPer100 { get; set; }
	}
}
