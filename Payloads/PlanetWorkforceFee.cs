﻿namespace FIOAPIWeb.Payloads
{
    /// <summary>
    /// PlanetWorkforceFee
    /// </summary>
    public class PlanetWorkforceFee : ICloneable
    {

        /// <summary>
        /// Category - The building/expertise category
        /// </summary>
        public string Category { get; set; } = null!;

        /// <summary>
        /// The tier of worker
        /// </summary>
        public string WorkforceLevel { get; set; } = null!;

        /// <summary>
        /// Fee
        /// </summary>
        public double? Fee { get; set; }

        /// <summary>
        /// Currency
        /// </summary>
        public string? Currency { get; set; } = null!;

        /// <summary>
        /// Clone
        /// </summary>
        /// <returns>A copy</returns>
        public object Clone()
        {
            return new PlanetWorkforceFee()
            {
                Category = Category,
                WorkforceLevel = WorkforceLevel,
                Fee = Fee,
                Currency = Currency,
            };
        }
    }
}
