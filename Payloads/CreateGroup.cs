﻿namespace FIOAPIWeb.Payloads
{
    /// <summary>
    /// The Create payload
    /// </summary>
    public class Create
    {
        /// <summary>
        /// An optionally requested Id number
        /// </summary>
        /// <remark>If the Id is taken, this will return NotAcceptable (HTTP 406)</remark>
        public int RequestedId { get; set; } = 0;

        /// <summary>
        /// The name of the group
        /// </summary>
        public string GroupName { get; set; } = "";

        /// <summary>
        /// All the invites
        /// </summary>
        public List<UserInvite> Invites { get; set; } = new List<UserInvite>();

        /// <summary>
        /// Permissions for this group
        /// </summary>
        public Permissions Permissions { get; set; } = new Permissions();
    }
}
