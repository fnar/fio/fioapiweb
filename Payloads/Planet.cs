﻿namespace FIOAPIWeb.Payloads
{
    /// <summary>
    /// Planet
    /// </summary>
    public class Planet : ICloneable
    {
        #region Constants
        public const string InvalidAPEXID = "00000000000000000000000000000000";

        /// <summary>
        /// NotFertile
        /// </summary>
        public const double NotFertile = -1.0;

        /// <summary>
        /// LowGravityThreshold
        /// </summary>
        public const double LowGravityThreshold = 0.25;

        /// <summary>
        /// HighGravityThreshold
        /// </summary>
        public const double HighGravityThreshold = 2.5;

        /// <summary>
        /// LowPressureThreshold
        /// </summary>
        public const double LowPressureThreshold = 0.25;

        /// <summary>
        /// HighPressureThreshold
        /// </summary>
        public const double HighPressureThreshold = 2.0;

        /// <summary>
        /// LowTemperatureThreshold
        /// </summary>
        public const double LowTemperatureThreshold = -25.0;

        /// <summary>
        /// HighTemperatureThreshold
        /// </summary>
        public const double HighTemperatureThreshold = 75.0;
        #endregion

        /// <summary>
        /// PlanetId
        /// </summary>
        public string PlanetId { get; set; } = null!;

        /// <summary>
        /// NaturalId
        /// </summary>
        public string NaturalId { get; set; } = null!;

        /// <summary>
        /// Name
        /// </summary>
        public string? Name { get; set; } = null;

        /// <summary>
        /// NamerId
        /// </summary>
        public string? NamerId { get; set; } = null;

        /// <summary>
        /// NamerUserName
        /// </summary>
        public string? NamerUserName { get; set; } = null;

        /// <summary>
        /// NamedTimestamp
        /// </summary>
        public DateTime? NamedTimestamp { get; set; }

        /// <summary>
        /// Nameable
        /// </summary>
        public bool Nameable { get; set; }

        /// <summary>
        /// Gravity
        /// </summary>
        public double Gravity { get; set; }

        /// <summary>
        /// MagneticField
        /// </summary>
        public double MagneticField { get; set; }

        /// <summary>
        /// Mass
        /// </summary>
        public double Mass { get; set; }

        /// <summary>
        /// MassEarth
        /// </summary>
        public double MassEarth { get; set; }

        /// <summary>
        /// SemiMajorAxis
        /// </summary>
        public double SemiMajorAxis { get; set; }

        /// <summary>
        /// Eccentricity
        /// </summary>
        public double Eccentricity { get; set; }

        /// <summary>
        /// Inclination
        /// </summary>
        public double Inclination { get; set; }

        /// <summary>
        /// RightAscension
        /// </summary>
        public double RightAscension { get; set; }

        /// <summary>
        /// Periapsis
        /// </summary>
        public double Periapsis { get; set; }

        /// <summary>
        /// OrbitIndex
        /// </summary>
        public int OrbitIndex { get; set; }

        /// <summary>
        /// Pressure
        /// </summary>
        public double Pressure { get; set; }

        /// <summary>
        /// Radiation
        /// </summary>
        public double Radiation { get; set; }

        /// <summary>
        /// Radius
        /// </summary>
        public double Radius { get; set; }

        /// <summary>
        /// Sunlight
        /// </summary>
        public double Sunlight { get; set; }

        /// <summary>
        /// Temperature
        /// </summary>
        public double Temperature { get; set; }

        /// <summary>
        /// Fertility
        /// </summary>
        public double? Fertility { get; set; }

        /// <summary>
        /// Surface
        /// </summary>
        public bool Surface { get; set; }

        /// <summary>
        /// Plots
        /// </summary>
        public int Plots { get; set; }

        /// <summary>
        /// CountryId
        /// </summary>
        public string? CountryId { get; set; }

        /// <summary>
        /// CountryName
        /// </summary>
        public string? CountryName { get; set; }

        /// <summary>
        /// CountryCode
        /// </summary>
        public string? CountryCode { get; set; }

        /// <summary>
        /// GovernorUserId
        /// </summary>

        public string? GovernorUserId { get; set; }

        /// <summary>
        /// GovernorUserName
        /// </summary>
        public string? GovernorUserName { get; set; }

        /// <summary>
        /// GoverningCompanyId
        /// </summary>
        public string? GoverningCompanyId { get; set; }

        /// <summary>
        /// GoverningCompanyName
        /// </summary>
        public string? GoverningCompanyName { get; set; }

        /// <summary>
        /// GoverningCompanyCode
        /// </summary>
        public string? GoverningCompanyCode { get; set; }

        /// <summary>
        /// CurrencyNumericCode
        /// </summary>
        public int? CurrencyNumericCode { get; set; }

        /// <summary>
        /// CurrencyCode
        /// </summary>
        public string? CurrencyCode { get; set; }

        /// <summary>
        /// CurrencyName
        /// </summary>
        public string? CurrencyName { get; set; }

        /// <summary>
        /// CollectionCurrencyNumericCode
        /// </summary>
        public int? CollectionCurrencyNumericCode { get; set; }

        /// <summary>
        /// CollectionCurrencyCode
        /// </summary>
        public string? CollectionCurrencyCode { get; set; }

        /// <summary>
        /// CollectionCurrencyName
        /// </summary>
        public string? CollectionCurrencyName { get; set; }

        /// <summary>
        /// LocalMarketFeeBase
        /// </summary>
        public double? LocalMarketFeeBase { get; set; }

        /// <summary>
        /// LocalMarketTimeFactor
        /// </summary>
        public double? LocalMarketTimeFactor { get; set; }

        /// <summary>
        /// WarehouseFee
        /// </summary>
        public double? WarehouseFee { get; set; }

        /// <summary>
        /// PopulationId
        /// </summary>
        public string? PopulationId { get; set; }

        /// <summary>
        /// ChamberOfCommerceId
        /// </summary>
        public string? ChamberOfCommerceId { get; set; }

        /// <summary>
        /// WarehouseId
        /// </summary>
        public string? WarehouseId { get; set; }


        /// <summary>
        /// LocalMarketId
        /// </summary>
        public string? LocalMarketId { get; set; }

        /// <summary>
        /// AdministrationCenterId
        /// </summary>
        public string? AdministrationCenterId { get; set; }

        /// <summary>
        /// ShipyardId
        /// </summary>
        public string? ShipyardId { get; set; }

        /// <summary>
        /// SupportsGhostSizes
        /// </summary>
        public bool SupportsGhostSites { get; set; }

        /// <summary>
        /// COGCProgram
        /// </summary>
        public string? CurrentCOGCProgram { get; set; }

        /// <summary>
        /// LastCOGCProgramUpdate
        /// </summary>
        public DateTime CurrentCOGCProgramEndTime { get; set; } = DateTime.MinValue;

        /// <summary>
        /// The name of the user that submitted this
        /// </summary>
        public string UserNameSubmitted { get; set; } = null!;

        /// <summary>
        /// Timestamp
        /// </summary>
        public DateTime Timestamp { get; set; }

        // DEV NOTE: If you add another virtual List here, make sure to update the queries here to `Include` them:
        // * PlanetController.PutCOGC()
        // * EntityCaches.InitializeEntityCaches
        // Basically, we need to ensure the cache is up to date.

        /// <summary>
        /// Resources
        /// </summary>
        public virtual List<PlanetResource> Resources { get; set; } = new();

        /// <summary>
        /// WorkforceFees
        /// </summary>
        public virtual List<PlanetWorkforceFee> WorkforceFees { get; set; } = new();

        /// <summary>
        /// COGCPrograms
        /// </summary>
        public virtual List<PlanetCOGCProgram> COGCPrograms { get; set; } = new();

        /// <summary>
        /// PopulationReports
        /// </summary>
        public virtual List<PlanetPopulationReport> PopulationReports { get; set; } = new();

        /// <summary>
        /// PopulationProjects
        /// </summary>
        public virtual List<PlanetPopulationProject> PopulationProjects { get; set; } = new();

        /// <summary>
        /// HasShipyard
        /// </summary>
        public bool HasShipyard { get; set; }

        /// <summary>
        /// HasAdministrationCenter
        /// </summary>
        public bool HasAdministrationCenter { get; set; }

        /// <summary>
        /// HasCOGC
        /// </summary>
        public bool HasChamberOfCommerce { get; set; }

        /// <summary>
        /// HasWarehouse
        /// </summary>
        public bool HasWarehouse { get; set; }

        /// <summary>
        /// HasLocalMarket
        /// </summary>
        public bool HasLocalMarket { get; set; }

        /// <summary>
        /// If planet requires MCG
        /// </summary>
        public bool Rocky { get; set; }

        /// <summary>
        /// If planet requires AEF
        /// </summary>
        public bool Gaseous { get; set; }

        /// <summary>
        /// If planet requires MGC
        /// </summary>
        public bool LowGravity { get; set; }

        /// <summary>
        /// If planet requires BL
        /// </summary>
        public bool HighGravity { get; set; }

        /// <summary>
        /// If planet requires SEA
        /// </summary>
        public bool LowPressure { get; set; }

        /// <summary>
        /// If planet requires HSE
        /// </summary>
        public bool HighPressure { get; set; }

        /// <summary>
        /// If planet requires INS
        /// </summary>
        public bool LowTemperature { get; set; }

        /// <summary>
        /// If planet requires TSH
        /// </summary>
        public bool HighTemperature { get; set; }

        /// <summary>
        /// If the planet is fertile
        /// </summary>
        public bool Fertile { get; set; }

        /// <summary>
        /// Clone
        /// </summary>
        /// <returns>A copy</returns>
        public object Clone()
        {
            var copy = new Planet();
            copy.PlanetId = PlanetId;
            copy.NaturalId = NaturalId;
            copy.Name = Name;
            copy.NamerId = NamerId;
            copy.NamerUserName = NamerUserName;
            copy.NamedTimestamp = NamedTimestamp;
            copy.Nameable = Nameable;
            copy.Gravity = Gravity;
            copy.MagneticField = MagneticField;
            copy.Mass = Mass;
            copy.MassEarth = MassEarth;
            copy.SemiMajorAxis = SemiMajorAxis;
            copy.Eccentricity = Eccentricity;
            copy.Inclination = Inclination;
            copy.RightAscension = RightAscension;
            copy.Periapsis = Periapsis;
            copy.OrbitIndex = OrbitIndex;
            copy.Pressure = Pressure;
            copy.Radiation = Radiation;
            copy.Radius = Radius;
            copy.Sunlight = Sunlight;
            copy.Temperature = Temperature;
            copy.Fertility = Fertility;
            copy.Surface = Surface;
            copy.Plots = Plots;
            copy.CountryId = CountryId;
            copy.CountryName = CountryName;
            copy.CountryCode = CountryCode;
            copy.GovernorUserId = GovernorUserId;
            copy.GovernorUserName = GovernorUserName;
            copy.GoverningCompanyId = GoverningCompanyId;
            copy.GoverningCompanyName = GoverningCompanyName;
            copy.GoverningCompanyCode = GoverningCompanyCode;
            copy.CurrencyNumericCode = CurrencyNumericCode;
            copy.CurrencyCode = CurrencyCode;
            copy.CurrencyName = CurrencyName;
            copy.CollectionCurrencyNumericCode = CollectionCurrencyNumericCode;
            copy.CollectionCurrencyCode = CollectionCurrencyCode;
            copy.CollectionCurrencyName = CollectionCurrencyName;
            copy.LocalMarketFeeBase = LocalMarketFeeBase;
            copy.LocalMarketTimeFactor = LocalMarketTimeFactor;
            copy.WarehouseFee = WarehouseFee;
            copy.PopulationId = PopulationId;
            copy.ChamberOfCommerceId = ChamberOfCommerceId;
            copy.WarehouseId = WarehouseId;
            copy.LocalMarketId = LocalMarketId;
            copy.AdministrationCenterId = AdministrationCenterId;
            copy.ShipyardId = ShipyardId;
            copy.SupportsGhostSites = SupportsGhostSites;
            copy.CurrentCOGCProgram = CurrentCOGCProgram;
            copy.CurrentCOGCProgramEndTime = CurrentCOGCProgramEndTime;

            Resources.ForEach(r =>
            {
                var rCopy = (PlanetResource)r.Clone();
                copy.Resources.Add(rCopy);
            });

            WorkforceFees.ForEach(wf =>
            {
                var wfCopy = (PlanetWorkforceFee)wf.Clone();
                copy.WorkforceFees.Add(wfCopy);
            });


            COGCPrograms.ForEach(p =>
            {
                var pCopy = (PlanetCOGCProgram)p.Clone();
                copy.COGCPrograms.Add(pCopy);
            });

            PopulationReports.ForEach(pr =>
            {
                var prCopy = (PlanetPopulationReport)pr.Clone();
                copy.PopulationReports.Add(prCopy);
            });

            return copy;
        }
    }
}
