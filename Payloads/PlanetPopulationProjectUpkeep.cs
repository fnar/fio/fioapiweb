﻿namespace FIOAPIWeb.Payloads
{
    /// <summary>
    /// PlanetPopulationProjectUpkeep
    /// </summary>
    public class PlanetPopulationProjectUpkeep : ICloneable
    {
        /// <summary>
        /// Stored
        /// </summary>
        public int Stored { get; set; }

        /// <summary>
        /// StoreCapacity
        /// </summary>
        public int StoreCapacity { get; set; }

        /// <summary>
        /// Duration
        /// </summary>
        public int Duration { get; set; }

        /// <summary>
        /// NextTick
        /// </summary>
        public DateTime NextTick { get; set; }

        /// <summary>
        /// Ticker
        /// </summary>
        public string Ticker { get; set; } = null!;

        /// <summary>
        /// Amount
        /// </summary>
        public int Amount { get; set; }

        /// <summary>
        /// CurrentAmount
        /// </summary>
        public int CurrentAmount { get; set; }


        /// <summary>
        /// Cone
        /// </summary>
        /// <returns>A copy</returns>
        public object Clone()
        {
            return new PlanetPopulationProjectUpkeep()
            {
                Stored = Stored,
                StoreCapacity = StoreCapacity,
                Duration = Duration,
                NextTick = NextTick,
                Ticker = Ticker,
                Amount = Amount,
                CurrentAmount = CurrentAmount,
            };
        }
    }
}
