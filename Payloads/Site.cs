﻿using Microsoft.VisualBasic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace FIOAPIWeb.Payloads
{
	/// <summary>
	/// Sites
	/// </summary>
	public class Site
	{
		/// <summary>
		/// SiteId
		/// </summary>
		public string SiteId { get; set; } = null!;

		/// <summary>
		/// LocationId
		/// </summary>
		public string LocationId { get; set; } = null!;

		/// <summary>
		/// LocationNaturalId
		/// </summary>
		public string LocationNaturalId { get; set; } = null!;

		/// <summary>
		/// LocationName
		/// </summary>
		public string LocationName { get; set; } = null!;

		/// <summary>
		/// Founded
		/// </summary>
		public DateTime Founded { get; set; } = DateTime.MinValue;

		/// <summary>
		/// Area
		/// </summary>
		public int Area { get; set; }

		/// <summary>
		/// InvestedPermits
		/// </summary>
		public int InvestedPermits { get; set; }

		/// <summary>
		/// MaximumPermits
		/// </summary>
		public int MaximumPermits { get; set; }

		/// <summary>
		/// Platforms
		/// </summary>
		public virtual List<SiteBuilding> Buildings { get; set; } = new();

		/// <summary>
		/// The timestamp of the data
		/// </summary>
		public DateTime Timestamp { get; set; }
	}
}
