﻿namespace FIOAPIWeb.Payloads
{
    public class Login
    {
        public string UserName { get; set; } = null!;

        public string Password { get; set; } = null!;
    }
}
