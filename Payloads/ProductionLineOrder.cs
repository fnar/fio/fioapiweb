﻿using FIOAPIWeb.Extensions;
using Microsoft.VisualBasic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace FIOAPIWeb.Payloads
{
	/// <summary>
	/// ProductionLineOrder
	/// </summary>
	public class ProductionLineOrder
	{
		/// <summary>
		/// ProductionLineOrderId
		/// </summary>
		public string ProductionLineOrderId { get; set; } = null!;

		/// <summary>
		/// Inputs
		/// </summary>
		public List<MaterialAndAmount> Inputs { get; set; } = new();
		

		/// <summary>
		/// RawOutputs
		/// </summary>
		public string RawOutputs { get; set; } = null!;

		/// <summary>
		/// Outputs
		/// </summary>
		public List<MaterialAndAmount> Outputs { get; set; } = new();

		/// <summary>
		/// Created
		/// </summary>
		public DateTime Created { get; set; }

		/// <summary>
		/// Started
		/// </summary>
		public DateTime? Started { get; set; }

		/// <summary>
		/// Completion
		/// </summary>
		public DateTime? Completion { get; set; }

		/// <summary>
		/// DurationMs
		/// </summary>
		public long? DurationMs { get; set; }

		/// <summary>
		/// LastUpdated
		/// </summary>
		public DateTime? LastUpdated { get; set; }

		/// <summary>
		/// CompletionPercentage
		/// </summary>
		public double CompletionPecentage { get; set; }

		/// <summary>
		/// Halted
		/// </summary>
		public bool Halted { get; set; }

		/// <summary>
		/// FeeCurrencyCode
		/// </summary>
		public string? FeeCurrencyCode { get; set; }

		/// <summary>
		/// Fee
		/// </summary>
		public double? Fee { get; set; }

		/// <summary>
		/// FeeCollectorId
		/// </summary>
		public string? FeeCollectorId { get; set; }

		/// <summary>
		/// FeeCollectorName
		/// </summary>
		public string? FeeCollectorName { get; set; }

		/// <summary>
		/// FeeCollectorCode
		/// </summary>
		public string? FeeCollectorCode { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public bool Recurring { get; set; }
	}
}
