﻿namespace FIOAPIWeb.Payloads
{
    /// <summary>
    /// The result from a /planet/search POST
    /// </summary>
    public class PlanetSearchResult : Planet
    {
        /// <summary>
        /// Default constructor, for Json Serialization purposes
        /// </summary>
        public PlanetSearchResult()
        { }

        /// <summary>
        /// Planet constructor
        /// </summary>
        /// <param name="planet">planet</param>
        public PlanetSearchResult(Planet planet)
        {
            var planetModelProperties = planet.GetType().GetProperties();
            var planetSearchProperties = this.GetType().GetProperties();

            foreach (var planetModelProperty in planetModelProperties)
            {
                foreach (var planetSearchProperty in planetSearchProperties)
                {
                    if (planetModelProperty.Name == planetSearchProperty.Name &&                                        // Same property
                        planetModelProperty.PropertyType == planetSearchProperty.PropertyType &&                        // Same property type
                        planetModelProperty.GetSetMethod() != null && planetModelProperty.GetGetMethod() != null &&     // Model has both Set/Get
                        planetSearchProperty.GetSetMethod() != null && planetSearchProperty.GetGetMethod() != null)     // Search has both Set/Get
                    {
                        planetSearchProperty.SetValue(this, planetModelProperty.GetValue(planet));
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// If we have job data
        /// </summary>
        public bool HasJobData { get; set; }

        /// <summary>
        /// Total pioneers
        /// </summary>
        public int? TotalPioneers { get; set; }

        /// <summary>
        /// Unemployed pioneers
        /// </summary>
        public int? OpenPioneers { get; set; }

        /// <summary>
        /// Total settlers
        /// </summary>
        public int? TotalSettlers { get; set; }

        /// <summary>
        /// Unemployed settlers
        /// </summary>
        public int? OpenSettlers { get; set; }

        /// <summary>
        /// Total technicians
        /// </summary>
        public int? TotalTechnicians { get; set; }

        /// <summary>
        /// Unemployed technicians
        /// </summary>
        public int? OpenTechnicians { get; set; }

        /// <summary>
        /// Total engineers
        /// </summary>
        public int? TotalEngineers { get; set; }

        /// <summary>
        /// Unemployed engineers
        /// </summary>
        public int? OpenEngineers { get; set; }

        /// <summary>
        /// Total scientists
        /// </summary>
        public int? TotalScientists { get; set; }

        /// <summary>
        /// Unemployed scientists
        /// </summary>
        public int? OpenScientists { get; set; }

        /// <summary>
        /// The distance results, in jump distance
        /// </summary>
        public List<int> DistanceResults { get; set; } = new List<int>();
    }
}
