﻿namespace FIOAPIWeb.Payloads
{
	/// <summary>
	/// Response representing a Permission Model
	/// </summary>
	public class PermissionResponse
	{
		/// <summary>
		/// The grantor of permissions
		/// </summary>
		public string GrantorUserName { get; set; } = "";

		/// <summary>
		/// The grantee of permissions
		/// </summary>
		public string GranteeUserName { get; set; } = "";

		/// <summary>
		/// GroupId of the permission
		/// </summary>
		/// <remarks>This is 0 if it's not a Group permission</remarks>
		public int GroupId { get; set; }

		/// <summary>
		/// The permissions object definition
		/// </summary>
		public Permissions Permissions { get; set; } = new Permissions();
	}
}
