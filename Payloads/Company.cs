﻿namespace FIOAPIWeb.Payloads
{
    /// <summary>
    /// Company
    /// </summary>
    public class Company
    {
        /// <summary>
        /// CompanyId
        /// </summary>
        public string CompanyId { get; set; } = null!;

        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; } = null!;

        /// <summary>
        /// Code
        /// </summary>
        public string Code { get; set; } = null!;

        /// <summary>
        /// Founded
        /// </summary>
        public DateTime Founded { get; set; }

        /// <summary>
        /// UserName
        /// </summary>
        public string UserName { get; set; } = null!;

        /// <summary>
        /// CountryId
        /// </summary>
        public string CountryId { get; set; } = null!;

        /// <summary>
        /// CountryCode
        /// </summary>
        public string CountryCode { get; set; } = null!;

        /// <summary>
        /// CountryName
        /// </summary>
        public string CountryName { get; set; } = null!;

        /// <summary>
        /// CorporationId
        /// </summary>
        public string? CorporationId { get; set; } = null!;

        /// <summary>
        /// CorporationName
        /// </summary>
        public string? CorporationName { get; set; } = null!;

        /// <summary>
        /// CorporationCode
        /// </summary>
        public string? CorporationCode { get; set; } = null!;

        /// <summary>
        /// CurrentAPEXRepresentationLevel
        /// </summary>
        public int CurrentAPEXRepresentationLevel { get; set; }

        /// <summary>
        /// OverallRating
        /// </summary>
        public string OverallRating { get; set; } = null!;

        /// <summary>
        /// RatingsEarliestContract
        /// </summary>
        public DateTime? RatingsEarliestContract { get; set; }

        /// <summary>
        /// RatingContractCount
        /// </summary>
        public int RatingContractCount { get; set; }

        /// <summary>
        /// ReputationEntityName
        /// </summary>
        public string? ReputationEntityName { get; set; } = null!;

        /// <summary>
        /// Reputation
        /// </summary>
        public int Reputation { get; set; }

        /// <summary>
        /// Planets
        /// </summary>
        public virtual List<CompanyPlanet> Planets { get; set; } = new();

        /// <summary>
        /// Offices
        /// </summary>
        public virtual List<CompanyOffice> Offices { get; set; } = new();

        /// <summary>
        /// StartingProfile
        /// </summary>
        public string? StartingProfile { get; set; } = null;

        /// <summary>
        /// StartingLocationId
        /// </summary>
        public string? StartingLocationId { get; set; }

        /// <summary>
        /// StartingLocationNaturalId
        /// </summary>
        public string? StartingLocationNaturalId { get; set; }

        /// <summary>
        /// StartingLocationName
        /// </summary>
        public string? StartingLocationName { get; set; }

        /// <summary>
        /// HeadquartersLocationId
        /// </summary>
        public string? HeadquartersLocationId { get; set; }

        /// <summary>
        /// HeadquartersLocationNaturalId
        /// </summary>
        public string? HeadquartersLocationNaturalId { get; set; }

        /// <summary>
        /// HeadquartersLocationName
        /// </summary>
        public string? HeadquartersLocationName { get; set; }

        /// <summary>
        /// HeadquartersLevel
        /// </summary>
        public int? HeadquartersLevel { get; set; }

        /// <summary>
        /// TotalBasePermits
        /// </summary>
        public int? TotalBasePermits { get; set; }

        /// <summary>
        /// UsedBasePermits
        /// </summary>
        public int? UsedBasePermits { get; set; }

        /// <summary>
        /// AdditionalBasePermits
        /// </summary>
        public int? AdditionalBasePermits { get; set; }

        /// <summary>
        /// AdditionalProductionQueueSlots
        /// </summary>
        public int? AdditionalProductionQueueSlots { get; set; }

        /// <summary>
        /// HeadquartersNextRelocationTime
        /// </summary>
        public DateTime? HeadquartersNextRelocationTime { get; set; }

        /// <summary>
        /// HeadquartersRelocationLocked
        /// </summary>
        public bool? HeadquartersRelocationLocked { get; set; }

        /// <summary>
        /// HeadquartersAgricultureEfficiencyGain
        /// </summary>
        public double? HeadquartersAgricultureEfficiencyGain { get; set; }

        /// <summary>
        /// HeadquartersChemistryEfficiencyGain
        /// </summary>
        public double? HeadquartersChemistryEfficiencyGain { get; set; }

        /// <summary>
        /// HeadquartersConstructionEfficiencyGain
        /// </summary>
        public double? HeadquartersConstructionEfficiencyGain { get; set; }

        /// <summary>
        /// HeadquartersElectronicsEfficiencyGain
        /// </summary>
        public double? HeadquartersElectronicsEfficiencyGain { get; set; }

        /// <summary>
        /// HeadquartersFoodIndustriesEfficiencyGain
        /// </summary>
        public double? HeadquartersFoodIndustriesEfficiencyGain { get; set; }

        /// <summary>
        /// HeadquartersFuelRefiningEfficiencyGain
        /// </summary>
        public double? HeadquartersFuelRefiningEfficiencyGain { get; set; }

        /// <summary>
        /// HeadquartersManufacturingEfficiencyGain
        /// </summary>
        public double? HeadquartersManufacturingEfficiencyGain { get; set; }

        /// <summary>
        /// HeadquartersMetallurgyEfficiencyGain
        /// </summary>
        public double? HeadquartersMetallurgyEfficiencyGain { get; set; }

        /// <summary>
        /// HeadquartersResourceExtractionEfficiencyGain
        /// </summary>
        public double? HeadquartersResourceExtractionEfficiencyGain { get; set; }

        /// <summary>
        /// AICLiquid
        /// </summary>
        public double? AICLiquid { get; set; }

        /// <summary>
        /// CISLiquid
        /// </summary>
        public double? CISLiquid { get; set; }

        /// <summary>
        /// ECDLiquid
        /// </summary>
        public double? ECDLiquid { get; set; }

        /// <summary>
        /// ICALiquid
        /// </summary>
        public double? ICALiquid { get; set; }

        /// <summary>
        /// NCCLiquid
        /// </summary>
        public double? NCCLiquid { get; set; }

        /// <summary>
        /// RepresentationCurrentLevel
        /// </summary>
        public int? RepresentationCurrentLevel { get; set; }

        /// <summary>
        /// RepresentationCostNextLevel
        /// </summary>
        public double? RepresentationCostNextLevel { get; set; }

        /// <summary>
        /// RepresentationContributedNextLevel
        /// </summary>
        public double? RepresentationContributedNextLevel { get; set; }

        /// <summary>
        /// RepresentationLeftNextLevel
        /// </summary>
        public double? RepresentationLeftNextLevel { get; set; }

        /// <summary>
        /// RepresentationContributedTotal
        /// </summary>
        public double? RepresentationContributedTotal { get; set; }

        /// <summary>
        /// Timestamp
        /// </summary>
        public DateTime Timestamp { get; set; }
    }
}
