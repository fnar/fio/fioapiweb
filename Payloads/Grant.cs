﻿namespace FIOAPIWeb.Payloads
{
    /// <summary>
    /// The payload for Granting permissions
    /// </summary>
    public class Grant
    {
        /// <summary>
        /// The username (or * for all users) to grant permission to
        /// </summary>
        public string UserName { get; set; } = "";

        /// <summary>
        /// The permissions to grant to the username specified
        /// </summary>
        public Permissions Permissions { get; set; } = new Permissions();

    }
}
