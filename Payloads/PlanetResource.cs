﻿namespace FIOAPIWeb.Payloads
{
    /// <summary>
    /// PlanetResource
    /// </summary>
    public class PlanetResource : ICloneable
    {
        /// <summary>
        /// MaterialId
        /// </summary>
        public string MaterialId { get; set; } = null!;

        /// <summary>
        /// Type (resource type)
        /// </summary>
        public string Type { get; set; } = null!;

        /// <summary>
        /// Resource concentration
        /// </summary>
        public double Concentration { get; set; }

        /// <summary>
        /// Clone
        /// </summary>
        /// <returns>A copy</returns>
        public object Clone()
        {
            return new PlanetResource()
            {
                MaterialId = MaterialId,
                Type = Type,
                Concentration = Concentration,
            };
        }
    }
}
