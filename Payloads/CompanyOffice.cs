﻿namespace FIOAPIWeb.Payloads
{
    /// <summary>
    /// CompanyOffice
    /// </summary>
    public class CompanyOffice
    {
        /// <summary>
        /// CompanyOfficeId: {CompanyId}-{StartEpochMs}
        /// </summary>
        public string CompanyOfficeId { get; set; } = null!;

        /// <summary>
        /// Type
        /// </summary>
        public string Type { get; set; } = null!;

        /// <summary>
        /// Start
        /// </summary>
        public DateTime Start { get; set; }

        /// <summary>
        /// End
        /// </summary>
        public DateTime End { get; set; }

        /// <summary>
        /// PlanetId
        /// </summary>
        public string PlanetId { get; set; } = null!;

        /// <summary>
        /// PlanetName
        /// </summary>
        public string? PlanetName { get; set; } = null!;

        /// <summary>
        /// PlanetNaturalId
        /// </summary>
        public string PlanetNaturalId { get; set; } = null!;
    }
}
