﻿namespace FIOAPIWeb.Payloads
{
    /// <summary>
    /// Invite payload
    /// </summary>
    public class Invite
    {
        /// <summary>
        /// GroupId
        /// </summary>
        public int GroupId { get; set; }

        /// <summary>
        /// Invites
        /// </summary>
        public List<UserInvite> Invites { get; set; } = new List<UserInvite>();
    }
}
