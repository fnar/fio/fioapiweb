﻿using System.ComponentModel;

namespace FIOAPIWeb.Payloads
{
    /// <summary>
    /// Ship Permissions
    /// </summary>
    public class ShipPermissions
    {
        /// <summary>
        /// Information such as Ship name, registration, specs (acceleration, mass, etc)
        /// </summary>
        [DefaultValue(true)]
        public bool Information { get; set; } = true;

        /// <summary>
        /// Information such as ship condition, last repair, and materials needed to repair
        /// </summary>
        [DefaultValue(true)]
        public bool Repair { get; set; } = true;

        /// <summary>
        /// Ship flight information
        /// </summary>
        [DefaultValue(true)]
        public bool Flight { get; set; } = true;

        /// <summary>
        /// Ship Inventory
        /// </summary>
        [DefaultValue(true)]
        public bool Inventory { get; set; } = true;

        /// <summary>
        /// Ship fuel inventory
        /// </summary>
        [DefaultValue(true)]
        public bool FuelInventory { get; set; } = true;

        public ShipPermissions() { }

        public ShipPermissions(ShipPermissions other)
        {
            Information = other.Information;
            Repair = other.Repair;
            Flight = other.Flight;
            Inventory = other.Inventory;
            FuelInventory = other.FuelInventory;
        }
    }

    /// <summary>
    /// Site Permissions
    /// </summary>
    public class SitePermissions
    {
        /// <summary>
        /// Where your sites are located
        /// </summary>
        [DefaultValue(true)]
        public bool Location { get; set; } = true;

        /// <summary>
        /// Workforce data (worker counts and capacity)
        /// </summary>
        [DefaultValue(true)]
        public bool Workforces { get; set; } = true;

        /// <summary>
        /// Experts
        /// </summary>
        [DefaultValue(true)]
        public bool Experts { get; set; } = true;

        /// <summary>
        /// The general building information (effectively, building ticker)
        /// </summary>
        [DefaultValue(true)]
        public bool Buildings { get; set; } = true;

        /// <summary>
        /// Repair costs
        /// </summary>
        [DefaultValue(true)]
        public bool Repair { get; set; } = true;

        /// <summary>
        /// Reclaimables
        /// </summary>
        [DefaultValue(true)]
        public bool Reclaimable { get; set; } = true;

        /// <summary>
        /// Production line information. This includes all active and pending orders
        /// </summary>
        [DefaultValue(true)]
        public bool ProductionLines { get; set; } = true;

        public SitePermissions() { }

        public SitePermissions(SitePermissions other)
        {
            Location = other.Location;
            Workforces = other.Workforces;
            Experts = other.Experts;
            Buildings = other.Buildings;
            Repair = other.Repair;
            Reclaimable = other.Reclaimable;
            ProductionLines = other.ProductionLines;
        }
    }

    /// <summary>
    /// Storage permissions
    /// </summary>
    public class StoragePermissions
    {
        /// <summary>
        /// The location of all your storage (inventory and warehouse)
        /// </summary>
        [DefaultValue(true)]
        public bool Location { get; set; } = true;

        /// <summary>
        /// Information about the storage (Weight/Volume capacity, warehouse or base storage, etc)
        /// </summary>
        [DefaultValue(true)]
        public bool Information { get; set; } = true;

        /// <summary>
        /// Inventory items. This also implies actual weight/volume permissions
        /// </summary>
        [DefaultValue(true)]
        public bool Items { get; set; } = true;

        public StoragePermissions() { }

        public StoragePermissions(StoragePermissions other)
        {
            Location = other.Location;
            Information = other.Information;
            Items = other.Items;
        }
    }

    /// <summary>
    /// Trade permissions
    /// </summary>
    public class TradePermissions
    {
        /// <summary>
        /// Contract information
        /// </summary>
        [DefaultValue(true)]
        public bool Contract { get; set; } = true;

        /// <summary>
        /// CXOS information
        /// </summary>
        [DefaultValue(true)]
        public bool CXOS { get; set; } = true;

        public TradePermissions() { }

        public TradePermissions(TradePermissions other)
        {
            Contract = other.Contract;
            CXOS = other.CXOS;
        }
    }

    /// <summary>
    /// Misc permissions
    /// </summary>
    public class MiscPermissions
    {
        /// <summary>
        /// If shipment tracking is allowed
        /// </summary>
        [DefaultValue(true)]
        public bool ShipmentTracking { get; set; } = true;

        public MiscPermissions() { }

        public MiscPermissions(MiscPermissions other)
        {
            ShipmentTracking = other.ShipmentTracking;
        }
    }

    /// <summary>
    /// Company permissions
    /// </summary>
    public class CompanyPermissions
    {
        /// <summary>
        /// Info
        /// </summary>
        [DefaultValue(true)]
        public bool Info { get; set; } = true;

        /// <summary>
        /// LiquidCurrency
        /// </summary>
        [DefaultValue(true)]
        public bool LiquidCurrency { get; set; } = true;

        /// <summary>
        /// Headquarters
        /// </summary>
        [DefaultValue(true)]
        public bool Headquarters { get; set; } = true;

        public CompanyPermissions() { }

        public CompanyPermissions(CompanyPermissions other)
        {
            Info = other.Info;
            LiquidCurrency = other.LiquidCurrency;
            Headquarters = other.Headquarters;
        }
    }

    /// <summary>
    /// Permissions
    /// </summary>
    public class Permissions
    {
        /// <summary>
        /// ShipPermissions
        /// </summary>
        public ShipPermissions ShipPermissions { get; set; } = new();

        /// <summary>
        /// SitePermissions
        /// </summary>
        public SitePermissions SitesPermissions { get; set; } = new();

        /// <summary>
        /// StoragePermissions
        /// </summary>
        public StoragePermissions StoragePermissions { get; set; } = new();

        /// <summary>
        /// TradePermissions
        /// </summary>
        public TradePermissions TradePermissions { get; set; } = new();

        /// <summary>
        /// CompanyPermissions
        /// </summary>
        public CompanyPermissions CompanyPermissions { get; set; } = new();

        /// <summary>
        /// MiscPermissions
        /// </summary>
        public MiscPermissions MiscPermissions { get; set; } = new();

        public Permissions() { }

        public Permissions(Permissions initPerms)
        {
            ShipPermissions = new ShipPermissions(initPerms.ShipPermissions);
            SitesPermissions = new SitePermissions(initPerms.SitesPermissions);
            StoragePermissions = new StoragePermissions(initPerms.StoragePermissions);
            TradePermissions = new TradePermissions(initPerms.TradePermissions);
            CompanyPermissions = new CompanyPermissions(initPerms.CompanyPermissions);
            MiscPermissions = new MiscPermissions(initPerms.MiscPermissions);
        }
    }
}
