﻿using System.Text.Json.Serialization;

namespace FIOAPIWeb.Payloads
{
    /// <summary>
    /// PlanetPopulationProject
    /// </summary>
    public class PlanetPopulationProject : ICloneable
    {
        /// <summary>
        /// PlanetPopulationProjectId
        /// </summary>
        public string PlanetPopulationProjectId { get; set; } = null!;

        /// <summary>
        /// Type
        /// </summary>
        public string Type { get; set; } = null!;

        /// <summary>
        /// ProjectIdentifier
        /// </summary>
        public string ProjectIdentifier { get; set; } = null!;

        /// <summary>
        /// Level
        /// </summary>
        public int Level { get; set; }

        /// <summary>
        /// ActiveLevel
        /// </summary>
        public int ActiveLevel { get; set; }

        /// <summary>
        /// CurrentLevel
        /// </summary>
        public int CurrentLevel { get; set; }

        /// <summary>
        /// UpgradeStatus
        /// </summary>
        public double UpgradeStatus { get; set; }

        /// <summary>
        /// UpgradeCosts
        /// </summary>
        [JsonIgnore]
        public List<PlanetaryProjectUpgradeCost> UpgradeCosts { get; set; } = null!;

        /// <summary>
        /// Upkeeps
        /// </summary>
        public virtual List<PlanetPopulationProjectUpkeep> Upkeeps { get; set; } = new();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public object Clone()
        {
            var copy = new PlanetPopulationProject()
            {
                PlanetPopulationProjectId = PlanetPopulationProjectId,
                Type = Type,
                ProjectIdentifier = ProjectIdentifier,
                Level = Level,
                ActiveLevel = ActiveLevel,
                CurrentLevel = CurrentLevel,
                UpgradeStatus = UpgradeStatus,
                UpgradeCosts = UpgradeCosts,
            };

            Upkeeps.ForEach(u =>
            {
                var uCopy = (PlanetPopulationProjectUpkeep)u.Clone();
                copy.Upkeeps.Add(uCopy);
            });

            return copy;
        }
    }

    /// <summary>
    /// PlanetaryProjectUpgradeCost
    /// </summary>
    public class PlanetaryProjectUpgradeCost
    {
        /// <summary>
        /// Ticker
        /// </summary>
        public string Ticker { get; set; } = null!;

        /// <summary>
        /// Amount
        /// </summary>
        public int Amount { get; set; }

        /// <summary>
        /// CurrentAmount
        /// </summary>
        public int CurrentAmount { get; set; }
    }
}
