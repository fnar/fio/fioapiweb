﻿using System.ComponentModel.DataAnnotations;

namespace FIOAPIWeb.Payloads
{
    /// <summary>
    /// The ChangePassword Payload
    /// </summary>
    public class ChangePassword
    {
        /// <summary>
        /// Old password
        /// </summary>
        /// <example>Hunter2</example>
        [StringLength(256, MinimumLength = 3)]
        public string OldPassword { get; set; } = "";

        /// <summary>
        /// New password
        /// </summary>
        /// <example>Hunter4</example>
        [StringLength(256, MinimumLength = 3)]
        public string NewPassword { get; set; } = "";
    }
}
