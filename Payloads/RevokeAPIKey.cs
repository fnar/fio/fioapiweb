﻿namespace FIOAPIWeb.Payloads
{
    /// <summary>
    /// RevokeAPIKey Payload
    /// </summary>
    public class RevokeAPIKey
    {
        /// <summary>
        /// Username
        /// </summary>
        /// <example>Saganaki</example>
        public string UserName { get; set; } = "";

        /// <summary>
        /// Password
        /// </summary>
        /// <example>Hunter2</example>
        public string Password { get; set; } = "";

        /// <summary>
        /// APIKeyToRevoke
        /// </summary>
        /// <Example>a2da80a60f2c44ac84bd2abbe3d39936</Example>
        public Guid APIKeyToRevoke { get; set; }
    }
}
