﻿namespace FIOAPIWeb.Payloads
{
	public class MaterialTickerAndAmount
	{
		public string Ticker { get; set; } = null!;

		public int Amount { get; set; }
	}

	/// <summary>
	/// WorkforceLevelAndEfficiency
	/// </summary>
	public class WorkforceLevelAndEfficiency
	{
		/// <summary>
		/// Level
		/// </summary>
		public string Level { get; set; } = null!;

		/// <summary>
		/// Efficiency
		/// </summary>
		public double Efficiency { get; set; }
	}

	/// <summary>
	/// EfficiencyFactor
	/// </summary>
	public class EfficiencyFactor
	{
		/// <summary>
		/// ExpertiseCategory
		/// </summary>
		public string? ExpertiseCategory { get; set; }

		/// <summary>
		/// Type
		/// </summary>
		public string Type { get; set; } = null!;

		/// <summary>
		/// Effectivity
		/// </summary>
		public double Effectivity { get; set; }

		/// <summary>
		/// Value
		/// </summary>
		public double Value { get; set; }
	}

	/// <summary>
	/// MaterialAndAmount
	/// </summary>
	public class MaterialAndAmount
	{
		/// <summary>
		/// Ticker
		/// </summary>
		public string Ticker { get; set; } = null!;

		/// <summary>
		/// Amount
		/// </summary>
		public int Amount { get; set; }
	}
}
