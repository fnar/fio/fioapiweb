﻿namespace FIOAPIWeb.Payloads
{
	/// <summary>
	/// An individual response
	/// </summary>
	/// <typeparam name="T">Data type</typeparam>
	public class IndividualUserResponse<T> where T : notnull, new()
	{
		/// <summary>
		/// Any errors with the associated username
		/// </summary>
		public List<string> Errors { get; set; } = new();

		/// <summary>
		/// The data
		/// </summary>
		public T Data { get; set; } = new();
	}

	/// <summary>
	/// A grouped response
	/// </summary>
	/// <typeparam name="T">Data type</typeparam>
	public class RetrievalResponse<T> where T : notnull, new()
	{
		/// <summary>
		/// All responses
		/// </summary>
		public Dictionary<string, IndividualUserResponse<T>> UserNameToData { get; set; } = new();
	}

}
