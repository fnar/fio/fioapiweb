﻿namespace FIOAPIWeb.Payloads
{
	/// <summary>
	/// SiteBuilding
	/// </summary>
	public class SiteBuilding
	{
		/// <summary>
		/// SiteBuildingId (PlatformId)
		/// </summary>
		public string SiteBuildingId { get; set; } = null!;

		/// <summary>
		/// Area
		/// </summary>
		public int Area { get; set; }

		/// <summary>
		/// CreationTime
		/// </summary>
		public DateTime CreationTime { get; set; }

		/// <summary>
		/// BuildingName
		/// </summary>
		public string BuildingName { get; set; } = null!;

		/// <summary>
		/// BuildingTicker
		/// </summary>
		public string BuildingTicker { get; set; } = null!;

		/// <summary>
		/// BuildingType
		/// </summary>
		public string BuildingType { get; set; } = null!;

		/// <summary>
		/// BookValueCurrencyCode
		/// </summary>
		public string BookValueCurrencyCode { get; set; } = null!;

		/// <summary>
		/// BookValueAmount
		/// </summary>
		public double BookValueAmount { get; set; }

		/// <summary>
		/// Condition
		/// </summary>
		public double Condition { get; set; }

		/// <summary>
		/// LastRepair
		/// </summary>
		public DateTime? LastRepair { get; set; }

		/// <summary>
		/// ReclaimableMaterials
		/// </summary>
		public List<MaterialTickerAndAmount> ReclaimableMaterials { get; set; } = new();

		/// <summary>
		/// SiteId
		/// </summary>
		public string SiteId { get; set; } = null!;
	}
}
