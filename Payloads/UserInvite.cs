﻿namespace FIOAPIWeb.Payloads
{
    /// <summary>
    /// UserInvite
    /// </summary>
    public class UserInvite
    {
        /// <summary>
        /// The username of the person being invited
        /// </summary>
        public string UserName { get; set; } = "";

        /// <summary>
        /// If this is an invite for an admin role
        /// </summary>
        public bool Admin { get; set; } = false;
    }
}
