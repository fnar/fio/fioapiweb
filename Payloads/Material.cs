﻿namespace FIOAPIWeb.Payloads
{
    public class Material : ICloneable
    {
        /// <summary>
        /// The APEX MaterialId
        /// </summary>
        public string MaterialId { get; set; } = null!;

        /// <summary>
        /// Name of the material
        /// </summary>
        public string Name { get; set; } = null!;

        /// <summary>
        /// The material's ticker
        /// </summary>
        public string Ticker { get; set; } = null!;

        /// <summary>
        /// The material's category id
        /// </summary>
        public string MaterialCategoryId { get; set; } = null!;

        /// <summary>
        /// Weight of the material in tons
        /// </summary>
        public double Weight { get; set; }

        /// <summary>
        /// Volume of the material in m^3
        /// </summary>
        public double Volume { get; set; }

        /// <summary>
        /// If it's a resource extractable from a planet
        /// </summary>
        public bool IsResource { get; set; }

        /// <summary>
        /// The resource type of the material
        /// </summary>
        public string? ResourceType { get; set; } = null;

        /// <summary>
        /// If this material is used in infrastructure
        /// </summary>
        public bool InfrastructureUsage { get; set; }

        /// <summary>
        /// If this material is used in COGC
        /// </summary>
        public bool COGCUsage { get; set; }

        /// <summary>
        /// If this material is used for workforces
        /// </summary>
        public bool WorkforceUsage { get; set; }

        /// <summary>
        /// The username that submitted the data
        /// </summary>
        public string UserNameSubmitted { get; set; } = null!;

        /// <summary>
        /// The timestamp of the data
        /// </summary>
        public DateTime Timestamp { get; set; }

		public object Clone()
		{
            return new Material()
            {
                MaterialId = MaterialId,
                Name = Name,
                Ticker = Ticker,
                MaterialCategoryId = MaterialCategoryId,
                Weight = Weight,
                Volume = Volume,
                IsResource = IsResource,
                ResourceType = ResourceType,
                InfrastructureUsage = InfrastructureUsage,
                COGCUsage = COGCUsage,
                WorkforceUsage = WorkforceUsage,
                UserNameSubmitted = UserNameSubmitted,
                Timestamp = Timestamp,
            };
		}
	}
}
