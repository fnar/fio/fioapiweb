﻿namespace FIOAPIWeb.Payloads
{
    /// <summary>
    /// The payload search payload to POST
    /// </summary>
    public class PlanetSearch
    {
        /// <summary>
        /// The material tickers to search for
        /// </summary>
        public List<string> Materials { get; set; } = new();

        /// <summary>
        /// Omit any material results with a concentration less than this value
        /// </summary>
        public double ConcentrationThreshold { get; set; } = 0.0;

        /// <summary>
        /// Include rocky planets
        /// </summary>
        public bool IncludeRocky { get; set; }

        /// <summary>
        /// Include gaseous planets
        /// </summary>
        public bool IncludeGaseous { get; set; }

        /// <summary>
        /// Include low gravity planets
        /// </summary>
        public bool IncludeLowGravity { get; set; }

        /// <summary>
        /// Include high gravity planets
        /// </summary>
        public bool IncludeHighGravity { get; set; }

        /// <summary>
        /// Include low pressure planets
        /// </summary>
        public bool IncludeLowPressure { get; set; }

        /// <summary>
        /// Include high pressure planets
        /// </summary>
        public bool IncludeHighPressure { get; set; }

        /// <summary>
        /// Include low temperature planets
        /// </summary>
        public bool IncludeLowTemperature { get; set; }

        /// <summary>
        /// Include high temperature planets
        /// </summary>
        public bool IncludeHighTemperature { get; set; }

        /// <summary>
        /// The planet must be fertile
        /// </summary>
        public bool MustBeFertile { get; set; }

        /// <summary>
        /// The planet must have a local market
        /// </summary>
        public bool MustHaveLocalMarket { get; set; }

        /// <summary>
        /// The planet must have a chamber of global commerce
        /// </summary>
        public bool MustHaveChamberOfCommerce { get; set; }

        /// <summary>
        /// The planet must have a warehouse
        /// </summary>
        public bool MustHaveWarehouse { get; set; }

        /// <summary>
        /// The planet must have an administration center
        /// </summary>
        public bool MustHaveAdministrationCenter { get; set; }

        /// <summary>
        /// The planet must have a shipyard
        /// </summary>
        public bool MustHaveShipyard { get; set; }

        /// <summary>
        /// Where to do distance checks from (maximum 10)
        /// </summary>
        public List<string> DistanceChecks { get; set; } = new List<string>();

        /// <summary>
        /// The jump distance threshold from the source below
        /// </summary>
        public int JumpDistanceThreshold { get; set; } = -1;

        /// <summary>
        /// What source to threshold from. Can be SystemID, System Name, Planet Name, Planet NaturalId, Planet Id
        /// </summary>
        public string? JumpDistanceThresholdSource { get; set; }
    }
}
