﻿namespace FIOAPIWeb.Payloads
{
	/// <summary>
	/// Storage
	/// </summary>
	public class Storage
	{
		/// <summary>
		/// StorageId
		/// </summary>
		public string StorageId { get; set; } = null!;

		/// <summary>
		/// AddressableId
		/// </summary>
		public string AddressableId { get; set; } = null!;

		/// <summary>
		/// SiteId
		/// </summary>
		public string SiteId { get;set; } = null!;

		/// <summary>
		/// Name
		/// </summary>
		public string? Name { get; set; }

		/// <summary>
		/// LocationId
		/// </summary>
		public string? LocationId { get; set; }

		/// <summary>
		/// LocationNaturalId
		/// </summary>
		public string? LocationNaturalId { get; set; }

		/// <summary>
		/// LocationName
		/// </summary>
		public string? LocationName { get; set; }

		/// <summary>
		/// WeightLoad
		/// </summary>
		public double WeightLoad { get; set; }

		/// <summary>
		/// WeightCapacity
		/// </summary>
		public double WeightCapacity { get; set; }

		/// <summary>
		/// VolumeLoad
		/// </summary>
		public double VolumeLoad { get; set; }

		/// <summary>
		/// VolumeCapacity
		/// </summary>
		public double VolumeCapacity { get; set; }

		/// <summary>
		/// Fixed - If it's a fixed/stationary store
		/// </summary>
		public bool Fixed { get; set; }

		/// <summary>
		/// TradeStore - If you can trade from it. Only fuel tanks cannot.
		/// </summary>
		public bool TradeStore { get; set; }

		/// <summary>
		/// Rank - Usage not yet known
		/// </summary>
		public int Rank { get; set; }

		/// <summary>
		/// Locked - If the store is locked (warehouse not payed)
		/// </summary>
		public bool Locked { get; set; }

		/// <summary>
		/// Type - STORE, WAREHOUSE_STORE, STL_FUEL_STORE, FTL_FUEL_STORE
		/// </summary>
		public string Type { get; set; } = null!;

		/// <summary>
		/// StorageItems
		/// </summary>
		public virtual List<StorageItem> StorageItems { get; set; } = new();

		/// <summary>
		/// The timestamp of the data
		/// </summary>
		public DateTime Timestamp { get; set; }
	}
}
