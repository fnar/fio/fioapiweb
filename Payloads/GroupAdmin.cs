﻿using Microsoft.VisualBasic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace FIOAPIWeb.Payloads
{
    /// <summary>
    /// GroupAdmin model
    /// </summary>
    public class GroupAdmin 
    {
        /// <summary>
        /// UserName
        /// </summary>
        public string UserName { get; set; } = null!;
    }
}
