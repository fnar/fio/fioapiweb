﻿namespace FIOAPIWeb.Payloads
{
    /// <summary>
    /// PlanetCOGCHistory
    /// </summary>
    public class PlanetCOGCProgram : ICloneable
    {
        /// <summary>
        /// Type
        /// </summary>
        public string Type { get; set; } = null!;

        /// <summary>
        /// StartTime
        /// </summary>
        public DateTime StartTime { get; set; }

        /// <summary>
        /// EndTime
        /// </summary>
        public DateTime EndTime { get; set; }

        /// <summary>
        /// Clone
        /// </summary>
        /// <returns>A copy</returns>
        public object Clone()
        {
            return new PlanetCOGCProgram()
            {
                Type = Type,
                StartTime = StartTime,
                EndTime = EndTime,
            };
        }
    }
}
