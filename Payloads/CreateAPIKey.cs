﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace FIOAPIWeb.Payloads
{
    /// <summary>
    /// Structure for creating an APIKey
    /// </summary>
    public class CreateAPIKey
    {
        /// <summary>
        /// Your username
        /// </summary>
        /// <example>Saganaki</example>
        [StringLength(32, MinimumLength = 3)]
        public string UserName { get; set; } = "";

        /// <summary>
        /// Your password
        /// </summary>
        /// <example>Hunter2</example>
        [StringLength(256, MinimumLength = 3)]
        public string Password { get; set; } = "";

        /// <summary>
        /// The application name for the APIkey
        /// </summary>
        /// <example>My Spreadsheet</example>
        [StringLength(128)]
        public string ApplicationName { get; set; } = "";

        /// <summary>
        /// If this APIKey will allow writes
        /// </summary>
        /// <example>false</example>
        [DefaultValue(false)]
        public bool AllowWrites { get; set; } = false;
    }
}
