﻿
namespace FIOAPIWeb.Payloads
{
	/// <summary>
	/// CXOS
	/// </summary>
	public class CXOS
	{
		/// <summary>
		/// OrderId
		/// </summary>
		public string OrderId { get; set; } = null!;

		/// <summary>
		/// ExchangeCode
		/// </summary>
		public string ExchangeCode { get; set; } = null!;

		/// <summary>
		/// BrokerId
		/// </summary>
		public string BrokerId { get; set; } = null!;

		/// <summary>
		/// OrderType
		/// </summary>
		public string OrderType { get; set; } = null!;

		/// <summary>
		/// MaterialTicker
		/// </summary>
		public string MaterialTicker { get; set; } = null!;

		/// <summary>
		/// Amount
		/// </summary>
		public int Amount { get; set; }

		/// <summary>
		/// InitialAmount
		/// </summary>
		public int InitialAmount { get; set; }

		/// <summary>
		/// LimitAmount
		/// </summary>
		public double LimitAmount { get; set; }

		/// <summary>
		/// LimitCurrency
		/// </summary>
		public string LimitCurrency { get; set; } = null!;

		/// <summary>
		/// Status
		/// </summary>
		public string Status { get; set; } = null!;

		/// <summary>
		/// Created
		/// </summary>
		public DateTime Created { get; set; }

		/// <summary>
		/// Timestamp
		/// </summary>
		public DateTime Timestamp { get; set; }

		/// <summary>
		/// Trades
		/// </summary>
		public virtual List<CXOSTrade> Trades { get; set; } = new();
	}
}
