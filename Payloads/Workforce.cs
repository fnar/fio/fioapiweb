﻿namespace FIOAPIWeb.Payloads
{
	/// <summary>
	/// Workforce
	/// </summary>
	public class Workforce
	{
		/// <summary>
		/// WorkforceId
		/// </summary>
		public string WorkforceId { get; set; } = null!;

		/// <summary>
		/// SiteId
		/// </summary>
		public string SiteId { get; set;} = null!;

		/// <summary>
		/// PioneerPopulation
		/// </summary>
		public int PioneerPopulation { get; set; }

		/// <summary>
		/// PioneerReserve
		/// </summary>
		public int PioneerReserve { get; set; }

		/// <summary>
		/// PioneerCapacity
		/// </summary>
		public int PioneerCapacity { get; set; }

		/// <summary>
		/// PioneerRequired
		/// </summary>
		public int PioneerRequired { get; set; }

		/// <summary>
		/// PioneerSatisfaction
		/// </summary>
		public double PioneerSatisfaction { get; set; }

		/// <summary>
		/// SettlerPopulation
		/// </summary>
		public int SettlerPopulation { get; set; }

		/// <summary>
		/// SettlerReserve
		/// </summary>
		public int SettlerReserve { get; set; }

		/// <summary>
		/// SettlerCapacity
		/// </summary>
		public int SettlerCapacity { get; set; }

		/// <summary>
		/// SettlerRequired
		/// </summary>
		public int SettlerRequired { get; set; }

		/// <summary>
		/// SettlerSatisfaction
		/// </summary>
		public double SettlerSatisfaction { get; set; }

		/// <summary>
		/// TechnicianPopulation
		/// </summary>
		public int TechnicianPopulation { get; set; }

		/// <summary>
		/// TechnicianReserve
		/// </summary>
		public int TechnicianReserve { get; set; }

		/// <summary>
		/// TechnicianCapacity
		/// </summary>
		public int TechnicianCapacity { get; set; }

		/// <summary>
		/// TechnicianRequired
		/// </summary>
		public int TechnicianRequired { get; set; }

		/// <summary>
		/// TechnicianSatisfaction
		/// </summary>
		public double TechnicianSatisfaction { get; set; }

		/// <summary>
		/// EngineerPopulation
		/// </summary>
		public int EngineerPopulation { get; set; }

		/// <summary>
		/// EngineerReserve
		/// </summary>
		public int EngineerReserve { get; set; }

		/// <summary>
		/// EngineerCapacity
		/// </summary>
		public int EngineerCapacity { get; set; }

		/// <summary>
		/// EngineerRequired
		/// </summary>
		public int EngineerRequired { get; set; }

		/// <summary>
		/// EngineerSatisfaction
		/// </summary>
		public double EngineerSatisfaction { get; set; }

		/// <summary>
		/// ScientistPopulation
		/// </summary>
		public int ScientistPopulation { get; set; }

		/// <summary>
		/// ScientistReserve
		/// </summary>
		public int ScientistReserve { get; set; }

		/// <summary>
		/// ScientistCapacity
		/// </summary>
		public int ScientistCapacity { get; set; }

		/// <summary>
		/// ScientistRequired
		/// </summary>
		public int ScientistRequired { get; set; }

		/// <summary>
		/// ScientistSatisfaction
		/// </summary>
		public double ScientistSatisfaction { get; set; }

		/// <summary>
		/// LastTickTime - When consumables were last pulled from this site
		/// </summary>
		public DateTime? LastTickTime { get; set; }

		/// <summary>
		/// Timestamp
		/// </summary>
		public DateTime Timestamp { get; set; }

		/// <summary>
		/// WorkforceNeeds
		/// </summary>
		public virtual List<WorkforceNeed> WorkforceNeeds { get; set; } = new List<WorkforceNeed>();
	}
}
