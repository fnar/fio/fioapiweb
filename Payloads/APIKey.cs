﻿using Microsoft.VisualBasic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Text.Json.Serialization;

namespace FIOAPIWeb.Payloads
{
	/// <summary>
	/// APIKey model
	/// </summary>
	public class APIKey
	{
		/// <summary>
		/// The actual Key to use
		/// </summary>
		public Guid Key { get; set; }

		/// <summary>
		/// The username owner
		/// </summary>
		public string UserName { get; set; } = null!;

		/// <summary>
		/// The application name
		/// </summary>
		public string Application { get; set; } = "";

		/// <summary>
		/// If this APIKey should allow writes
		/// </summary>
		public bool AllowWrites { get; set; }

		/// <summary>
		/// The time of APIKey creation in Utc
		/// </summary>
		public DateTime CreateTime { get; set; }
	}
}
