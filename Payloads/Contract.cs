﻿using FIOAPIWeb.Extensions;
using Microsoft.VisualBasic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace FIOAPIWeb.Payloads
{
	/// <summary>
	/// Contract
	/// </summary>
	public class Contract
	{
		/// <summary>
		/// ContractId
		/// </summary>
		public string ContractId { get; set; } = null!;

		/// <summary>
		/// Date
		/// </summary>
		public DateTime Date { get; set; }

		/// <summary>
		/// Party
		/// </summary>
		public string Party { get; set; } = null!;

		/// <summary>
		/// PartnerId
		/// </summary>
		public string PartnerId { get; set; } = null!;

		/// <summary>
		/// PartnerCode
		/// </summary>
		public string? PartnerCode { get; set; }

		/// <summary>
		/// PartnerName
		/// </summary>
		public string PartnerName { get; set; } = null!;

		/// <summary>
		/// Status
		/// </summary>
		public string Status { get; set; } = null!;

		/// <summary>
		/// ExtensionDeadline
		/// </summary>
		public DateTime? ExtensionDeadline { get; set; }

		/// <summary>
		/// CanExtend
		/// </summary>
		public bool CanExtend { get; set; }

		/// <summary>
		/// CanRequestTerminiation
		/// </summary>
		public bool CanRequestTermination { get; set; }

		/// <summary>
		/// DueDate
		/// </summary>
		public DateTime? DueDate { get; set; }

		/// <summary>
		/// Name
		/// </summary>
		public string? Name { get; set; }

		/// <summary>
		/// Preamble
		/// </summary>
		public string? Preamble { get; set; }

		/// <summary>
		/// TerminationSent
		/// </summary>
		public bool TerminationSent { get; set; }

		/// <summary>
		/// TerminationReceived
		/// </summary>
		public bool TerminationReceived { get; set; }

		/// <summary>
		/// AgentContract
		/// </summary>
		public bool AgentContract { get; set; }

		/// <summary>
		/// RelatedContractsList
		/// </summary>
		public List<string> RelatedContractsList { get; set; } = new List<string>();

		/// <summary>
		/// ContractType
		/// </summary>
		public string? ContractType { get; set; }

		/// <summary>
		/// Timestamp
		/// </summary>
		public DateTime Timestamp { get; set; }

		/// <summary>
		/// Conditions
		/// </summary>
		public virtual List<ContractCondition> Conditions { get; set; } = new();
	}
}
