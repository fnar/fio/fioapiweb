﻿namespace FIOAPIWeb.Payloads
{
	/// <summary>
	/// An "AllData" payload - All available data on the user
	/// </summary>
	public class AllData
	{
		/// <summary>
		/// Company
		/// </summary>
		public Company? Company { get; set; }

		/// <summary>
		/// Contracts
		/// </summary>
		public List<Contract>? Contracts { get; set; }

		/// <summary>
		/// CXOSs
		/// </summary>
		public List<CXOS>? CXOSs { get; set; }

		/// <summary>
		/// ProductionLines
		/// </summary>
		public List<ProductionLine>? ProductionLines { get; set; }

		/// <summary>
		/// Ships
		/// </summary>
		public List<Ship>? Ships { get; set; }

		/// <summary>
		/// Sites
		/// </summary>
		public List<Site>? Sites { get; set; }

		/// <summary>
		/// Storages
		/// </summary>
		public List<Storage>? Storages { get; set; }

		/// <summary>
		/// Workforces
		/// </summary>
		public List<Workforce>? Workforces { get; set; }
	}
}
