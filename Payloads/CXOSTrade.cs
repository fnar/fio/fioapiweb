﻿

namespace FIOAPIWeb.Payloads
{
	/// <summary>
	/// CXOSTrade
	/// </summary>
	public class CXOSTrade
	{
		/// <summary>
		/// TradeId
		/// </summary>
		public string TradeId { get; set; } = null!;

		/// <summary>
		/// Amount
		/// </summary>
		public int Amount { get; set; }

		/// <summary>
		/// PriceAmount
		/// </summary>
		public double PriceAmount { get; set; }

		/// <summary>
		/// PriceCurrency
		/// </summary>
		public string PriceCurrency { get; set; } = null!;

		/// <summary>
		/// Time
		/// </summary>
		public DateTime Time { get; set; }

		/// <summary>
		/// PartnerId
		/// </summary>
		public string PartnerId { get; set; } = null!;

		/// <summary>
		/// PartnerCode
		/// </summary>
		public string? PartnerCode { get; set; }

		/// <summary>
		/// PartnerName
		/// </summary>
		public string PartnerName { get; set; } = null!;
	}
}
