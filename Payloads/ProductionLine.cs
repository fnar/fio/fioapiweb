﻿namespace FIOAPIWeb.Payloads
{
	/// </summary>
	public class ProductionLine
	{
		/// <summary>
		/// ProductionLineId
		/// </summary>
		public string ProductionLineId { get; set; } = null!;

		/// <summary>
		/// SiteId
		/// </summary>
		public string SiteId { get; set; } = null!;

		/// <summary>
		/// LocationId
		/// </summary>
		public string LocationId { get; set; } = null!;

		/// <summary>
		/// LocationNaturalId
		/// </summary>
		public string LocationNaturalId { get; set; } = null!;

		/// <summary>
		/// LocationName
		/// </summary>
		public string LocationName { get; set; } = null!;

		/// <summary>
		/// BuildingName
		/// </summary>
		public string BuildingName { get; set; } = null!;

		/// <summary>
		/// BuildingTicker
		/// </summary>
		public string? BuildingTicker { get; set; }

		/// <summary>
		/// Capacity
		/// </summary>
		public int Capacity { get; set; }

		/// <summary>
		/// Slots - Not sure what this represents
		/// </summary>
		public int Slots { get; set; }

		/// <summary>
		/// Efficiency
		/// </summary>
		public double Efficiency { get; set; }

		/// <summary>
		/// Condition
		/// </summary>
		public double Condition { get; set; }

		/// <summary>
		/// WorkforceEfficiencies
		/// </summary>
		public List<WorkforceLevelAndEfficiency> WorkforceEfficiencies { get; set; } = new List<WorkforceLevelAndEfficiency>();

		/// <summary>
		/// EfficiencyFactors
		/// </summary>
		public List<EfficiencyFactor> EfficiencyFactors { get; set; } = new List<EfficiencyFactor>();

		/// <summary>
		/// Orders
		/// </summary>
		public virtual List<ProductionLineOrder> Orders { get; set; } = new();

		/// <summary>
		/// The timestamp of the data
		/// </summary>
		public DateTime Timestamp { get; set; }
	}
}
