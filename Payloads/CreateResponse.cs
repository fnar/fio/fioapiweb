﻿using System.ComponentModel.DataAnnotations;

namespace FIOAPIWeb.Payloads
{
    /// <summary>
    /// Response to group create
    /// </summary>
    public class CreateResponse
    {
        /// <summary>
        /// The GroupId of the new group
        /// </summary>
        public int GroupId { get; set; }
    }
}
