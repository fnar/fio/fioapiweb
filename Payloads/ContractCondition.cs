﻿namespace FIOAPIWeb.Payloads
{
	/// <summary>
	/// ContractCondition
	/// </summary>
	public class ContractCondition
	{
		/// <summary>
		/// ContractConditionId
		/// </summary>
		public string ContractConditionId { get; set; } = null!;

		/// <summary>
		/// Type
		/// </summary>
		public string Type { get; set; } = null!;

		/// <summary>
		/// Index
		/// </summary>
		public int Index { get; set; }

		/// <summary>
		/// Status
		/// </summary>
		public string Status { get; set; } = null!;

		/// <summary>
		/// DependenciesList
		/// </summary>
		public List<string> DependenciesList { get; set; } = new List<string>();

		/// <summary>
		/// DeadlineDuration
		/// </summary>
		public long? DeadlineDuration { get; set; }

		/// <summary>
		/// Deadline
		/// </summary>
		public DateTime? Deadline { get; set; }

		/// <summary>
		/// QuantityMaterialTicker
		/// </summary>
		public string? QuantityMaterialTicker { get; set; }

		/// <summary>
		/// QuantityAmount
		/// </summary>
		public int? QuantityAmount { get; set; }

		/// <summary>
		/// AddressId
		/// </summary>
		public string? AddressId { get; set; }

		/// <summary>
		/// AddressNaturalId
		/// </summary>
		public string? AddressNaturalId { get; set; }

		/// <summary>
		/// AddressName
		/// </summary>
		public string? AddressName { get; set; }

		/// <summary>
		/// BlockId
		/// </summary>
		public string? BlockId { get; set; }

		/// <summary>
		/// PickedUpMaterialTicker
		/// </summary>
		public string? PickedUpMaterialTicker { get; set; }

		/// <summary>
		/// PickedUpAmount
		/// </summary>
		public int? PickedUpAmount { get; set; }

		/// <summary>
		/// Weight
		/// </summary>
		public double? Weight { get; set; }

		/// <summary>
		/// Volume
		/// </summary>
		public double? Volume { get; set; }

		/// <summary>
		/// DestinationAddressId
		/// </summary>
		public string? DestinationAddressId { get; set; }

		/// <summary>
		/// DestinationAddressNaturalId
		/// </summary>
		public string? DestinationAddressNaturalId { get; set; }

		/// <summary>
		/// DestinationAddressName
		/// </summary>
		public string? DestinationAddressName { get; set; }

		/// <summary>
		/// ShipmentItemId
		/// </summary>
		public string? ShipmentItemId { get; set; }

		/// <summary>
		/// CountryId
		/// </summary>
		public string? CountryId { get; set; }

		/// <summary>
		/// ReputationChange
		/// </summary>
		public int? ReputationChange { get; set; }

		/// <summary>
		/// LoanInterestAmount
		/// </summary>
		public double? LoanInterestAmount { get; set; }

		/// <summary>
		/// LoanInterestCurrency
		/// </summary>
		public string? LoanInterestCurrency { get; set; }

		/// <summary>
		/// LoanRepaymentAmount
		/// </summary>
		public double? LoanRepaymentAmount { get; set; }

		/// <summary>
		/// LoanRepaymentCurrency
		/// </summary>
		public string? LoanRepaymentCurrency { get; set; }

		/// <summary>
		/// LoanTotalAmount
		/// </summary>
		public double? LoanTotalAmount { get; set; }

		/// <summary>
		/// LoanTotalCurrency
		/// </summary>
		public string? LoanTotalCurrency { get; set; }
	}
}
