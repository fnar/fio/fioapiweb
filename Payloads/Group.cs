﻿using Microsoft.VisualBasic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace FIOAPIWeb.Payloads
{

    /// <summary>
    /// Group Model
    /// </summary>
    public class Group
    {
        /// <summary>
        /// The largest GroupId possible
        /// </summary>
        public const int LargestGroupId = 999999;

        /// <summary>
        /// GroupId
        /// </summary>
        public int GroupId { get; set; }

        /// <summary>
        /// GroupName
        /// </summary>
        public string GroupName { get; set; } = null!;

        /// <summary>
        /// GroupOwner
        /// </summary>
        public string GroupOwner { get; set; } = null!;

        /// <summary>
        /// PendingInvites
        /// </summary>
        public  List<GroupPendingInvite> PendingInvites { get; set; } = new List<GroupPendingInvite>();

        /// <summary>
        /// Admins
        /// </summary>
        public List<GroupAdmin> Admins { get; set; } = new List<GroupAdmin>();

        /// <summary>
        /// Users
        /// </summary>
        public List<GroupUser> Users { get; set; } = new List<GroupUser>();

        /// <summary>
        /// Retrieves a list of member UserNames
        /// </summary>
        [NotMapped]
        public List<string> MemberUserNames
        {
            get
            {
                return Admins
                    .Select(a => a.UserName)
                    .Concat(Users.Select(u => u.UserName))
                    .Concat(new string[] { GroupOwner })
                    .ToList();
            }
        }
    }
}
