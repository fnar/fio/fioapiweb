﻿namespace FIOAPIWeb.Payloads
{
    /// <summary>
    /// CompanyPlanet
    /// </summary>
    public class CompanyPlanet
    {
        /// <summary>
        /// CompanyPlanetId: {CompanyId}-{PlanetId}
        /// </summary>
        public string CompanyPlanetId { get; set; } = null!;

        /// <summary>
        /// PlanetId
        /// </summary>
        public string PlanetId { get; set; } = null!;

        /// <summary>
        /// PlanetName
        /// </summary>
        public string PlanetName { get; set; } = null!;

        /// <summary>
        /// PlanetNaturalId
        /// </summary>
        public string PlanetNaturalId { get; set; } = null!;
    }
}
