﻿namespace FIOAPIWeb.Payloads
{
    /// <summary>
    /// The CreateAPI Response Payload
    /// </summary>
    public class CreateAPIKeyResponse
    {
        /// <summary>
        /// The newly created APIKey
        /// </summary>
        public Guid APIKey { get; set; }
    }
}
