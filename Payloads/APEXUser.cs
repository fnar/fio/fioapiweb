﻿namespace FIOAPIWeb.Payloads
{
	/// <summary>
	/// APEXUser
	/// </summary>
	public class APEXUser
	{
		/// <summary>
		/// APEXUserId
		/// </summary>
		public string APEXUserId { get; set; } = null!;

		/// <summary>
		/// UserName
		/// </summary>
		public string? UserName { get; set; } = null!;

		/// <summary>
		/// SubscriptionLevel
		/// </summary>
		public string? SubscriptionLevel { get; set; } = null!;

		/// <summary>
		/// HighestTier
		/// </summary>
		public string? HighestTier { get; set; }

		/// <summary>
		/// CompanyId
		/// </summary>
		public string? CompanyId { get; set; } = null!;

		/// <summary>
		/// CompanyName
		/// </summary>
		public string? CompanyName { get; set; } = null!;

		/// <summary>
		/// CompanyCode
		/// </summary>
		public string? CompanyCode { get; set; }

		/// <summary>
		/// Liquidated
		/// </summary>
		public bool? Liquidated { get; set; }

		/// <summary>
		/// CreatedTimestamp
		/// </summary>
		public long? CreatedTimestamp { get; set; }

		/// <summary>
		/// Created
		/// </summary>
		public DateTime? Created { get; set; }

		/// <summary>
		/// Team
		/// </summary>
		public bool Team { get; set; }

		/// <summary>
		/// Moderator
		/// </summary>
		public bool Moderator { get; set; }

		/// <summary>
		/// Pioneer
		/// </summary>
		public bool Pioneer { get; set; }

		/// <summary>
		/// ActiveDaysPerWeek
		/// </summary>
		public int? ActiveDaysPerWeek { get; set; }

		/// <summary>
		/// LastOnlineTimestamp
		/// </summary>
		public long? LastOnlineTimestamp { get; set; }

		/// <summary>
		/// When the user was last online
		/// </summary>
		public DateTime? LastOnline { get; set; }
	}
}
