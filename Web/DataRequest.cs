﻿using FIOAPIWeb.Extensions;
using FIOAPIWeb.Payloads;

namespace FIOAPIWeb.Web
{
	public class DataRequestPerms
	{
		public bool CompanyData { get; set; } = false;
		public bool ContractData { get; set; } = false;
		public bool CXOSData { get; set; } = false;
		public bool ProductionLineData { get; set; } = false;
		public bool ShipData { get; set; } = false;
		public bool SiteData { get; set; } = false;
		public bool StorageData {  get; set; } = false;
		public bool WorkforceData {  get; set; } = false;
	}

	public static class DataRequest
	{
		public static async Task<RetrievalResponse<AllData>?> Get(NavigationResult navigationResult, DataRequestPerms perms, string AuthToken)
		{
			if (navigationResult.GroupId != null)
			{
				return await Get((int)navigationResult.GroupId, perms, AuthToken);
			}
			else
			{
				return await Get(navigationResult.Usernames, perms, AuthToken);
			}
		}

		public static async Task<RetrievalResponse<AllData>?> Get(List<string> usernames, DataRequestPerms perms, string AuthToken)
		{
			if (usernames.Count == 0)
			{
				return new();
			}

			return await Get(usernames, null, perms, AuthToken);
		}

		public static async Task<RetrievalResponse<AllData>?> Get(int GroupId, DataRequestPerms perms, string AuthToken)
		{
			if (GroupId <= -1 || GroupId > 999999)
			{
				return new();
			}

			return await Get(null, GroupId, perms, AuthToken);
		}

		private static async Task<RetrievalResponse<AllData>?> Get(List<string>? usernames, int? GroupId, DataRequestPerms perms, string AuthToken)
		{
			bool IsGroupAsk = GroupId != -1;
			List<string> GetParams = new();
			if (IsGroupAsk)
			{
				GetParams.Add($"group={GroupId}");
			}
			else
			{
				usernames!.ForEach(username => GetParams.Add($"username={username}"));
			}

			if (perms.CompanyData)
			{
				GetParams.Add("include_company_data=true");
			}
			if (perms.ContractData)
			{
				GetParams.Add("include_contract_data=true");
			}
			if (perms.CXOSData)
			{
				GetParams.Add("include_cxos_data=true");
			}
			if (perms.ProductionLineData)
			{
				GetParams.Add("include_productionline_data=true");
			}
			if (perms.ShipData)
			{
				GetParams.Add("include_ship_data=true");
			}
            if (perms.SiteData)
			{
				GetParams.Add("include_site_data=true");
			}
            if (perms.StorageData)
			{
				GetParams.Add("include_storage_data=true");
			}
			if (perms.WorkforceData) 
			{
				GetParams.Add("include_workforce_data=true");
			}

			// @TODO: Escape URL?
			string get_params = string.Join('&', GetParams);
			string endpoint = $"/data?{get_params}";

			Request request = new Request(HttpMethod.Get, endpoint, AuthToken);
			return await request.GetResponseAsync<RetrievalResponse<AllData>>();
        }
	}
}
